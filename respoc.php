<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<title>Wilderness at the Smokies | Tennessee's Largest Waterpark Resort</title>
<link rel="author" href="https://plus.google.com/108004470087454676179/"/>
<link href="https://plus.google.com/108004470087454676179/" rel="publisher"/>
<link rel='stylesheet' type="text/css" href="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/A.style.css,qv1.0.pagespeed.cf.i3mjtlxEDI.css"/>
<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="/wp-content/themes/smokies/css/ie8.css" /><![endif]-->


<!-- This site is optimized with the Yoast WordPress SEO plugin v1.7.1 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="http://www.wildernessatthesmokies.com"/>
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Wilderness at the Smokies - Tennessee&#039;s Largest Waterpark Resort"/>
<meta property="og:url" content="http://www.wildernessatthesmokies.com"/>
<meta property="og:site_name" content="Wilderness at the Smokies"/>
<script type="application/ld+json">{ "@context": "http://schema.org", "@type": "WebSite", "url": "http://www.wildernessatthesmokies.com/", "potentialAction": { "@type": "SearchAction", "target": "http://www.wildernessatthesmokies.com/?s={search_term}", "query-input": "required name=search_term" } }</script>
<!-- / Yoast WordPress SEO plugin. -->

<style id='mc_style-css' media='all'>.mcForm input[type="submit"],input[type="button"]{background:#3971aa;border:0;padding:.3em .5em;color:#fff;font-size:1em;font-weight:900;cursor:pointer}.mcForm input[type="text"]{font-size:1em;padding:5px;border:1px solid #cecece}#listToggle span{cursor:pointer;font-style:italic;color:#52a2da}#result{background:#fff;position:absolute;top:0;padding:15px;color:#777;display:none;box-shadow:0 0 4px -2px #000;z-index:9999999;border:1px solid #cecece}#results_pointer{background:url(wp-content/plugins/mailchimp-sign-up/assets/images/xresults_pointer.png.pagespeed.ic.G89qeIe9gd.png) no-repeat;display:block;height:13px;width:28px;position:absolute;bottom:-13px!important}</style>
<link rel='stylesheet' id='rs-plugin-settings-css' href='http://www.wildernessatthesmokies.com/wp-content/plugins/revslider/rs-plugin/css/A.settings.css,qrev=4.6.0,aver=4.1.pagespeed.cf.cZRZtvlmKe.css' type='text/css' media='all'/>
<style id='rs-plugin-settings-inline-css' type='text/css'>.tp-caption a{text-decoration:none;color:#ccc;text-shadow:none;-webkit-transition:all .2s ease-out;-moz-transition:all .2s ease-out;-o-transition:all .2s ease-out;-ms-transition:all .2s ease-out}.tp-caption a:hover{color:#ccc}</style>
<link rel='stylesheet' id='ubermenu-basic-css' href='http://www.wildernessatthesmokies.com/wp-content/plugins/ubermenu/standard/styles/A.basic.css,qver=2.4.0.3.pagespeed.cf.5Aw9QhpZot.css' type='text/css' media='all'/>
<link rel='stylesheet' id='ubermenu-custom-css' href='http://www.wildernessatthesmokies.com/wp-content/plugins/ubermenu/custom/A.custom.css,qver=2.4.0.3.pagespeed.cf.41iHNkjOK6.css' type='text/css' media='all'/>
<link rel='stylesheet' id='jq_ui_css-css' href='http://www.wildernessatthesmokies.com/wp-content/plugins/ajax-event-calendar/css/A.jquery-ui-1.8.16.custom.css,qver=1.8.16.pagespeed.cf.c3CVzo4zBP.css' type='text/css' media='all'/>
<link rel='stylesheet' id='custom-css' href='http://www.wildernessatthesmokies.com/wp-content/plugins/ajax-event-calendar/css/A.custom.css,qver=1.0.4.pagespeed.cf.jVoT3Ye3NM.css' type='text/css' media='all'/>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-includes/js/jquery/jquery.js,qver=1.11.1.pagespeed.jm.z9hb-Gxqf6.js'></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-includes/js/jquery/jquery-migrate.min.js,qver=1.2.1.pagespeed.jm.mhpNjdU8Wl.js'></script>
<script type='text/javascript'>//<![CDATA[
jQuery.noConflict();(function($){$(function(){function results(){var media=$('input#address');var h=$('input#address').outerHeight(true)+$('#results_pointer').height()+$('#result').height();var w=$('input#address').width();var top=media.offset().top-h+"px";var left=media.offset().left+"px"
$('#result').css({'top':top,'left':left});var $box=$('#result');var fadeOut=function(){clearTimeout(timeout);$box.fadeOut('slow');};var timeout=setTimeout(fadeOut,5000);}
$(document).click(function(e){if(e.target.id!='emailSignupBtn'&&!$('#emailSignupBtn').find(e.target).length){}});$("#close").click(function(){$(this).parent().fadeOut(300);});$(document).ready(function(){$('#result').appendTo('body')
$('#invite').submit(function(event){event.preventDefault();$.ajax({url:'/wp-admin/admin-ajax.php',type:'POST',data:{action:'mailchimp_submit',email:$('#address').attr('value'),},success:function(data){$('#result').html(data+'<div id="results_pointer"></div><div id="close"></div>');$('#result').html(data+'<div id="results_pointer"></div><div id="close"></div>');$('#result').fadeIn(300);$("#close").click(function(){$(this).parent().fadeOut(300);});results();},error:function(){$('#result').html('Sorry, an error occurred.'+'<div id="close"></div>');$('#result').fadeIn(300);$("#close").click(function(){$(this).parent().fadeOut(300);results();});}});return false;});});});})(jQuery);
//]]></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js,qrev=4.6.0,aver=4.1.pagespeed.jm.FzibbBWE-N.js'></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=4.6.0&#038;ver=4.1'></script>
<script type="text/javascript">var _gaq=_gaq||[];_gaq.push(['_setAccount','UA-2473887-7']);_gaq.push(['_trackPageview']);(function(){var ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.src=('https:'==document.location.protocol?'https://ssl':'http://www')+'.google-analytics.com/ga.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})();</script>
<!-- <meta name="vfb" version="2.8.1" /> -->

<!-- UberMenu CSS - Controlled through UberMenu Options Panel 
================================================================ -->
<style type="text/css" id="ubermenu-style-generator-css">#megaMenu .ss-nav-menu-with-img>a>.wpmega-link-title,#megaMenu .ss-nav-menu-with-img>a>.wpmega-link-description,#megaMenu .ss-nav-menu-with-img>a>.wpmega-item-description,#megaMenu .ss-nav-menu-with-img>span.um-anchoremulator>.wpmega-link-title,#megaMenu .ss-nav-menu-with-img>span.um-anchoremulator>.wpmega-link-description,#megaMenu .ss-nav-menu-with-img>span.um-anchoremulator>.wpmega-item-description{padding-left:23px}</style>
<!-- end UberMenu CSS -->
		
			
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
 
<script type='text/javascript' src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/js/datepicker/js/bootstrap-datepicker.js.pagespeed.jm.U8kpRRkvG4.js"></script>
<script>//<![CDATA[
jQuery.noConflict();(function($){$(function(){$(window).resize(function(){resolutionchange()})
function resolutionchange(){width=$(window).width();height=$(window).height();if(width<1110){$('#book_btn_container span').text('Rooms');}else{$('#book_btn_container span').text('Room Search');}
if(width<935){$('.mcForm input[type=submit]').val('Go');}else{$('.mcForm input[type=submit]').val('Sign Up');}}
$(document).ready(function(){resolutionchange();$(".custom-select").each(function(){$(this).wrap("<span class='select-wrapper'></span>");$(this).after("<span class='holder'></span>");});$(".custom-select, select#adults").change(function(){var selectedOption=$(this).find(":selected").text();$(this).next(".holder").text(selectedOption);}).trigger('change');$('#dpd2').data('datepicker').hide();$('#dpd1').data('datepicker').hide();});var nowTemp=new Date();var now=new Date(nowTemp.getFullYear(),nowTemp.getMonth(),nowTemp.getDate(),0,0,0,0);var checkin=$('#dpd1').datepicker({onRender:function(date){return date.valueOf()<now.valueOf()?'disabled':'';}}).on('changeDate',function(ev){if(ev.date.valueOf()>checkout.date.valueOf()){var newDate=new Date(ev.date)
newDate.setDate(newDate.getDate()+1);checkout.setValue(newDate);}
checkin.hide();$('#dpd2')[0].focus();}).data('datepicker');var checkout=$('#dpd2').datepicker({onRender:function(date){return date.valueOf()<=checkin.date.valueOf()?'disabled':'';}}).on('changeDate',function(ev){checkout.hide();}).data('datepicker');});})(jQuery);
//]]></script>
<script src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/js/weather.js.pagespeed.jm.1YRWvmNQL1.js"></script> 
<script src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/js/jquery.jcarousel.min.js.pagespeed.jm.w7rfkGLMLF.js"></script>
<script>//<![CDATA[
(function($){$(function(){var jcarousel=$('.jcarousel');jcarousel.on('jcarousel:reload jcarousel:create',function(){var width=jcarousel.innerWidth();if(width>=800){width=width/4;}else if(width>=350){width=width/1;}
else if(width>=768){width=width/2;}
jcarousel.jcarousel('items').css('width',width+'px');}).jcarousel({wrap:'circular'});$('.jcarousel-control-prev').jcarouselControl({target:'-=1'});$('.jcarousel-control-next').jcarouselControl({target:'+=1'});$('.jcarousel-pagination').on('jcarouselpagination:active','a',function(){$(this).addClass('active');}).on('jcarouselpagination:inactive','a',function(){$(this).removeClass('active');}).on('click',function(e){e.preventDefault();}).jcarouselPagination({perPage:1,item:function(page){return'<a href="#'+page+'">'+page+'</a>';}});var jcarousel2=$('.jc-slider');jcarousel2.on('jcarousel:reload jcarousel:create',function(){var width=jcarousel.innerWidth();if(width>=600){width=width/1;}else if(width>=350){width=width/1;}
jcarousel2.jcarousel('items').css('width',width+'px');}).jcarousel({wrap:'circular'}).jcarouselAutoscroll({interval:3000,target:'+=1',autostart:true})
$('.jc-slider-control-prev').jcarouselControl({target:'-=1'});$('.jc-slider-control-next').jcarouselControl({target:'+=1'});$('.jc-slider-pagination').on('jcarouselpagination:active','a',function(){$(this).addClass('active');}).on('jcarouselpagination:inactive','a',function(){$(this).removeClass('active');}).on('click',function(e){e.preventDefault();}).jcarouselPagination({perPage:1,item:function(page){return'<a href="#'+page+'">'+page+'</a>';}});});})(jQuery);
//]]></script>
<script>//<![CDATA[
(function(t){"use strict";t.jCarousel.plugin("jcarouselAutoscroll",{_options:{target:"+=1",interval:3e3,autostart:!0},_timer:null,_init:function(){this.onDestroy=t.proxy(function(){this._destroy(),this.carousel().one("jcarousel:createend",t.proxy(this._create,this))},this),this.onAnimateEnd=t.proxy(this.start,this)},_create:function(){this.carousel().one("jcarousel:destroy",this.onDestroy),this.options("autostart")&&this.start()},_destroy:function(){this.stop(),this.carousel().off("jcarousel:destroy",this.onDestroy)},start:function(){return this.stop(),this.carousel().one("jcarousel:animateend",this.onAnimateEnd),this._timer=setTimeout(t.proxy(function(){this.carousel().jcarousel("scroll",this.options("target"))},this),this.options("interval")),this},stop:function(){return this._timer&&(this._timer=clearTimeout(this._timer)),this.carousel().off("jcarousel:animateend",this.onAnimateEnd),this}})})(jQuery);
//]]></script>
<script src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/js/smokies.js,qv1.4.pagespeed.jm.o-LpvDfcP-.js"></script>
<script src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/js/jquery.fancybox.js.pagespeed.jm.hIBEZ7J8Mq.js"></script>
<script src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/js/jquery.jpanelmenu.min.js.pagespeed.jm.RK-pcjOBQh.js"></script>
</head>
<body>

 
<div id="mobile-nav">
<div id="logo-mobile"></div>
<a href="#menu" class="menu-trigger"></a></div>
		<nav id="mobile-menu">
			<div class="menu-mobile-container"><ul id="menu-mobile" class="menu"><li id="menu-item-6798" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-6798"><a href="/">Home</a></li>
<li id="menu-item-6767" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6767"><a href="http://www.wildernessatthesmokies.com/lodging">Lodging</a></li>
<li id="menu-item-6768" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6768"><a href="http://www.wildernessatthesmokies.com/specials">Specials</a></li>
<li id="menu-item-6769" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6769"><a href="http://www.wildernessatthesmokies.com/waterparks">Waterparks</a></li>
<li id="menu-item-6770" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6770"><a href="http://www.wildernessatthesmokies.com/adventureforest">Wilderness Adventure Forest</a></li>
<li id="menu-item-6789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6789"><a href="http://www.wildernessatthesmokies.com/amenities">Amenities</a></li>
<li id="menu-item-6772" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6772"><a href="http://www.wildernessatthesmokies.com/meetings-events">Meetings &#038; Events</a></li>
<li id="menu-item-6775" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6775"><a href="http://www.wildernessatthesmokies.com/resort-info/videos">Videos</a></li>
<li id="menu-item-6776" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6776"><a href="http://www.wildernessatthesmokies.com/resort-info/photo-gallery">Photo Gallery</a></li>
<li id="menu-item-6782" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6782"><a href="http://www.wildernessatthesmokies.com/resort-info/faqs">FAQ&#8217;s</a></li>
<li id="menu-item-6773" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6773"><a href="http://www.wildernessatthesmokies.com/resort-info">Resort Info</a></li>
</ul></div>		</nav>


<div id="header_socials_wrap" class="inner max">
<div id="header_socials"><div id="social_lead">Like.<span> Share. </span>Follow. </div><div id="google" class="header_social"><a href="http://www.youtube.com/channel/UCgYMlymvovwkShvBC65OtHA">YouTube</a></div><div id="twitter" class="header_social"><a href="https://twitter.com/WildSmokies">Twitter</a></div><div id="facebook" class="header_social"><a href="https://www.facebook.com/Wildernessatthesmokies?ref=share">Facebook</a></div></div></div>
<div id="header" class="  span12">
<div class="inner max"><div id="logo"></div></div>
<div id="nav"><div class="inner max"><div id="header-menu"><div id="megaMenu" class="megaMenuContainer megaMenu-nojs megaMenuHorizontal megaResponsive megaResponsiveToggle wpmega-withjs megaMenuOnHover megaFullWidth wpmega-noconflict megaMinimizeResiduals megaResetStyles themeloc-header-menu"><div id="megaMenuToggle" class="megaMenuToggle"> &nbsp; <span class="megaMenuToggle-icon"></span></div><ul id="megaUber" class="megaMenu"><li id="menu-item-4833" class="logo menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-0 ss-nav-menu-item-depth-0 ss-nav-menu-reg um-flyout-align-center"><a href="http://wildernessatthesmokies.com"><span class="wpmega-link-title">Logo</span></a></li><li id="menu-item-4827" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-1 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth ss-nav-menu-mega-alignCenter"><a href="http://www.wildernessatthesmokies.com/lodging"><span class="wpmega-link-title">Lodging</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-4843" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-1 ss-nav-menu-notext ss-sidebar"><a href="http://www.wildernessatthesmokies.com/lodging/stone-hill-lodge"></a><div class="wpmega-nonlink wpmega-widgetarea ss-colgroup-2 uberClearfix"><ul class="um-sidebar" id="wpmega-wpmega-sidebar"><li id="execphp-2" class="widget widget_execphp">			<div class="execphpwidget"><div id="lodging_nav"><div class="span4 col"><div class="menu_header"><a href="/lodging?4916">Stone Hill Lodge</a></div><ul class="sub_menu_item"><li><a href="/lodging/room?r=Double+Queen+w%2F+Balcony">Double Queen w/ Balcony</a></li><li><a href="/lodging/room?r=Double+Queen+Sofa">Double Queen Sofa</a></li><li><a href="/lodging/room?r=King+Sofa+w%2F+Balcony">King Sofa w/ Balcony</a></li><li><a href="/lodging/room?r=King+Executive+Suite">King Executive Suite</a></li></ul><br class="clear cf"/></div><div class="span4 col"><div class="menu_header"><a href="/lodging?4922">River Lodge Suites</a></div><ul class="sub_menu_item"><li><a href="/lodging/room?r=Studio+Suite">Studio Suite</a></li><li><a href="/lodging/room?r=Bunk+Suite">Bunk Suite</a></li><li><a href="/lodging/room?r=Queen+Murphy+Deluxe+Suite">Queen Murphy Deluxe Suite</a></li><li><a href="/lodging/room?r=Junior+Deluxe+Suite">Junior Deluxe Suite</a></li><li><a href="/lodging/room?r=1+Bedroom+Deluxe+Suite">1 Bedroom Deluxe Suite</a></li><li><a href="/lodging/room?r=Deluxe+Family+Suite">Deluxe Family Suite</a></li><li><a href="/lodging/room?r=2+Bedroom+Premier+Suite">2 Bedroom Premier Suite</a></li><li><a href="/lodging/room?r=Fireplace+Suite">Fireplace Suite</a></li><li><a href="/lodging/room?r=1+Bedroom+Fireplace+Suite">1 Bedroom Fireplace Suite</a></li><li><a href="/lodging/room?r=2+Bedroom+Fireplace+Suite">2 Bedroom Fireplace Suite</a></li></ul><br class="clear cf"/></div><div class="span4 col"><div class="menu_header"><a href="/lodging?4924">Sanctuary Villas</a></div><ul class="sub_menu_item"><li><a href="/lodging/room?r=Villa+112">Villa 112</a></li><li><a href="/lodging/room?r=Villa+114">Villa 114</a></li><li><a href="/lodging/room?r=Villa+116">Villa 116</a></li><li><a href="/lodging/room?r=Villa+118">Villa 118</a></li><li><a href="/lodging/room?r=Villa+120">Villa 120</a></li><li><a href="/lodging/room?r=Villa+124">Villa 124</a></li><li><a href="/lodging/room?r=Villa+126">Villa 126</a></li></ul><br class="clear cf"/></div><br class="clear cf"/></div></div>
		</li>
<li id="simpleimage-12" class="widget widget_simpleimage">

	<p class="simple-image">
		<a href="/lodging"><img width="430" height="230" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/03/430x230x430_lodging1.jpg.pagespeed.ic.kNz7QJPwTu.jpg" class="attachment-full" alt="430_lodging"/></a>	</p>


</li>
</ul></div></li><li id="menu-item-6924" class="menu_header cf hadicap_rooms_nav menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-1"><a href="/lodging?handicap"><span class="wpmega-link-title">All Handicap Units</span></a></li></ul>
</li><li id="menu-item-4830" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-2 ss-nav-menu-item-depth-0 ss-nav-menu-reg um-flyout-align-center"><a href="http://www.wildernessatthesmokies.com/specials"><span class="wpmega-link-title">Specials</span></a></li><li id="menu-item-4831" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-3 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth ss-nav-menu-mega-alignCenter"><a href="http://www.wildernessatthesmokies.com/waterparks"><span class="wpmega-link-title">Waterparks</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-5724" class="menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-1 ss-nav-menu-notext ss-nav-menu-nolink ss-sidebar"><div class="wpmega-nonlink wpmega-widgetarea ss-colgroup-2 uberClearfix"><ul class="um-sidebar" id="wpmega-wpmega-sidebar-2"><li id="execphp-3" class="widget widget_execphp">			<div class="execphpwidget"><div id="custom_nav"><div class="span4 col"><div class="menu_header"><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Wild WaterDome Indoor Waterpark</a></div><ul class="sub_menu_item"><li><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Storm Chaser Thrill Ride</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">The Great Wave</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Indoor Tube Slides</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Magnolia Grove Hot Spa</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Runaway Canyon</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Smokies Surf Rider</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Washout Mountain</a></li></ul><br class="clear"/></div><div class="span4 col"><div class="menu_header"><a href="http://www.wildernessatthesmokies.com/waterparks/lake-wilderness">Lake Wilderness Outdoor Waterpark</a></div><ul class="sub_menu_item"><li><a href="http://www.wildernessatthesmokies.com/waterparks/lake-wilderness">NEW Cyclone Racers</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/lake-wilderness">Wild Vortex</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/lake-wilderness">Cataloochee Creek Adventure River</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/lake-wilderness">Washout Wilderness Rapids</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/lake-wilderness">Lake Wilderness Cabanas</a></li></ul><br class="clear"/></div><div class="span4 col"><div class="menu_header"><a href="http://www.wildernessatthesmokies.com/waterparks/salamander-springs">Salamander Springs Outdoor Waterpark</a></div><ul class="sub_menu_item"><li><a href="http://www.wildernessatthesmokies.com/waterparks/salamander-springs">Lunker's Landing</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/salamander-springs">The Timber Rattler</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/salamander-springs">Catfish Corral Activity Pool</a></li><li><a href="http://www.wildernessatthesmokies.com/waterparks/salamander-springs">Minnow Marsh & Wildflower Lagoon</a></li></ul><br class="clear"/></div></div></div>
		</li>
<li id="simpleimage-5" class="widget widget_simpleimage">

	<p class="simple-image">
		<a href="/waterparks"><img width="400" height="230" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/03/400x230x400_waterparks.jpg.pagespeed.ic.gpoE9cVFYq.jpg" class="attachment-full" alt="400_waterparks"/></a>	</p>


</li>
</ul></div></li></ul>
</li><li id="menu-item-5987" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-4 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth ss-nav-menu-mega-alignCenter"><a href="http://www.wildernessatthesmokies.com/adventureforest"><span class="wpmega-link-title">Attractions</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-6177" class="menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-1 ss-nav-menu-notext ss-sidebar"><a href="#"></a><div class="wpmega-nonlink wpmega-widgetarea ss-colgroup-3 uberClearfix"><ul class="um-sidebar" id="wpmega-wpmega-sidebar-3"><li id="nav_menu-2" class="widget widget_nav_menu"><h2 class="widgettitle">Adventure Forest</h2><div class="menu-adventure-forest-menu-container"><ul id="menu-adventure-forest-menu" class="menu"><li id="menu-item-6383" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6383"><a href="http://www.wildernessatthesmokies.com/resort-info/hours-of-operation">Hours of Operation</a></li>
<li id="menu-item-6182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6182"><a href="http://www.wildernessatthesmokies.com/adventureforest/parties-groups">Parties &#038; Groups</a></li>
<li id="menu-item-6183" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6183"><a href="http://www.wildernessatthesmokies.com/adventureforest/prices-passes">Prices &#038; Passes</a></li>
</ul></div></li>
<li id="execphp-4" class="widget widget_execphp">			<div class="execphpwidget"><div id="custom_nav"><div class="span4 col"><div class="menu_header"><a href="/adventureforest/attractions">Attractions</a></div><ul class="sub_menu_item"><li><a href="/adventureforest/attractions">Tree Top Towers</a></li><li><a href="/adventureforest/attractions">Fury in the Forest</a></li><li><a href="/adventureforest/attractions">Mount Wild</a></li><li><a href="/adventureforest/attractions">Copperhead</a></li><li><a href="/adventureforest/attractions">Daredevils Drop</a></li><li><a href="/adventureforest/attractions">Moonshine Run</a></li><li><a href="/adventureforest/attractions">Cataloochee Creek</a></li><li><a href="/adventureforest/attractions">Howlin' Hound Dog Alley</a></li><li><a href="/adventureforest/attractions">Cubs Climbing Den</a></li><li><a href="/adventureforest/attractions">Mega Arcade</a></li></ul><br class="clear"/></div></div></div>
		</li>
<li id="simpleimage-8" class="widget widget_simpleimage">

	<p class="simple-image">
		<a href="/adventureforest"><img width="640" height="180" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/03/640x180x640_af.jpg.pagespeed.ic.TxYlHXB9Uj.jpg" class="attachment-full" alt="640_af"/></a>	</p>


</li>
</ul></div></li></ul>
</li><li id="menu-item-5822" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-5 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth ss-nav-menu-mega-alignCenter"><a href="http://www.wildernessatthesmokies.com/amenities"><span class="wpmega-link-title">Amenities</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-6390" class="menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-1 ss-nav-menu-notext ss-nav-menu-nolink ss-sidebar"><div class="wpmega-nonlink wpmega-widgetarea ss-colgroup-3 uberClearfix"><ul class="um-sidebar" id="wpmega-wpmega-sidebar-4"><li id="execphp-5" class="widget widget_execphp"><h2 class="widgettitle">World Class Amenities</h2>			<div class="execphpwidget"><div id="custom_nav"><ul class="sub_menu_item"><div class="span2 col"><li><a href="/adventureforest">Adventure Forest</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/restaurants">Dining</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/retail-shops">Retail Shops</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/free-tickets">Area Attraction Tickets</a></li><div class="span2 col"><li><a href="/waterparks">Waterparks</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/paint-your-own-pottery">Paint Your Own Pottery</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/fitness-center">Fitness Center</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/golf">Golf</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/wilderness-rafting">Wilderness Rafting</a></li><div class="span2 col"><li><a href="http://www.wildernessatthesmokies.com/amenities/jetboat-adventure">Jetboat Adventure</a></li></ul></div></div>
		</li>
<li id="simpleimage-9" class="widget widget_simpleimage">

	<p class="simple-image">
		<a href="/amenities"><img width="350" height="230" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/03/350x230x350_amenities2.jpg.pagespeed.ic.HZeClwyK4e.jpg" class="attachment-full" alt="350_amenities2"/></a>	</p>


</li>
<li id="simpleimage-13" class="widget widget_simpleimage">

	<p class="simple-image">
		<a href="/amenities/jetboat-adventure"><img width="350" height="230" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/09/350x230xjet_boat_amenities.jpg.pagespeed.ic.VDEpbMDaex.jpg" class="attachment-full" alt="jet_boat_amenities"/></a>	</p>


</li>
</ul></div></li></ul>
</li><li id="menu-item-4828" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-6 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth ss-nav-menu-mega-alignCenter"><a href="http://www.wildernessatthesmokies.com/meetings-events"><span class="wpmega-link-title">Meetings &#038; Events</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-5898" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ss-nav-menu-item-depth-1 ss-nav-menu-nolink"><span class="um-anchoremulator"><span class="wpmega-link-title">Premier Meeting Facilities</span></span>
	<ul class="sub-menu sub-menu-2">
<li id="menu-item-5712" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/meeting-facilities-at-wilderness-at-the-smokies"><span class="wpmega-link-title">Meeting Facilities</span></a></li><li id="menu-item-5713" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/meeting-planner-materials"><span class="wpmega-link-title">Meeting Planner Materials</span></a></li><li id="menu-item-6837" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/request-for-proposal"><span class="wpmega-link-title">Request For Proposal</span></a></li>	</ul>
</li><li id="menu-item-5897" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ss-nav-menu-item-depth-1 ss-nav-menu-nolink"><span class="um-anchoremulator"><span class="wpmega-link-title">Ceremonies &#038; Gatherings</span></span>
	<ul class="sub-menu sub-menu-2">
<li id="menu-item-5708" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/weddings-honeymoons-at-wilderness-at-the-smokies"><span class="wpmega-link-title">Weddings &#038; Honeymoons</span></a></li><li id="menu-item-5709" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/family-reunions-at-wilderness-at-the-smokies"><span class="wpmega-link-title">Family Reunions</span></a></li><li id="menu-item-5710" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/tour-and-travel"><span class="wpmega-link-title">Tour &#038; Travel</span></a></li><li id="menu-item-5711" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/birthday-parties-at-wilderness-at-the-smokies"><span class="wpmega-link-title">Birthday Parties</span></a></li><li id="menu-item-5704" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/conventions-and-sporting-events-at-wilderness-at-the-smokies"><span class="wpmega-link-title">Conventions</span></a></li><li id="menu-item-5706" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/corporateassociation"><span class="wpmega-link-title">Corporate &#038; Association</span></a></li><li id="menu-item-5707" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/meetings-events/sports-and-competitive-events"><span class="wpmega-link-title">Sporting Events</span></a></li>	</ul>
</li><li id="menu-item-6435" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ss-nav-menu-item-depth-1 ss-nav-menu-notext"><a href="#"></a>
	<ul class="sub-menu sub-menu-2">
<li id="menu-item-6547" class="menuImage menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-2 ss-nav-menu-notext ss-nav-menu-nolink ss-sidebar"><div class="wpmega-nonlink wpmega-widgetarea ss-colgroup-1 uberClearfix"><ul class="um-sidebar" id="wpmega-wpmega-sidebar-5"><li id="simpleimage-11" class="widget widget_simpleimage">

	<p class="simple-image">
		<a href="/meetings-events"><img width="640" height="180" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/03/640x180x640_meetings2.jpg.pagespeed.ic.-ktKw7XN46.jpg" class="attachment-full" alt="640_meetings2"/></a>	</p>


</li>
</ul></div></li>	</ul>
</li></ul>
</li><li id="menu-item-5616" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children mega-with-sub ss-nav-menu-item-7 ss-nav-menu-item-depth-0 ss-nav-menu-mega ss-nav-menu-mega-fullWidth ss-nav-menu-mega-alignCenter"><a href="http://www.wildernessatthesmokies.com/resort-info"><span class="wpmega-link-title">Resort Info</span></a>
<ul class="sub-menu sub-menu-1">
<li id="menu-item-5900" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ss-nav-menu-item-depth-1 ss-nav-menu-nolink"><span class="um-anchoremulator"><span class="wpmega-link-title">Resort Information</span></span>
	<ul class="sub-menu sub-menu-2">
<li id="menu-item-5699" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/faqs"><span class="wpmega-link-title">FAQ&#8217;s</span></a></li><li id="menu-item-5432" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/about-wilderness-resorts"><span class="wpmega-link-title">About Wilderness Resorts</span></a></li><li id="menu-item-5694" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/area-info"><span class="wpmega-link-title">Area Info</span></a></li><li id="menu-item-6409" class="menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-2"><a href="http://wildernessatthesmokies.com/wp-content/uploads/2014/03/Resort_Map.pdf"><span class="wpmega-link-title">Resort Overview Map</span></a></li><li id="menu-item-5695" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/area-partners"><span class="wpmega-link-title">Area Partners</span></a></li><li id="menu-item-7209" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/donations"><span class="wpmega-link-title">Donations</span></a></li>	</ul>
</li><li id="menu-item-5901" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ss-nav-menu-item-depth-1 ss-nav-menu-nolink"><span class="um-anchoremulator"><span class="wpmega-link-title">Media</span></span>
	<ul class="sub-menu sub-menu-2">
<li id="menu-item-6219" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/photo-gallery"><span class="wpmega-link-title">Photo Gallery</span></a></li><li id="menu-item-6236" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/videos"><span class="wpmega-link-title">Videos</span></a></li><li id="menu-item-5697" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/media"><span class="wpmega-link-title">Media Room</span></a></li><li id="menu-item-5703" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/brochure-request"><span class="wpmega-link-title">Brochure Request</span></a></li><li id="menu-item-5698" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/order-gift-cards"><span class="wpmega-link-title">Order Gift Cards</span></a></li>	</ul>
</li><li id="menu-item-6220" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ss-nav-menu-item-depth-1 ss-nav-menu-nolink"><span class="um-anchoremulator"><span class="wpmega-link-title">Contact</span></span>
	<ul class="sub-menu sub-menu-2">
<li id="menu-item-5696" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/contact"><span class="wpmega-link-title">Contact</span></a></li><li id="menu-item-5701" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/guest-feedback"><span class="wpmega-link-title">Guest Feedback</span></a></li><li id="menu-item-5702" class="menu-item menu-item-type-post_type menu-item-object-page ss-nav-menu-item-depth-2"><a href="http://www.wildernessatthesmokies.com/resort-info/directions"><span class="wpmega-link-title">Directions</span></a></li>	</ul>
</li><li id="menu-item-6475" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children ss-nav-menu-item-depth-1 ss-nav-menu-notext"><a href="#"></a>
	<ul class="sub-menu sub-menu-2">
<li id="menu-item-6632" class="menu-item menu-item-type-custom menu-item-object-custom ss-nav-menu-item-depth-2 ss-nav-menu-notext ss-nav-menu-nolink ss-sidebar"><div class="wpmega-nonlink wpmega-widgetarea ss-colgroup-1 uberClearfix"><ul class="um-sidebar" id="wpmega-wpmega-sidebar-6"><li id="simpleimage-10" class="widget widget_simpleimage">

	<p class="simple-image">
		<a href="/resort-info"><img width="640" height="180" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/03/640x180x640_resort.jpg.pagespeed.ic.OZiN46j6BJ.jpg" class="attachment-full" alt="640_resort"/></a>	</p>


</li>
</ul></div></li>	</ul>
</li></ul>
</li></ul></div></div></div></div>
<div id="resWidgetTop"><script>function OnSubmitForm()
{var aa=document.getElementById('availabilityform');aa.submit();}
function cUpper(cObj)
{cObj.value=cObj.value.toUpperCase();}
function createCookie(name,value,seconds){if(seconds){var date=new Date();date.setTime(date.getTime()+(seconds));var expires="; expires="+date.toGMTString();}
else var expires="";document.cookie=name+"="+value+expires+"; path=www.wildernessatthesmokies.com";}
function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0)return c.substring(nameEQ.length,c.length);}
return null;}
function eraseCookie(name){createCookie(name,"",-1);}

function OnSubmitFormNew() {
	var aa=document.getElementById('availabilityform');
	var actionval = jQuery("#ctl00_ContentPlaceHolder1_ucFetchCalendar_ddlSpecialRates").val();
	jQuery(aa).attr("action",actionval);
	
	var date1 = jQuery("#dpd1").val();
	var date2 = jQuery("#dpd2").val();
	
	date1 = date1.replace(/\b0(?=\d)/g, '')
	date2 = date2.replace(/\b0(?=\d)/g, '')
	
	jQuery("#dpd1").val(date1);
	jQuery("#dpd2").val(date2);
	
	aa.submit();
}







</script>

<div id="resResponsive" class="span12 cf">
<div class="span6 col res_btn"><a href="http://res.wildernessatthesmokies.com/Default.aspx">Book Now</a></div>
<div class="span6 col res_btn"><a href="tel:(877) 325-9453">Call</a></div>
</div>

<div id="resForm" class="span12 cf">
<div id="book" class="span2 col">Book<span> Your stay</span></div>
<form action="https://b4c.wildernessatthesmokies.com/B4CheckinBNWebServiceSHT/B4CheckinBNWebService.asmx" method="GET" name="aspnetForm" id="availabilityform" target="_PARENT" onsubmit='return false'>
   <div class="span2 col"><label for="dpd1">Check in<br/>
<input type="text" value="01/13/2015" id="dpd1" name="checkInDate" readonly="true">
 </label></div>
  <div class="span2 col"> <label for="dpd2">Check out<br/>
<input type="text" name="checkOutDate" value="01/14/2015" id="dpd2" readonly="true">
            </label></div>
       <!--div id="adults" class="span1 col"> <label for="adults"># Guests<br/>       
	<select class="adults custom-select" name="no-guests" id="adults">
        <option value="1">Age3+</option>
        <option value="1">1</option>
        <option value="2">2</option>

        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>

        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>

        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
         <option value="16">16</option>
        <option value="17">17</option>
     </select>
     </label>
    </div-->
    <div class="span2 col">
 	 <label for="ctl00_ContentPlaceHolder1_ucFetchCalendar_ddlSpecialRates"><span id="promo">Property Type</span><br/>       
     <select name="special-rate" id="ctl00_ContentPlaceHolder1_ucFetchCalendar_ddlSpecialRates" class="custom-select">
        <option value="https://sh-secure.wildernessatthesmokies.com/" selected="selected">Stone Hill Lodge</option>
        <option value="https://rl-secure.wildernessatthesmokies.com/" >River Lodge</option>
        <option value="https://rl-secure.wildernessatthesmokies.com/">Sanctuary Villas</option>
	 </select>
     </label></div>
       
    <div class="span2 col"><label for="codebox">Code<br/>       
  	<input id="codebox" class="span2 col" name="TID" type="text" value="" onkeyup="return cUpper(this);" style="text-transform:uppercase;" placeholder="ENTER PROMO CODE">
    </label></div>
    
    <input type='hidden' name='hp' value='0'/>
	 </form>

    <input type=hidden DISABLED id="m1" value="11">
    <input type=hidden DISABLED id="d1" value="30">
    <input type=hidden DISABLED id="m2" value="12">
    <input type=hidden DISABLED id="d2" value="31">
    <input type='hidden' name='hotelCode' id='hotelCode'/>
<div id='book_btn_container' class="span2 col"><a id="book_btn" href="javascript:OnSubmitFormNew();"><span>Room Search</span></a></div>
</div></div>

	


</div>

<div id="container">
<div id="ch"><!--clear header--></div>
<div id="homeSlider">
<!-- START REVOLUTION SLIDER 4.6.0 fullwidth mode --><link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'><div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;max-height:600px;">	<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:600px;height:600px;"><ul>	<!-- SLIDE  -->	<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">		<!-- MAIN IMAGE -->		<img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/12/xNew-Year.jpg.pagespeed.ic.QLcy4WXACf.jpg" alt="New-Year" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">		<!-- LAYERS -->	</li>	<!-- SLIDE  -->	<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">		<!-- MAIN IMAGE -->		<img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/02/xwaterdome4.jpg.pagespeed.ic.j3mkczeEnS.jpg" alt="waterdome4" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">		<!-- LAYERS -->		<!-- LAYER NR. 1 -->		<div class="tp-caption slideTxt lfl tp-resizeme" data-x="12" data-y="151" data-speed="500" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;"><h2>Weatherproof Your Vacation<span>Always 84º</span></h2>		</div>	</li>	<!-- SLIDE  -->	<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">		<!-- MAIN IMAGE -->		<img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/02/slide12-4.jpg.pagespeed.ce.UZuBHqfSnM.jpg" alt="slide12-4" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">		<!-- LAYERS -->		<!-- LAYER NR. 1 -->		<div class="tp-caption slideTxt lfl tp-resizeme" data-x="14" data-y="136" data-speed="500" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;"><h2>Experience Family Fun<span>Huge Indoor Entertainment Center</span></h2>		</div>	</li></ul><div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>			<script type="text/javascript">var setREVStartSize=function(){var tpopt=new Object();tpopt.startwidth=1200;tpopt.startheight=600;tpopt.container=jQuery('#rev_slider_1_1');tpopt.fullScreen="off";tpopt.forceFullWidth="on";tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}};setREVStartSize();var tpj=jQuery;tpj.noConflict();var revapi1;tpj(document).ready(function(){if(tpj('#rev_slider_1_1').revolution==undefined)revslider_showDoubleJqueryError('#rev_slider_1_1');else revapi1=tpj('#rev_slider_1_1').show().revolution({dottedOverlay:"none",delay:4000,startwidth:1200,startheight:600,hideThumbs:200,thumbWidth:100,thumbHeight:50,thumbAmount:3,simplifyAll:"off",navigationType:"none",navigationArrows:"none",navigationStyle:"round",touchenabled:"on",onHoverStop:"off",nextSlideOnWindowFocus:"off",swipe_threshold:0.7,swipe_min_touches:1,drag_block_vertical:false,keyboardNavigation:"off",navigationHAlign:"center",navigationVAlign:"bottom",navigationHOffset:0,navigationVOffset:20,soloArrowLeftHalign:"left",soloArrowLeftValign:"center",soloArrowLeftHOffset:20,soloArrowLeftVOffset:0,soloArrowRightHalign:"right",soloArrowRightValign:"center",soloArrowRightHOffset:20,soloArrowRightVOffset:0,shadow:0,fullWidth:"on",fullScreen:"off",spinner:"spinner0",stopLoop:"off",stopAfterLoops:-1,stopAtSlide:-1,shuffle:"off",autoHeight:"off",forceFullWidth:"on",hideTimerBar:"on",hideThumbsOnMobile:"off",hideNavDelayOnMobile:1500,hideBulletsOnMobile:"off",hideArrowsOnMobile:"off",hideThumbsUnderResolution:0,hideSliderAtLimit:0,hideCaptionAtLimit:0,hideAllCaptionAtLilmit:0,startWithSlide:0});});</script>			</div><!-- END REVOLUTION SLIDER --></div> 
<div id="landing">
<div class="inner max cf np">
<div id="signup_hub">



<div class="span6">
<div id="waterpark_hours_link" class="hub_link span4 col"><a href="/resort-info/hours-of-operation">Waterpark Hrs</a></div>
<div id="faq_link" class="hub_link span4 col"><a href="/resort-info/faqs">FAQ's</a></div>
<div id="photos_link" class="hub_link span4 col"><a href="/resort-info/photo-gallery">Resort Photos</a></div>
</div>


<div class="span6">
<div id="homeSignUp" class="span12"><div id="signup_lead" class="span4"><span>Exclusive</span> Offers</div><div class="span8"><div id="result"></div><form class="mcForm" action="" id="invite" method="POST">
		<input type="text" placeholder="Enter your email..." name="email" id="address" data-validate="validate(required, email)"/>
		<input type="submit" value="Sign Up">
		</form></div></div>
</div>




</div>
</div>








</div>
<div id="homeSec1" class="fws white"> 
<div class="inner max cf">
     <div class="span6 col" id="home_content"><h1 style="text-align: left;">Rustic Mountain-Modern Charm</h1>
<h3 class="primary" style="text-align: left;">With World-Class Amenities</h3>
<p>Wilderness at the Smokies Waterpark Resort and Family Adventure Center is a luxurious vacation and meeting destination. Wilderness is a whole new concept of FUN for your Smoky Mountain vacation! Our <strong>Guest Exclusive</strong> <a title="Waterparks" href="http://wildernessatthesmokies.com/waterparks">Waterparks</a> are only for resort guests and <strong>Included With Your Stay!</strong></p>
<p> Wilderness is home to the <a title="Wild Waterdome Indoor Waterpark" href="http://wildernessatthesmokies.com/waterparks/wild-waterdome-indoor-waterpark">Wild WaterDome</a>, <strong>Tennessee's Largest Indoor Waterpark</strong>, which features a see-through roof that will weather-proof your vacation and give you the opportunity to tan inside all year long. Along with year-round indoor waterpark fun, you will find two <a title="Waterparks" href="http://wildernessatthesmokies.com/waterparks">outdoor waterparks</a> right outside your guest room door! And remember to check out the <a title="Wilderness Adventure Forest" href="http://wildernessatthesmokies.com/adventureforest">Adventure Forest </a>dry, family adventure center. Featuring a 3 story ropes course, multi-level laser tag, black light mini-golf, mega arcade and much more!</p>
<p><div style="text-align:center;" class="home_phone">Reservations &#038; Resort Info<br/>
<span class="phone">(877) 325-9453</span></div></p>
</div>
           <div class="span6 col">
         
          <div class="span6 col pixelated" id="home_last_col"> 
          <h4>Waterparks &amp; Attractions</h4>
			<br/>
            <div id="waterpark_logo_home"><a href="/waterparks"> <img src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/images/xwp_home.png.pagespeed.ic.mjansDxau0.png"/></a></div> 
         	  <div id="af_logo_home"><a href="/adventureforest">  <img src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/images/xad_home.png.pagespeed.ic.0lirBi1xNK.png"/>	</a></div> 
            <div id="gift_card" class="home"><span>Gift Cards</span><a href="/resort-info/order-gift-cards">Buy Now &rsaquo;</a></div>
            
             
		 </div>
	  <div class="span6 col" id="home_featured_special"><h3 class="featured_special_header">Featured <b>Special Offer</b></h3>         	<a href="http://www.wildernessatthesmokies.com/specials/wild-weekdays-2"> <img width="450" height="200" src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/03/450x200xwild_99-450x200.jpg.pagespeed.ic.PYoMRn8q0E.jpg" class="attachment-feat_special wp-post-image" alt="wild_99"/></a>
            <h4><a href="http://www.wildernessatthesmokies.com/specials/wild-weekdays-2">Wild Weekdays!</a></h4>
            <p>Stay for as low as $99 per night! Valid for Stone Hill Lodge stays this month for select weekdays, please call 877.325.9453 or view online.
</p>
            <div class='view_more'><a href="/specials">View more special offers &rsaquo; </a></div></div>
 	</div>	            
 </div>
 </div>     
 <br/>
 <div id="homeSec2" class="fws grey cf">
	<div class="inner max cf"> 
	 <div class="esp" id="eat">Laugh</div><div class="esp" id="play">Play</div><div class="esp" id="stay">Stay</div>
	        <div class="jcarousel-wrapper">
        <div class="jcarousel">
        <ul> 			 
	        		 <li>
                 
                    <a href="http://www.wildernessatthesmokies.com/amenities/restaurants"><img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/01/xkef.jpg.pagespeed.ic.olE7e73CnJ.jpg"/></a>
                    <p>Kids 11 &amp; under Eat Free for breakfast and dinner with each paying adult</p>
                </li>
      
	          		 <li>
                 
                    <a href="http://www.wildernessatthesmokies.com/specials"><img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/01/xcarousel_01.jpg.pagespeed.ic.DO5LfBXqQe.jpg"/></a>
                    <p>Save over 50% with the Adventure Forest Attraction Passes</p>
                </li>
      
	          		 <li>
                 
                    <a href="http://www.wildernessatthesmokies.com/amenities"><img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/01/xgatlinburg-aquarium-logo-275x230.jpg.pagespeed.ic.ZolCgGLeSG.jpg"/></a>
                    <p>Stay at Wilderness and Receive Passes to Area Attractions</p>
                </li>
      
	          		 <li>
                 
                    <a href="http://www.wildernessatthesmokies.com/meetings-events"><img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/01/xgroupsales.jpg.pagespeed.ic.cDIJLMqMmk.jpg"/></a>
                    <p>Perfect Destination for Your Smoky Mountain Meeting or Event</p>
                </li>
      
	          		 <li>
                 
                    <a href="http://www.wildernessatthesmokies.com/amenities/golf"><img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/01/xgolfcarousel.jpg.pagespeed.ic.O74cUPh1CB.jpg"/></a>
                    <p>36 Holes of Smoky Mountain Golf, just a few steps away</p>
                </li>
      
	          		 <li>
                 
                    <a href="http://www.wildernessatthesmokies.com/amenities"><img src="http://www.wildernessatthesmokies.com/wp-content/uploads/2014/01/xpotterycarousel.jpg.pagespeed.ic.Y7Z82DCFt8.jpg"/></a>
                    <p>Pick It! Paint It! Love It! Paint Your Own Pottery at Polka Dot Pots</p>
                </li>
      
	   
    </ul>
    </div>
    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    <a href="#" class="jcarousel-control-next">&rsaquo;</a>

    </div> 	
	</div>
  	</div>
  
  <div id="homeSec3" class="fws white">
	<div id="social_feeds" class="inner max cf">
    
    </div>
  </div>
  </div>

<div id="footer" class="cf ">
<div class="inner max cf">
<div class="span4 col footercol"><div id="gift_card"><span>Gift Cards</span><a href="/resort-info/order-gift-cards">Buy Now &rsaquo;</a></div><div class="trip-advisor"><img src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/images/xTA-award-wats-small.png.pagespeed.ic.2I9B7i4qLk.jpg"/></div></div>
<div class="span4 col footercol"><img src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/images/xfooter_logo.png.pagespeed.ic.Vw1TRR49MK.png"/>
    <div itemscope itemtype="http://schema.org/LocalBusiness">
      <h4><span itemprop="name">Wilderness at the Smokies</span></h4>
      <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <span itemprop="streetAddress">1424 Old Knoxville Hwy | </span>
        <span itemprop="addressLocality">Sevierville</span>,
        <span itemprop="addressRegion">TN</span>
      </div>
      Phone: <span itemprop="telephone">(865) 429-0625</span><br/>
      Reservations: <span itemprop="telephone">(877) 325-9453</span>
    </div>

</div>
<div class="span4 col footercol"><img src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/images/xservierville.png.pagespeed.ic.dHk5l1Y3TH.png"/>
<br/>
<div id="checkusout">
<a href="http://www.wildernessresort.com" id="checkit" target="_blank">
<img src="http://www.wildernessatthesmokies.com/wp-content/themes/smokies/images/xdells_logo.png.pagespeed.ic.LtefqTajTu.png"/>
<h4>Check Us Out In Wisconsin Dells</h4>
</a>
</div>
</div>
</div>
</div>

<div id="footercap">
<div class="inner max cf">
<div class="span3 copy">&copy; 2015  Wilderness at the Smokies</div>
<div class="span6" id="footer-menu"><div class="menu-footer-menu-container"><ul id="menu-footer-menu" class="menu"><li id="menu-item-6718" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6718"><a href="http://www.wildernessatthesmokies.com/resort-info/media">Media and PR</a></li>
<li id="menu-item-6723" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6723"><a href="http://www.summitwilderness.com/">Real Estate</a></li>
<li id="menu-item-4910" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4910"><a href="http://www.wildernessatthesmokies.com/employment">Employment</a></li>
<li id="menu-item-4912" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4912"><a href="http://www.wildernessatthesmokies.com/privacy-policy">Privacy Policy</a></li>
<li id="menu-item-7008" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7008"><a href="http://www.wildernessatthesmokies.com/resort-info/directions">Directions</a></li>
<li id="menu-item-7009" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7009"><a href="http://www.wildernessatthesmokies.com/resort-info/contact">Contact</a></li>
</ul></div></div>
<div class="span3 copy">Website by <a href="http://www.adlitwebsolutions.com/">Ad-Lit Web Solutions</a></div>
</div>
</div>
<script type="text/javascript">var $buoop={vs:{i:9,f:19,o:11,s:5},reminder:0,test:false,newwindow:true};$buoop.ol=window.onload;window.onload=function(){try{if($buoop.ol)$buoop.ol();}catch(e){}
var e=document.createElement("script");e.setAttribute("type","text/javascript");e.setAttribute("src","//browser-update.org/update.js");document.body.appendChild(e);}</script><div class="revsliderstyles"><style type="text/css">.tp-caption.slideTxt{line-height:28px;font-weight:100;font-family:"PT Sans";color:#fff;text-decoration:none;background-color:transparent;font-size:29px!important;border-width:0px;border-color:#ffd658;border-style:none}.slideTxt{line-height:28px;font-weight:100;font-family:"PT Sans";color:#fff;text-decoration:none;background-color:transparent;font-size:29px!important;border-width:0px;border-color:#ffd658;border-style:none}</style>
</div><script type='text/javascript'>//<![CDATA[
!function(a){a.fn.hoverIntent=function(b,c,d){var e={interval:100,sensitivity:7,timeout:0};e="object"==typeof b?a.extend(e,b):a.isFunction(c)?a.extend(e,{over:b,out:c,selector:d}):a.extend(e,{over:b,out:b,selector:c});var f,g,h,i,j=function(a){f=a.pageX,g=a.pageY},k=function(b,c){return c.hoverIntent_t=clearTimeout(c.hoverIntent_t),Math.abs(h-f)+Math.abs(i-g)<e.sensitivity?(a(c).off("mousemove.hoverIntent",j),c.hoverIntent_s=1,e.over.apply(c,[b])):(h=f,i=g,c.hoverIntent_t=setTimeout(function(){k(b,c)},e.interval),void 0)},l=function(a,b){return b.hoverIntent_t=clearTimeout(b.hoverIntent_t),b.hoverIntent_s=0,e.out.apply(b,[a])},m=function(b){var c=jQuery.extend({},b),d=this;d.hoverIntent_t&&(d.hoverIntent_t=clearTimeout(d.hoverIntent_t)),"mouseenter"==b.type?(h=c.pageX,i=c.pageY,a(d).on("mousemove.hoverIntent",j),1!=d.hoverIntent_s&&(d.hoverIntent_t=setTimeout(function(){k(c,d)},e.interval))):(a(d).off("mousemove.hoverIntent",j),1==d.hoverIntent_s&&(d.hoverIntent_t=setTimeout(function(){l(c,d)},e.timeout)))};return this.on({"mouseenter.hoverIntent":m,"mouseleave.hoverIntent":m},e.selector)}}(jQuery);
//]]></script>
<script type='text/javascript'>//<![CDATA[
var uberMenuSettings={"speed":"50","trigger":"hoverIntent","orientation":"horizontal","transition":"none","hoverInterval":"20","hoverTimeout":"100","removeConflicts":"on","autoAlign":"off","noconflict":"off","fullWidthSubs":"off","androidClick":"on","windowsClick":"on","iOScloseButton":"off","loadGoogleMaps":"off","repositionOnLoad":"on"};
//]]></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-content/plugins/ubermenu/core/js/ubermenu.min.js,qver=4.1.pagespeed.jm.F45KV_2rtm.js'></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-content/plugins/ajax-event-calendar/js/jquery.fullcalendar.min.js,qver=1.5.3.pagespeed.jm.N-kKzNy8ua.js'></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-content/plugins/ajax-event-calendar/js/jquery.simplemodal.1.4.3.min.js,qver=1.4.3.pagespeed.jm._ZBGOhWHqu.js'></script>
<script type='text/javascript'>//<![CDATA[
(function(a){function d(b){var c=b||window.event,d=[].slice.call(arguments,1),e=0,f=!0,g=0,h=0;return b=a.event.fix(c),b.type="mousewheel",c.wheelDelta&&(e=c.wheelDelta/120),c.detail&&(e=-c.detail/3),h=e,c.axis!==undefined&&c.axis===c.HORIZONTAL_AXIS&&(h=0,g=-1*e),c.wheelDeltaY!==undefined&&(h=c.wheelDeltaY/120),c.wheelDeltaX!==undefined&&(g=-1*c.wheelDeltaX/120),d.unshift(b,e,g,h),(a.event.dispatch||a.event.handle).apply(this,d)}var b=["DOMMouseScroll","mousewheel"];if(a.event.fixHooks)for(var c=b.length;c;)a.event.fixHooks[b[--c]]=a.event.mouseHooks;a.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=b.length;a;)this.addEventListener(b[--a],d,!1);else this.onmousewheel=d},teardown:function(){if(this.removeEventListener)for(var a=b.length;a;)this.removeEventListener(b[--a],d,!1);else this.onmousewheel=null}},a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery)
//]]></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-content/plugins/ajax-event-calendar/js/jquery.jgrowl.min.js,qver=1.2.5.pagespeed.jm.MLhHQKYJfo.js'></script>
<script type='text/javascript'>//<![CDATA[
var custom={"is_rtl":"","locale":"en","start_of_week":"1","step_interval":"30","datepicker_format":"mm\/dd\/yy","is24HrTime":"","show_weekends":"1","agenda_time_format":"h:mmt{ - h:mmt}","other_time_format":"h:mmt","axis_time_format":"h:mmt","limit":"1","today":"Today","all_day":"All Day","years":"Years","year":"Year","months":"Months","month":"Month","weeks":"Weeks","week":"Week","days":"Days","day":"Day","hours":"Hours","hour":"Hour","minutes":"Minutes","minute":"Minute","january":"January","february":"February","march":"March","april":"April","may":"May","june":"June","july":"July","august":"August","september":"September","october":"October","november":"November","december":"December","jan":"Jan","feb":"Feb","mar":"Mar","apr":"Apr","may_short":"May","jun":"Jun","jul":"Jul","aug":"Aug","sep":"Sep","oct":"Oct","nov":"Nov","dec":"Dec","sunday":"Sunday","monday":"Monday","tuesday":"Tuesday","wednesday":"Wednesday","thursday":"Thursday","friday":"Friday","saturday":"Saturday","sun":"Sun","mon":"Mon","tue":"Tue","wed":"Wed","thu":"Thu","fri":"Fri","sat":"Sat","close_event_form":"Close Event Form","loading_event_form":"Loading Event Form...","update_btn":"Update","delete_btn":"Delete","category_type":"Category type","hide_all_notifications":"hide all notifications","has_been_created":"has been created.","has_been_modified":"has been modified.","has_been_deleted":"has been deleted.","add_event":"Add Event","edit_event":"Edit Event","delete_event":"Delete this event?","loading":"Loading Events...","category_filter_label":"Category filter label","repeats_every":"Repeats Every","until":"Until","success":"Success!","whoops":"Whoops!","ajaxurl":"http:\/\/www.wildernessatthesmokies.com\/wp-admin\/admin-ajax.php","editable":""};
//]]></script>
<script type='text/javascript' src='http://www.wildernessatthesmokies.com/wp-content/plugins/ajax-event-calendar/js/jquery.init_show_calendar.js,qver=1.0.4.pagespeed.jm.AhtCOKChCG.js'></script>
</body>
</html>
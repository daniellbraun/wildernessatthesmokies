<?php 
/*
Template Name: packages-single
*/
?>
<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>

<div class="span12 cf" id="page_wrap">

<div class="inner cf max">	
<h1><? echo get_the_title(); ?></h1>
<div class="back_btn"> <a  href="/specials" > &lsaquo; &nbsp;  View all Packages </a></div>

</div>
	
<br class="cf">
<div class="inner max cf full_width_image">
<?
		$post_id = get_the_ID(); 
		$url = get_permalink();
		$image = get_the_post_thumbnail( $post_id,'specials'); 
		if(!$image):
		$image = '<img src="'.get_bloginfo('template_directory').'/images/special_placeholder.jpg" />';
		endif; 
		echo $image;
?>
</div>
<br class="cf">
<div class="inner cf max">	
<div class="span7 col">
<div class="span12 col specials_content">	
<h3 class="offer_details">What's Included?</h3>

<? echo the_content(); ?>


<? 
$value = get_field('options');
if ( in_array('Waterpark passes included',$value, TRUE) ):
echo '<span class="primary" style="margin:10px 0px; font-size:1.1em;"><b>Waterpark passes included!</b></span>';
endif;
?>
<br /><br />
<h4> Offer Details </h4>
<p><? echo strip_tags(get_field('package_details')); ?></p>
<br />
<? $promo = get_field('promo_code'); ?>
<? if($promo): ?>
Use Promo Code :<b> <? echo $promo; ?> </b>
<br class="cf" />
<? endif; ?>
</div>

</div>
<div class="span5 col">
<div class="span12 dark " style=" text-align:center"><h5>Call to Book Now</h5>Reservations & Resort Info<span class="phone" >(877) 325-9453</span></div>
<div class="span12 dark ">
<h5>Resort Policies</h5>
<?php include (TEMPLATEPATH . '/resort_policies.php'); ?>
</div>
</div>
</div>
</div>
</div>
<?php get_footer(); ?>
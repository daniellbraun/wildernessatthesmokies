<?php get_header(); ?>

<br class="clear" />
<div id="ch"><?    /* Clear Header  */    ?></div>
<div id="container">
<div class="bread_crumbs_wrap <? if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'af';} ?>"><div class="inner max"><div id="bread_crumbs" <? if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'class="af"';} ?>><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>

<div id="pageSlider">

<?    /*  add page sliders here */    ?>

</div>

<?php include (TEMPLATEPATH . '/slider_hub_bar.php'); ?>
<div class="span12" id="page_wrap">
<div id="container" class="light shadow inner max cf">



<?    /* Start Content */    ?>



<h1><? echo get_the_title(); ?></h1> 

<? echo apply_filters('the_content',  get_the_content()); ?>



<?    /* End Content */    ?>



<? 
/* Below Gets The Page Listing Formats */ 
$rows = get_field('listing_format');
if($rows):
foreach($rows as $row):	?>
<div class="span12 cf">
<div class="span3 col cf full_width_image"><img src="<? echo $row['image']["url"]; ?>" /></div>
<div class="span8 col cf">
<h2><? echo $row['title']; ?></h2>
<p><? echo $row['content']; ?></p>
</div>
</div>
<div id="container" class="cf"><hr /></div>
<?
endforeach;
endif;
?>
</div>
</div>
<div id="container">
<br class="cf"  />
<div id="homeSec3" class="fws cf white">
<div  id="social_feeds" class="inner max cf"><!-- Socials Loaded with ajax--></div>
</div>
</div></div>

<?php get_footer(); ?>
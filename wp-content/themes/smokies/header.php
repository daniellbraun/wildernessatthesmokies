<?php
/* 
 *  Cookie being set in order to prevent modal on front-page.php from appearing more than once.
 */
if ( $_COOKIE['popup'] != 1 ) { 
    setcookie( 'popup', 1 );
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
<title><?php customTitle(); ?></title>
<link rel="author" href="https://plus.google.com/108004470087454676179/"/>
<link href="https://plus.google.com/108004470087454676179/" rel="publisher" />
<link rel='stylesheet' type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/style.css?v1.1" />
<link rel='stylesheet' type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/css/pikaday.css" />
<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="/wp-content/themes/smokies/css/ie8.css" /><![endif]-->

<?php wp_head() ?>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<?php if (!is_page('201')){?>
<script type='text/javascript'  src="<?php echo get_bloginfo('template_directory'); ?>/js/datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/resWidget.js"></script>
<?php } ?>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.jcarousel.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jcarousel.responsive.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.jcarousel-autoscroll.min.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/smokies.js?v1.4"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/rewrites.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.fancybox.js"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/jquery.jpanelmenu.min.js"></script>
    <script src="<?php echo get_bloginfo('template_directory'); ?>/js/moment.min.js"></script>
    <script src="<?php echo get_bloginfo('template_directory'); ?>/js/pikaday.js"></script>
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5CR4PS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5CR4PS');</script>
<!-- End Google Tag Manager -->
<div id='bodywrap'>
 
<div id="mobile-nav">
<div id="logo-mobile"></div>
<a href="#menu" class="menu-trigger"></a></div>
<nav id="mobile-menu"><?php if (!$_GET['nomobilemenu2']) {echo get_mobile_menu_rewritten();}?></nav>
<div id="header_socials_wrap" class="inner max">
<div id="header_socials"><div id="social_lead">Like.<span> Share. </span>Follow. </div><div id="google" class="header_social"><a href="https://www.youtube.com/channel/UCzR19hUJ35Aa6poSvntWwXA">YouTube</a></div><div id="twitter" class="header_social"><a href="https://twitter.com/WildSmokies">Twitter</a></div><div id="facebook" class="header_social"><a href="https://www.facebook.com/Wildernessatthesmokies?ref=share">Facebook</a></div><div id="instagram" class="header_social"><a href="https://www.instagram.com/wildernesssmokies/">Instagram</a></div></div></div>
<div id="header" class="<?php if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'af';}?>  span12">
<div class="inner max"><div id="logo" <?php //if ( $post->post_parent == 5902 )  {echo 'class="af"';}?>></div></div>
<div id="nav" <?php if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'class="af"';}?>><div class="inner max"><div id="header-menu"><?php
if (!$_GET['nomobilemenu']) {
echo get_main_menu_rewritten();
}
?></div></div></div>
<?php if(is_front_page()){?>
<div id="resWidgetTop"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
<?php } ?>

<?php if(is_page('adventureforest')){?>
<div id="resWidgetTopAf"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>

<?php } ?>
	


</div>

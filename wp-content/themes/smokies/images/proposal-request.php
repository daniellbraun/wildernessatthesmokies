<html><head>
<style>
<!--
.leftblock {

width:360px;
float:left;
}
body {
color:#4B3117;
font-family:Arial,Helvetica,sans-serif;
font-size:13px;
}
table.main, table.main table {
/*background-color:#004C2C;
color:white;
border:1px solid black;*/
width:480px;margin-left:auto;margin-right:auto;
/*font-weight:bold;*/
font-size:12px;
}
.buffertd {

line-height:4px;
}
.highlight {
color:red;
}
/*#contactform table {
width:505px;
}*/
#contactform table textarea {
/*width:460px;*/
width:100%;
}
#contactform table .commentfield {
width:370px;
}
#submitbutton {
width:70px;
float:right;margin-right:194px;
}
-->
</style>

</head><body style='background-color: transparent'><a id='submitted'></a><div id='maincontentcontainer'>
<div align='center'>
  <h1 align="left" style="font-family: Times New Roman,Times,serif;font-size: 24px;font-weight: bold;">Request For Proposal</h1>
  <div align="left"></div>
</div>
<a id='submitted' />
<form action="#submitted" method="post" id='contactform'  >
              <table  cellpadding="0" border='0' cellspacing="5" class='main' >
                <tr>
                  <td colspan="8" valign="top"><div style='font-size:13px;font-weight:100'>Thank you for your interest in our wonderful facility. Let our experienced staff help you make your next special event, meeting or convention memorable.<br>
                  <br />Please take a moment to complete the information below so that we may be of service to you. Or if you prefer, please call <b>865.868.2171 </b> to contact one our friendly event planners.</div></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                  <td colspan="6" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="8" valign="top"><span style='color:red;font-size:13px;'>Required fields have a * next to them</span></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                  <td colspan="6" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Group  Name<span style="color:red;padding-left:10px">*</span></td>
                  <td colspan="6" valign="top"><input style='width:100%' name="group_name" type="text" id="group_name" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Contact  Name<span style="color:red;padding-left:10px">*</span></td>
                  <td colspan="6" valign="top"><input style='width:100%' name="contact_name" type="text" id="contact_name" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Company</td>
                  <td colspan="6" valign="top"><input style='width:100%' name="company" type="text" id="company" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Address</td>
                  <td colspan="6" valign="top"><input style='width:100%' name="address" type="text" id="address" value=""></td>
                </tr>
                                <tr>
                  <td colspan="2">City</td>
                  <td colspan="6"><input  style='width:100%'  name="city" type="text" id="city" value=""></td>
                </tr>
                <tr>
                  <td colspan="2">State</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="state" type="text" id="state" value=""></td>
                </tr>
                <tr>
                  <td colspan="2">Zip</td>
                  <td colspan="6" valign="top"><input style='width:100%'  name="zip" type="text" id="zip" value=""></td>
                </tr>
                                <tr>
                  <td colspan="2">Office Phone<span style="color:red;padding-left:10px">*</span></td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="officephone" type="text" id="officephone" value=""></td>
                </tr>
                <tr>
                  <td colspan="2">Cell Phone</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="cell_phone" type="text" id="cell_phone" value=""></td>
                </tr>
<tr>
                  <td colspan="2">Fax</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="fax" type="text" id="fax" value=""></td>
                </tr><tr>
                  <td colspan="2">Email</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="email" type="text" id="email" value=""></td>
                </tr>                <tr>
                  <td colspan="8" valign="top">What is the best way to reach you? 
                  <input  type="radio" name="best_reach_method" value="Phone" id="best_reach_method">Phone <input  type="radio" name="best_reach_method" value="Fax" id="best_reach_method">Fax <input  type="radio" name="best_reach_method" value="Email" id="best_reach_method">Email <input  type="radio" name="best_reach_method" value="US Mail" id="best_reach_method">US Mail</td>
                  </tr>
                <tr>
                  <td colspan="2" valign="top">Preferred Dates/Pattern</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="preferred_dates" type="text" id="preferred_dates" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Alternate Dates/Pattern:</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="alt_dates" type="text" id="alt_dates" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Number of Attendees</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="number_of_attendees" type="text" id="number_of_attendees" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Number of Rooms</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="number_of_rooms" type="text" id="number_of_rooms" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Guest Rooms Schedule:</td>
                  <td colspan="6" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="8" valign="top"><table width="100%" border="0" cellpadding="0">
                    <tr>
                      <td>Day:</td>
                      <td>Date:</td>
                      <td>Rooms:</td>
                    </tr>
                    <tr>
                      <td>Monday</td>
                      <td><input  style='width:100%' name="ga1" type="text" id="ga1" value=""></td>
                      <td><input  style='width:100%' name="gb1" type="text" id="gb1" value=""></td>
                    </tr>
                    <tr>
                      <td>Tuesday</td>
                      <td><input  style='width:100%' name="ga2" type="text" id="ga2" value=""></td>
                      <td><input  style='width:100%' name="gb2" type="text" id="gb2" value=""></td>
                    </tr>
                    <tr>
                      <td>Wednesday</td>
                      <td><input  style='width:100%' name="ga3" type="text" id="ga3" value=""></td>
                      <td><input  style='width:100%' name="gb3" type="text" id="gb3" value=""></td>
                    </tr>
                    <tr>
                      <td>Thursday</td>
                      <td><input  style='width:100%' name="ga4" type="text" id="ga4" value=""></td>
                      <td><input  style='width:100%' name="gb4" type="text" id="gb4" value=""></td>
                    </tr>
                    <tr>
                      <td>Friday</td>
                      <td><input  style='width:100%' name="ga5" type="text" id="ga5" value=""></td>
                      <td><input  style='width:100%' name="gb5" type="text" id="gb5" value=""></td>
                    </tr>
                    <tr>
                      <td>Saturday</td>
                      <td><input  style='width:100%' name="ga6" type="text" id="ga6" value=""></td>
                      <td><input  style='width:100%' name="gb6" type="text" id="gb6" value=""></td>
                    </tr>
                    <tr>
                      <td>Sunday</td>
                      <td><input  style='width:100%' name="ga7" type="text" id="ga7" value=""></td>
                      <td><input  style='width:100%' name="gb7" type="text" id="gb7" value=""></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">History:</td>
                  <td colspan="6" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="8" valign="top">Please  take a moment to tell us about the last three or more meetings for this group  in order for our hotel to provide you with a competitive and prompt proposal.  We would appreciate it if you would include, year/dates, city/state, hotel and  rates given. </td>
                </tr>
                <tr>
                  <td colspan="8" valign="top"><textarea name="history" id="history" cols="45" rows="5"></textarea></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Budget/Rate Range:</td>
                  <td colspan="6" valign="top"><input  style='width:100%' name="budget" type="text" id="budget" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Meeting Room/Banquet Requirements:</td>
                  <td colspan="6" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="8" valign="top"><table width="100%" border="0" cellpadding="0">
                    <tr>
                      <td>Date/Day:</td>
                      <td>Time:</td>
                      <td>Function:</td>
                      <td>Meal:</td>
                      <td>Set up/#PP:</td>
                    </tr>
                    <tr>
                      <td><input  style='width:100%' name="ma1" type="text" id="ma1" value=""></td>
                      <td><input  style='width:100%' name="mb1" type="text" id="mb1" value=""></td>
                      <td><input  style='width:100%' name="mc1" type="text" id="mc1" value=""></td>
                      <td><input  style='width:100%' name="md1" type="text" id="md1" value=""></td>
                      <td><input  style='width:100%' name="me1" type="text" id="me1" value=""></td>
                    </tr>
                    <tr>
                      <td><input  style='width:100%' name="ma2" type="text" id="ma2" value=""></td>
                      <td><input  style='width:100%' name="mb2" type="text" id="mb2" value=""></td>
                      <td><input  style='width:100%' name="mc2" type="text" id="mc2" value=""></td>
                      <td><input  style='width:100%' name="md2" type="text" id="md2" value=""></td>
                      <td><input  style='width:100%' name="me2" type="text" id="me2" value=""></td>
                    </tr>
                    <tr>
                      <td><input  style='width:100%' name="ma3" type="text" id="ma3" value=""></td>
                      <td><input  style='width:100%' name="mb3" type="text" id="mb3" value=""></td>
                      <td><input  style='width:100%' name="mc3" type="text" id="mc3" value=""></td>
                      <td><input  style='width:100%' name="md3" type="text" id="md3" value=""></td>
                      <td><input  style='width:100%' name="me3" type="text" id="me3" value=""></td>
                    </tr>
                    <tr>
                      <td><input  style='width:100%' name="ma4" type="text" id="ma4" value=""></td>
                      <td><input  style='width:100%' name="mb4" type="text" id="mb4" value=""></td>
                      <td><input  style='width:100%' name="mc4" type="text" id="mc4" value=""></td>
                      <td><input  style='width:100%' name="md4" type="text" id="md4" value=""></td>
                      <td><input  style='width:100%' name="me4" type="text" id="me4" value=""></td>
                    </tr>
                    <tr>
                      <td><input  style='width:100%' name="ma5" type="text" id="ma5" value=""></td>
                      <td><input  style='width:100%' name="mb5" type="text" id="mb5" value=""></td>
                      <td><input  style='width:100%' name="mc5" type="text" id="mc5" value=""></td>
                      <td><input  style='width:100%' name="md5" type="text" id="md5" value=""></td>
                      <td><input  style='width:100%' name="me5" type="text" id="me5" value=""></td>
                    </tr>
                    <tr>
                      <td><input  style='width:100%' name="ma6" type="text" id="ma6" value=""></td>
                      <td><input  style='width:100%' name="mb6" type="text" id="mb6" value=""></td>
                      <td><input  style='width:100%' name="mc6" type="text" id="mc6" value=""></td>
                      <td><input  style='width:100%' name="md6" type="text" id="md6" value=""></td>
                      <td><input  style='width:100%' name="me6" type="text" id="me6" value=""></td>
                    </tr>
                    <tr>
                      <td><input  style='width:100%' name="ma7" type="text" id="ma7" value=""></td>
                      <td><input  style='width:100%' name="mb7" type="text" id="mb7" value=""></td>
                      <td><input  style='width:100%' name="mc7" type="text" id="mc7" value=""></td>
                      <td><input  style='width:100%' name="md7" type="text" id="md7" value=""></td>
                      <td><input  style='width:100%' name="me7" type="text" id="me7" value=""></td>
                    </tr>
<tr>
                      <td><input  style='width:100%' name="ma8" type="text" id="ma8" value=""></td>
                      <td><input  style='width:100%' name="mb8" type="text" id="mb8" value=""></td>
                      <td><input  style='width:100%' name="mc8" type="text" id="mc8" value=""></td>
                      <td><input  style='width:100%' name="md8" type="text" id="md8" value=""></td>
                      <td><input  style='width:100%' name="me8" type="text" id="me8" value=""></td>
                    </tr><tr>
                      <td><input  style='width:100%' name="ma9" type="text" id="ma9" value=""></td>
                      <td><input  style='width:100%' name="mb9" type="text" id="mb9" value=""></td>
                      <td><input  style='width:100%' name="mc9" type="text" id="mc9" value=""></td>
                      <td><input  style='width:100%' name="md9" type="text" id="md9" value=""></td>
                      <td><input  style='width:100%' name="me9" type="text" id="me9" value=""></td>
                    </tr><tr>
                      <td><input  style='width:100%' name="ma10" type="text" id="ma10" value=""></td>
                      <td><input  style='width:100%' name="mb10" type="text" id="mb10" value=""></td>
                      <td><input  style='width:100%' name="mc10" type="text" id="mc10" value=""></td>
                      <td><input  style='width:100%' name="md10" type="text" id="md10" value=""></td>
                      <td><input  style='width:100%' name="me10" type="text" id="me10" value=""></td>
                    </tr><tr>
                      <td><input  style='width:100%' name="ma11" type="text" id="ma11" value=""></td>
                      <td><input  style='width:100%' name="mb11" type="text" id="mb11" value=""></td>
                      <td><input  style='width:100%' name="mc11" type="text" id="mc11" value=""></td>
                      <td><input  style='width:100%' name="md11" type="text" id="md11" value=""></td>
                      <td><input  style='width:100%' name="me11" type="text" id="me11" value=""></td>
                    </tr><tr>
                      <td><input  style='width:100%' name="ma12" type="text" id="ma12" value=""></td>
                      <td><input  style='width:100%' name="mb12" type="text" id="mb12" value=""></td>
                      <td><input  style='width:100%' name="mc12" type="text" id="mc12" value=""></td>
                      <td><input  style='width:100%' name="md12" type="text" id="md12" value=""></td>
                      <td><input  style='width:100%' name="me12" type="text" id="me12" value=""></td>
                    </tr><tr>
                      <td><input  style='width:100%' name="ma13" type="text" id="ma13" value=""></td>
                      <td><input  style='width:100%' name="mb13" type="text" id="mb13" value=""></td>
                      <td><input  style='width:100%' name="mc13" type="text" id="mc13" value=""></td>
                      <td><input  style='width:100%' name="md13" type="text" id="md13" value=""></td>
                      <td><input  style='width:100%' name="me13" type="text" id="me13" value=""></td>
                    </tr>                 
                    
                  </table></td>
                </tr>
                <tr>
                  <td colspan="8" valign="top">Meeting/banquet requirements (Additional functions, exhibit requirements, extensive AV, etc)</td>
                </tr>
                <tr>
                  <td colspan="8" valign="top"><textarea name="meeting_requirements" id="meeting_requirements" cols="45" rows="5"></textarea></td>
                </tr>
                <tr>
                  <td colspan="8" valign="top">How would you like to receive your proposal:
                    <input  type="radio" name="receive_proposal" value="Phone" id="receive_proposal">
                    Fax
                    <input  type="radio" name="receive_proposal" value="Fax" id="receive_proposal">
                    Email
                    <input  type="radio" name="receive_proposal" value="Email" id="receive_proposal">
                    US Mail                  </td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Would you also like us to include a meeting planner kit?</td>
                  <td colspan="6" valign="top"><select name="planner_kit" id="planner_kit">
                    <option  value="Yes">Yes</option>
                    <option   value="No">No</option>
                  </select>                  </td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">Proposal Deadline</td>
                  <td colspan="6" valign="top"><input style='width:100%' name="proposal_deadline" type="text" id="proposal_deadline" value=""></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">When will you be making your decision?</td>
                  <td colspan="6" valign="top"><input style='width:100%' name="when_decision" type="text" id="when_decision" value=""></td>
                </tr>
                <tr>
                  <td colspan="8" valign="top">Is  there anything else you would like to tell us to help us with this proposal  (other properties you are considering, flexibility of dates, decision factors,  purpose of meeting)&nbsp; </td>
                </tr>
                <tr>
                  <td colspan="8" valign="top"><textarea name="additional_details" id="additional_details" cols="45" rows="5"></textarea></td>
                </tr>
				
				<tr>
                  <td colspan="8" valign="top"><b>Match the Security Image:</b><strong><font color="#FF0000">*</font></strong>&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="validator" id="validator" size="4" />&nbsp;<img src="/wp-content/themes/parallelus-traject/includes/formengines/random2.php" alt="CAPTCHA image" width="60" height="20" vspace="1" align="top" /><input type='hidden' name='token' value='1564fc127c03d5d448a4e11be9296371' /></td>
                </tr>
				
              <tr><td><input type="submit" name="submit" id="submit" value="Submit!">
                </td>
                <td>&nbsp;</td>
                <td width="50%">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr></table>
</form><br /> 
      <br />
      <br />
      <br />
    </div>
	</body></html>

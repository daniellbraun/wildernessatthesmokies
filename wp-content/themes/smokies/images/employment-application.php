<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Employment Application</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Wilderness Resort" />
<meta name="keywords" content="wilderness resort" />
<link href="/sites/all/themes/seviervilleresort/style.css" rel="stylesheet" type="text/css" />
</head>
<body style='background-color: transparent'><a id='topsection'></a>
        
       <div align="center"><strong style='font-size:17px'>&nbsp;<br />
  APPLICATION FOR EMPLOYMENT</strong><br />
  <em>Please answer all questions.</em><br />WE ARE AN EQUAL OPPORTUNITY EMPLOYER<br /><br /></div><div align='left'><div style='font-size:10px;width:490px;'>NOTICE: Applicant should read the following information carefully before filling out any of the questions in this form. We are an equal opportunity employer and fully subscribe to the principles of equal opportunity. It is our policy to seek and employ the best qualified personnel in all positions without regard to race, color, religion, sex, disability, national origin, or any other basis made unlawful by either state or federal law. It is our policy to comply with all federal and state employment statutes.  Information requested on this application will not be used for any purpose prohibited by law.</div>


<br />
<style>
<!--
.rightcolumn {width:90%;}
.errormark {
color:red;
font-weight:bold;
font-size:18px;
display:none}
-->
</style>


<form action="#topsection" method="post" id='employmentform'>
<table border='0' style='width:490px;'>
  <tr>
    <td>First Name:</td>
    <td valign="top"><span class='errormark'>*</span></td> 
    <td valign="top" style='width:50%'>      <input  class="rightcolumn" name="firstname" type="text" id="firstname" value="" />    </td>
  </tr>
  <tr>
    <td>Last Name: </td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input class="rightcolumn" name="lastname" type="text" id="lastname" value="" /></td>
  </tr>
  <tr>
    <td>M.I.:</td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  class="rightcolumn" name="mi" type="text" id="mi" value="" /></td>
  </tr>
  <tr>
    <td>Address:</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="address" type="text" id="address" value="" /></td>
  </tr>
  <tr>
    <td>City:</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="city" type="text" id="city" value="" /></td>
  </tr>
  <tr>
    <td>State:</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="state" type="text" id="state" value="" /></td>
  </tr>
  <tr>
    <td>Zip Code: </td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="zip" type="text" id="zip" value="" /></td>
  </tr>
  <tr>
    <td>Phone:</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="phone" type="text" id="phone" value="" /></td>
  </tr>
  <tr>
    <td>How long have you lived at the above address? </td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="addressage" type="text" id="addressage" value="" /></td>
  </tr>
  <tr>
    <td>Are you 18 years or older? </td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="age18" type="radio" value="yes"  />
NO
<input name="age18" type="radio" value="no" checked="checked" /></td>
  </tr>
  <tr>
    <td>If not, state date of birth: </td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  class="rightcolumn" name="dob" type="text" id="dob" value="" /></td>
  </tr>
  <tr>
    <td>If under age 18, how many hours per week are you employed elsewhere?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  class="rightcolumn" name="hourselsewhere" type="text" id="hourselsewhere" value="" /></td>
  </tr>
  <tr>
    <td>Have you had any name changes this employer should know about in order to verify job or education history?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">
      YES
      <input name="namechange" type="radio" value="yes"   />
      NO
      <input name="namechange" type="radio" value="no"  checked="checked"  />    </td>
  </tr>
  <tr>
    <td>Previous Name: </td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  class="rightcolumn" name="previousname" type="text" id="previousname" value="" /></td>
  </tr>
  <tr>
    <td>Have you ever been convicted of a crime or felony?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="pastcrime" type="radio" value="yes"   />
NO
<input name="pastcrime" type="radio" value="no"  checked="checked"  /></td>
  </tr>
  <tr>
    <td>If yes, please explain:</td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  class="rightcolumn" name="crimeexplanation" type="text" id="crimeexplanation" value="" /></td>
  </tr>
  <tr>
    <td>Do you have transportation to and from work?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="transportation" type="radio" value="yes"   />
NO
<input name="transportation" type="radio" value="no"  checked="checked"  /></td>
  </tr>
  <tr>
    <td>Are you authorized to work in the U.S.?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="workvisa" type="radio" value="yes"   />
NO
<input name="workvisa" type="radio" value="no"  checked="checked"  /></td>
  </tr>
  <tr>
    <td>Position applied for:</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="position" type="text" id="position" value="" /></td>
  </tr>
  <tr>
    <td>Date you can start:</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="datestart" type="text" id="datestart" value="" /></td>
  </tr>
  <tr>
    <td>Wage desired:</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="wage" type="text" id="wage" value="" /></td>
  </tr>
  <tr>
    <td>Are you applying for:</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">      <select name="applyingfor" id="applyingfor">
      <option selected="selected" value="Full-time">Full-time</option>
      <option   value="Part-time" >Part-time</option>
      <option   value="Temporary">Temporary</option>
      </select>&nbsp;
      <select name="applyingfortime" id="applyingfortime">
        <option   value="Days Only">Days Only</option>
        <option   value="Nights Only">Nights Only</option>
        <option   value="Days/Nights">Days/Nights</option>
      </select></td>
  </tr>
  <tr>
    <td>Who recommended you for this position or how did you hear about the position?</td>
    <td valign="top"><span class="errormark">
      *    </span></td>
    <td valign="top"><input  class="rightcolumn" name="referral" type="text" id="referral" value="" /></td>
  </tr>
</table>
<div align="center"><br />
  <strong>EDUCATION</strong></div>
<table style='width:490px'>
    <tr>
      <td valign="top">SCHOOLING</td>
      <td>NAME AND ADDRESS OF SCHOOL </td>
      <td>GRADE or DEGREE COMPLETED </td>
      <td>GRADUATE YES/NO </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>High School </td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="hsname" type="text" id="hsname" value="" />
      </span></td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="hsgrade" type="text" id="hsgrade" value="" />
      </span></td>
      <td><select name="hsgraduate" id="hsgraduate">
        <option value="Yes"  >Yes</option>
        <option value="No" >No</option>
        <option value="NA"  selected="selected">NA</option>
                        </select></td>
    </tr>
    <tr>
      <td>College or University </td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="collegename" type="text" id="collegename" value="" />
      </span></td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="collegegrade" type="text" id="collegegrade" value="" />
      </span></td>
      <td><select name="collegegraduate" id="collegegraduate">
        <option value="Yes"  >Yes</option>
        <option value="No" >No</option>
        <option value="NA"  selected="selected">NA</option>
      </select></td>
    </tr>
    <tr>
      <td>Others (Specify) </td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="othereducation" type="text" id="othereducation" value="" />
      </span></td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="othereducationgrade" type="text" id="othereducationgrade" value="" />
      </span></td>
      <td><select name="othergraduate" id="othergraduate">
        <option value="Yes"  >Yes</option>
        <option value="No" >No</option>
        <option value="NA"  selected="selected">NA</option>
      </select></td>
    </tr>
    <tr>
      <td>Military Service<br /> 
      Schools Attended </td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="mschool" type="text" id="mschool" value="" />
      </span></td>
      <td><span style="width:50%">
        <input  class="rightcolumn" name="mschoolgrade" type="text" id="mschoolgrade" value="" />
      </span></td>
      <td><select name="militarygraduate" id="militarygraduate">
        <option value="Yes"  >Yes</option>
        <option value="No" >No</option>
        <option value="NA"  selected="selected">NA</option>
      </select></td>
    </tr>
  </table>
<br />
<div align="center"><br />
    <strong>MILITARY SERVICE </strong></div>
<table style="width:490px">
  <tr>
    <td valign="top">War Veteran: </td>
    <td valign="top">&nbsp;</td>
    <td valign="top" style='width:50%'>YES
      <input name="vet" type="radio" value="yes"   />
      NO<input name="vet" type="radio" value="no"  checked="checked"  /></td>
  </tr>
  <tr>
    <td valign="top">Branch:</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">      <input  class="rightcolumn" name="branch" type="text" id="branch" value="" />    </td>
  </tr>
  <tr>
    <td valign="top">Start of Military Service: </td>
    <td valign="top">&nbsp;</td>
    <td valign="top">      <input  class="rightcolumn" name="militarystartdate" type="text" id="militarystartdate" value="" />    </td>
  </tr>
  <tr>
    <td valign="top">End of Military Service: </td>
    <td valign="top">&nbsp;</td>
    <td valign="top">      <input  class="rightcolumn" name="militaryenddate" type="text" id="militaryenddate" value="" />    </td>
  </tr>
  <tr>
    <td valign="top">Highest Grade: </td>
    <td valign="top">&nbsp;</td>
    <td valign="top">      <input  class="rightcolumn" name="highestgrade" type="text" id="highestgrade" value="" />    </td>
  </tr>
</table>
<div align="center"><br />
    <br />
    <strong>PREVIOUS WORK EXPERIENCE</strong><br />
  <span style="font-size:10px;">(List below your last three employers, starting <br />
  with the most recent one first)</span> 
  <br />
  <br />
  <table style="width:490px;text-align:left">
    <tr>
      <td>Company Name: </td>
      <td>&nbsp;</td>
      <td style='width:50%'>        <input  class="rightcolumn" name="companyname" type="text" id="companyname" value="" />      </td>
    </tr>
    <tr>
      <td>Company Address: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyaddress" type="text" id="companyaddress" value="" />      </td>
    </tr>
    <tr>
      <td>Company Phone: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyphone" type="text" id="companyphone" value="" />      </td>
    </tr>
    <tr>
      <td>Your Position: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyposition" type="text" id="companyposition" value="" />      </td>
    </tr>
    <tr>
      <td>Immediate Supervisor: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companysupervisor" type="text" id="companysupervisor" value="" />      </td>
    </tr>
    <tr>
      <td>Title:</td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companytitle" type="text" id="companytitle" value="" />      </td>
    </tr>
    <tr>
      <td>Employment Start Date: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="employmentstartdate" type="text" id="employmentstartdate" value="" />      </td>
    </tr>
    <tr>
      <td>Employment End Date: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="employmentenddate" type="text" id="employmentenddate" value="" />      </td>
    </tr>
    <tr>
      <td>Starting Wage/Salary: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="startingwage" type="text" id="startingwage" value="" />      </td>
    </tr>
    <tr>
      <td>Ending Wage/Salary: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="endingwage" type="text" id="endingwage" value="" />      </td>
    </tr>
    <tr>
      <td>Reason For Leaving: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="reasonforleaving" type="text" id="reasonforleaving" value="" />      </td>
    </tr>
    <tr>
      <td>Job Duties: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="jobtitles" type="text" id="jobtitles" value="" />      </td>
    </tr>
  </table>
  <strong><br />
  2ND MOST RECENT JOB <br />
  <br />
  </strong>
  <table style="width:490px;text-align:left">
    <tr>
      <td>Company Name: </td>
      <td>&nbsp;</td>
      <td style='width:50%'>        <input  class="rightcolumn" name="companyname2" type="text" id="companyname2" value="" />      </td>
    </tr>
    <tr>
      <td>Company Address: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyaddress2" type="text" id="companyaddress2" value="" />      </td>
    </tr>
    <tr>
      <td>Company Phone: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyphone2" type="text" id="companyphone2" value="" />      </td>
    </tr>
    <tr>
      <td>Your Position: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyposition2" type="text" id="companyposition2" value="" />      </td>
    </tr>
    <tr>
      <td>Immediate Supervisor: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companysupervisor2" type="text" id="companysupervisor2" value="" />      </td>
    </tr>
    <tr>
      <td>Title:</td>
      <td>&nbsp;</td>
      <td><input  class="rightcolumn" name="companytitle2" type="text" id="companytitle2" value="" /></td>
    </tr>
    <tr>
      <td>Employment Start Date: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="employmentstartdate2" type="text" id="employmentstartdate2" value="" />      </td>
    </tr>
    <tr>
      <td>Employment End Date: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="employmentenddate2" type="text" id="employmentenddate2" value="" />      </td>
    </tr>
    <tr>
      <td>Starting Wage/Salary: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="startingwage2" type="text" id="startingwage2" value="" />      </td>
    </tr>
    <tr>
      <td>Ending Wage/Salary: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="endingwage2" type="text" id="endingwage2" value="" />      </td>
    </tr>
    <tr>
      <td>Reason For Leaving: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="reasonforleaving2" type="text" id="reasonforleaving2" value="" />      </td>
    </tr>
    <tr>
      <td>Job Duties: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="jobtitles2" type="text" id="jobtitles2" value="" />      </td>
    </tr>
  </table>
  <br />
  <strong>3RD MOST RECENT JOB</strong><br />
  <br />
  <table style='width:490px;text-align:left'>
    <tr>
      <td>Company Name: </td>
      <td>&nbsp;</td>
      <td style='width:50%'>        <input  class="rightcolumn" name="companyname3" type="text" id="companyname3" value="" />      </td>
    </tr>
    <tr>
      <td>Company Address: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyaddress3" type="text" id="companyaddress3" value="" />      </td>
    </tr>
    <tr>
      <td>Company Phone: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyphone3" type="text" id="companyphone3" value="" />      </td>
    </tr>
    <tr>
      <td>Your Position: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companyposition3" type="text" id="companyposition3" value="" />      </td>
    </tr>
    <tr>
      <td>Immediate Supervisor: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companysupervisor3" type="text" id="companysupervisor3" value="" />      </td>
    </tr>
    <tr>
      <td>Title:</td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="companytitle3" type="text" id="companytitle3" value="" />      </td>
    </tr>
    <tr>
      <td>Employment Start Date: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="employmentstartdate3" type="text" id="employmentstartdate3" value="" />      </td>
    </tr>
    <tr>
      <td>Employment End Date: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="employmentenddate3" type="text" id="employmentenddate3" value="" />      </td>
    </tr>
    <tr>
      <td>Starting Wage/Salary: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="startingwage3" type="text" id="startingwage3" value="" />      </td>
    </tr>
    <tr>
      <td>Ending Wage/Salary: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="endingwage3" type="text" id="endingwage3" value="" />      </td>
    </tr>
    <tr>
      <td>Reason For Leaving: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="reasonforleaving3" type="text" id="reasonforleaving3" value="" />      </td>
    </tr>
    <tr>
      <td>Job Duties: </td>
      <td>&nbsp;</td>
      <td>        <input  class="rightcolumn" name="jobtitles3" type="text" id="jobtitles3" value="" />      </td>
    </tr>
  </table>
  <br />
</div>
<div align="center"><br />
  <strong>FINAL INFORMATION </strong><br />
  <br />
</div>
<table style='width:490px;'>
  <tr>
    <td>Are there any job duties that you would be unable to perform?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top" style="width:30%"><input  class="rightcolumn" name="avoidjobs" type="text" id="avoidjobs" value="" /></td>
  </tr>
  <tr>
    <td>Is there anything we could do to accommodate you so you could perform all the required job duties?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  class="rightcolumn" name="needhelp" type="text" id="needhelp" value="" /></td>
  </tr>
  <tr>
    <td>Have you ever applied to this company before?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="appliedbefore" type="radio" value="yes"   />
NO
<input name="appliedbefore" type="radio" value="no"  checked="checked"  /></td>
  </tr>
  <tr>
    <td>If yes, when?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top"><input  class="rightcolumn" name="whenappliedbefore" type="text" id="whenappliedbefore" value="" /></td>
  </tr>
  <tr>
    <td>Are you or an immediate family member(s) currently employed, or have ever been employed in the past, with this company?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="companyconnection" type="radio" value="yes"   />
NO
<input name="companyconnection" type="radio" value="no"  checked="checked"  /></td>
  </tr>
  <tr>
    <td>Are you now employed?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="employed" type="radio" value="yes"   />
NO
<input name="employed" type="radio" value="no"  checked="checked"  /></td>
  </tr>
  <tr>
    <td>If yes, may we contact you current employer?</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">YES
      <input name="contactemployer" type="radio" value="yes"  checked="checked" />
NO
<input name="contactemployer" type="radio" value="no"    /></td>
  </tr>
  
    <tr>
    <td nowrap>Match the Security Image:<strong><font color="#FF0000">*</font></strong> </td>
    <td valign="top">&nbsp;</td>
    <td><input type="text" name="validator" id="validator" size="4" />&nbsp;<img src="/wp-content/themes/parallelus-traject/includes/formengines/random2.php" alt="CAPTCHA image" width="60" height="20" vspace="1" align="top" /><input type='hidden' name='token' value='1564fc127c03d5d448a4e11be9296371' /></td>
  </tr>
  
</table>
<br />
<div style="font-size:10px">1. I authorize investigation of all statements contained in this application.<br />
2. I understand that misrepresentation or omission of facts called for is cause for dismissal and that my employment is substantially dependent on truthful answers to the<br />
forgoing inquires.<br />
3. I have read these statements and answers to these inquires.  YES
      <input name="readstatements" type="radio" value="yes"  checked="checked" />
NO
<input name="readstatements" type="radio" value="no"    />
</div>
<div align="center">

  <br /><input type="submit" name="submit" value="Submit Application" />
  <br />
  <br />
  Wilderness at the Smokies &ndash; Application for Employment</div>
</form></div>

       
    
</body>
</html>

<?php get_header(); ?>

	<br class="clear"/>
	<div id="ch"><!--clear header--></div>
	<div id="container">
		<div class="bread_crumbs_wrap">
			<div class="inner max">
				<div id="bread_crumbs"><?php the_breadcrumb(); ?></div>
			</div>
		</div>

		<div id="resWidgetTopSub">
			<div class="inner max"><?php include( TEMPLATEPATH . '/res_top.php' ); ?></div>
		</div>
		<div id="pageSlider">
			<?php echo do_shortcode( '[rev_slider MeetingsEvents]' ); ?>
		</div>
		<?php include( TEMPLATEPATH . '/slider_hub_bar.php' ); ?>

		<div class="span12" id="page_wrap">
			<?php if ( $post->post_parent == 4799 && get_the_ID() != 223 ) {
				include( TEMPLATEPATH . '/meetings_nav.php' );
			} ?>


			<div id="container" class="light shadow inner max cf">
				<h1><?php echo get_the_title(); ?></h1>


				<?php echo apply_filters( 'the_content', get_the_content() ); ?>
			</div>
		</div>

		<div class="span12 cf" id="page_wrap">
			<div class="inner max cf">

				<?php if ( have_rows( 'school_outings_repeater' ) ):
					while ( have_rows( 'school_outings_repeater' ) ) : the_row(); ?>
						<div class="span4 col colDarkBox cf">
							<div class="block_thumb full_width_image">
								<a href=" <?php the_sub_field('school_url'); ?>"><img src="<?php the_sub_field('school_image'); ?>" width="388" height="190"/></a>
							</div>
							<div class="waterpark_wrap">
								<h4><a href="<?php the_sub_field('school_url'); ?>"><?php the_sub_field('school_title'); ?></a></h4>

								<div class="waterpark_text"><?php the_sub_field('school_content'); ?></div>
								<br class="cf"/>
								<a href="<?php the_sub_field('school_url'); ?>" class="view_more">More Info</a>
							</div>
						</div>

					<?php endwhile;
				endif; ?>
			</div>
		</div>


	</div>
<?php get_footer(); ?>
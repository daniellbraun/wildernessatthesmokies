<?php 
/*
Template Name: Mobile Redirect
*/
?>
<?php get_header(); ?>

	<br class="clear"/>
	<div id="ch"><!--clear header--></div>
	<div id="container">
		<div class="span12" id="page_wrap">

			<div id="container" class="light shadow inner max cf">

				<? if ( is_page( '223' ) ) {

					$my_id        = 6161;
					$post_id_6161 = get_post( $my_id );
					$content      = $post_id_6161->post_content;
					$content      = apply_filters( 'the_content', $content );
					$content      = str_replace( ']]>', ']]>', $content );
					echo $content;

					$rows = get_field( 'listing_format', 6161 );
					if ( $rows ):
						foreach ( $rows as $row ): ?>
							<div class="span12 cf">
								<div class="span3 col cf full_width_image"><img src="<? echo $row['image']["url"]; ?>"/></div>
								<div class="span8 col cf">
									<h2><? echo $row['title']; ?></h2>

									<p><? echo $row['content']; ?></p>
								</div>
							</div>
							<div id="container" class="cf">
								<hr/>
							</div>
						<?
						endforeach;
					endif;


				} else {
					echo apply_filters( 'the_content', get_the_content() );
				}
				?>

				<?
				$rows = get_field( 'listing_format' );
				if ( $rows ):
					foreach ( $rows as $row ): ?>
						<div class="span12 cf">
							<div class="span3 col cf full_width_image"><img src="<? echo $row['image']["url"]; ?>"/></div>
							<div class="span8 col cf">
								<h2><? echo $row['title']; ?></h2>

								<p><? echo $row['content']; ?></p>
							</div>
						</div>
						<div id="container" class="cf">
							<hr/>
						</div>
					<?
					endforeach;
				endif;
				?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
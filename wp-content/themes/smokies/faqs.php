<?php 
/*
Template Name: FAQ'S
*/
?>
<?php get_header(); ?>
<?
function faqs($cat){	

$args = array( 'post_type' => 'faq');
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post();	


	$rows = get_field('faqs');
	if($rows):

	foreach($rows as $row):
	$value = $row['category'];
	if ( in_array($cat,$value, TRUE) ):
	$question = '<h2><span>Q : </span>'.$row['question'] . '</h2>';
	$answer =  '<span>A : </span>'.$row['answer'];
	
	echo '<dt><a href="">' . $question . '</a></dt>';
	echo  '<dd>' . $answer . '</dd>';
	endif;
	endforeach;
	endif;
	endwhile; wp_reset_query();

}
?>


<br class="clear" />
<div id="ch"><!--clear header--></div>

<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>

<div class="span12 cf" id="page_wrap">
<div class="inner cf max">	
<h1><? echo get_the_title(); ?></h1>
</div>
<div class="inner cf max">	
<h1>Reservations</h1>
<dl class="accordion">
<? echo faqs('Reservation Questions'); ?>
</dl>
<h1>Waterpark</h1>
<dl class="accordion">
<? echo faqs('Waterpark Questions'); ?>
</dl>
<h1>General</h1>
<dl class="accordion">
<? echo faqs('General Questions'); ?>
</dl>
</div>
</div>

<?php get_footer(); ?>
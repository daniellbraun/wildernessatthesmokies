<div id="footer" class="cf <?php if ( is_page( 'adventureforest' ) || $post->post_parent == 5902 ) {
	echo 'af_footer';
} ?>">
	<div class="inner max cf">
		<div class="span4 col footercol">
			<div id="gift_card"><span>Gift Cards</span><a href="/resort-info/order-gift-cards">Buy Now &rsaquo;</a>
			</div>
			<div class="trip-advisor"><img src="/wp-content/themes/smokies/images/TA-award-wats-small.png"/></div>
		</div>
		<div class="span4 col footercol">
			<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/images/footer_logo.png"/>

			<div itemscope itemtype="http://schema.org/LocalBusiness">
				<h4><span itemprop="name">Wilderness at the Smokies</span></h4>

				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<span itemprop="streetAddress">1424 Old Knoxville Hwy | </span>
					<span itemprop="addressLocality">Sevierville</span>,
					<span itemprop="addressRegion">TN</span>
				</div>
				Phone: <span itemprop="telephone">(865) 429-0625</span><br/>
				Reservations: <span itemprop="telephone">(877) 325-9453</span>
			</div>

		</div>
		<div class="span4 col footercol">
			<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/images/servierville.png"/>
			<br/>

			<div id="checkusout">
				<a href="http://www.wildernessresort.com" id="checkit" target="_blank">
					<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/images/dells_logo.png"/>
					<h4>Check Us Out In Wisconsin Dells</h4>
				</a>
			</div>
		</div>
	</div>
</div>

<div id="footercap" <?php if ( is_page( 'adventureforest' ) || $post->post_parent == 5902 ) {
	echo 'class="af_footer"';
} ?>>
	<div class="inner max cf">
		<div class="span3 copy">&copy; <?php echo date( 'Y' ); ?> Wilderness at the Smokies</div>
		<div class="span6" id="footer-menu"><?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?></div>
		<div class="span3 copy">Website by <a href="http://www.adlitwebsolutions.com/">Ad-Lit Web Solutions</a></div>
	</div>
</div>

<div id="ttdUniversalPixelTag54ccc87698de4a46a5aa01f0e461dbb2" style="display:none">
	<script src="https://js.adsrvr.org/up_loader.1.1.0.js" type="text/javascript"></script>
	<script type="text/javascript">
		(function(global) {
			if (typeof TTDUniversalPixelApi === 'function') {
				var universalPixelApi = new TTDUniversalPixelApi();
				universalPixelApi.init("muxvg3j", ["5teo8f8"], "https://insight.adsrvr.org/track/up", "ttdUniversalPixelTag54ccc87698de4a46a5aa01f0e461dbb2");
			}
		})(this);
	</script>
</div>
<?php wp_footer(); ?>
<?php
wp_enqueue_script( 'jquery' );
wp_enqueue_script( 'jquery-form' );
wp_enqueue_script( 'bgiframe', '/' . PLUGINDIR . '/' . $plugin_dir . '/js/jquery.bgiframe.js', array( 'jquery' ) );
wp_enqueue_script( 'datePicker', '/' . PLUGINDIR . '/' . $plugin_dir . '/js/jquery.datePicker.js', array( 'jquery' ) );
wp_enqueue_script( 'textarearesizer', '/' . PLUGINDIR . '/' . $plugin_dir . '/js/jquery.textarearesizer.js', array( 'jquery' ) );
if ( strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post-new.php' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post.php' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/page-new.php' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/page.php' ) || ( is_object( $post ) && $post->post_type == 'page' ) ) :
	wp_enqueue_script( 'date', '/' . PLUGINDIR . '/' . $plugin_dir . '/js/date.js', array( 'jquery' ) );
	wp_enqueue_script( 'editor' );
	wp_enqueue_script( 'quicktags' );

endif;
?></div>
</body>
</html>
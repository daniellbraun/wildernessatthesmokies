<script>
function OnSubmitForm()

{

  //urchinTracker('/outgoing/sidebar_reservation_start');
  var aa = document.getElementById('availabilityform');
  aa.submit();

}

function cUpper(cObj)
{
cObj.value=cObj.value.toUpperCase();
}

function createCookie(name,value,seconds) {
	if (seconds) {
		var date = new Date();
		date.setTime(date.getTime()+(seconds));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=www.wildernessatthesmokies.com";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function OnSubmitFormNew() {
	
	//window.alert("Our Online Booking System is down for maintenance. We apologize for the inconvenience. Please check back later.");
	
	//event.preventDefault();
	
	var aa=document.getElementById('availabilityform');
	var actionval = jQuery("#ctl00_ContentPlaceHolder1_ucFetchCalendar_ddlSpecialRates").val();
	jQuery(aa).attr("action",actionval);
	
	var date1 = jQuery("#dpd1").val();
	var date2 = jQuery("#dpd2").val();
	
	date1 = date1.replace(/\b0(?=\d)/g, '')
	date2 = date2.replace(/\b0(?=\d)/g, '')
	
	jQuery("#dpd1").val(date1);
	jQuery("#dpd2").val(date2);
	
	aa.submit();
}

</script>

<div id="resResponsive" class="span12 cf">
<?php $tomorrow = new DateTime('tomorrow'); 
$reservationurl = "https://sh-secure.wildernessatthesmokies.com/?checkInDate=". date("m/d/Y") ."&checkOutDate=" . $tomorrow->format('m/d/Y');
$redirecturl = "https://www.wildernessatthesmokies.com/mobile-booking-redirect";
?>
<!-- <div class="span6 col res_btn"><a onclick='event.preventDefault();window.alert("Our Online Booking System is down for maintenance. We apologize for the inconvenience. Please check back later.")' href="<? echo $reservationurl; ?>">Book Now</a></div>-->
<div class="span6 col res_btn"><a href="<?php echo $redirecturl; ?>">Book Now</a></div>
<div class="span6 col res_btn"><a href="tel:(877) 325-9453">Call</a></div>
</div>

<div id="resForm" class="span12 cf">
<div id="book" class="span2 col">Book<span> Your stay</span></div>
<form action="https://b4c.wildernessatthesmokies.com/B4CheckinBNWebServiceSHT/B4CheckinBNWebService.asmx" method="GET" name="aspnetForm" id="availabilityform"  onsubmit='return false'>
   <div class="span2 col"><label for="dpd1">Check in<br />
<!--<input type="text"  value="<?php /*print date("m/d/Y"); */?>" id="dpd1" name="checkInDate"  readonly="true">-->
 <input type="text"  value="<?php print date("m/d/Y"); ?>" id="dpd1" name="ArrivalDate"  readonly="true">
 </label></div>
  <div class="span2 col"> <label for="dpd2" >Check out<br />
<!--<input type="text"  name="checkOutDate"  value="<?php /*$tomorrow = mktime(0,0,0,date("m"),date("d")+1,date("Y")); echo date("m/d/Y", $tomorrow);*/?>" id="dpd2" readonly="true">-->
<input type="text"  name="DepartureDate"  value="<?php $tomorrow = mktime(0,0,0,date("m"),date("d")+1,date("Y")); echo date("m/d/Y", $tomorrow);?>" id="dpd2" readonly="true">
            </label></div>
    <div class="span2 col">
 	 <label for="ctl00_ContentPlaceHolder1_ucFetchCalendar_ddlSpecialRates"><span id="promo">Property Type</span><br/>       
<!--     <select name="special-rate" id="ctl00_ContentPlaceHolder1_ucFetchCalendar_ddlSpecialRates" class="custom-select">
        <option value="https://sh-secure.wildernessatthesmokies.com/" selected="selected">Stone Hill Lodge</option>
        <option value="https://rl-secure.wildernessatthesmokies.com/" >River Lodge</option>
        <option value="https://rl-secure.wildernessatthesmokies.com/">Sanctuary Villas</option>
	 </select>-->
         <select name="special-rate" id="ctl00_ContentPlaceHolder1_ucFetchCalendar_ddlSpecialRates" class="custom-select">
             <option value="https://sh-secure.wildernessatthesmokies.com/wildernessatthesmokies-sh" selected="selected">Stone Hill Lodge</option>
             <option value="https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl" >River Lodge</option>
             <option value="https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl">Sanctuary Villas</option>
         </select>
     </label></div>
       
    <div class="span1 col"><label for="codebox">Code<br />       
<!--  	<input id="codebox" class="span2 col" name="CID" type="text"  value="" onkeyup="return cUpper(this);"  style="text-transform:uppercase;">-->
        <input id="codebox" class="span2 col" name="RatePlanCode" type="text"  value="" onkeyup="return cUpper(this);"  style="text-transform:uppercase;">
</label></div>
    
    <input type='hidden' name='hp' value='0' />
	 </form>

    <input type=hidden DISABLED id="m1" value="11">
    <input type=hidden DISABLED id="d1" value="30">
    <input type=hidden DISABLED id="m2" value="12">
    <input type=hidden DISABLED id="d2" value="31">
    <input type='hidden' name='hotelCode' id='hotelCode' />
<div id='book_btn_container' class="span2 col"><a id="book_btn" href="javascript:OnSubmitFormNew();"><span>Room Search</span></a></div>
</div>
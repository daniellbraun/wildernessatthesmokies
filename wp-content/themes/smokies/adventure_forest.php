<?php 
/*
Template Name: Adventure Forest
*/
?>
<?php get_header(); ?>

<div id="container">
<div id="ch"><!--clear header--></div>
<div id="homeSlider">

<?php echo do_shortcode('[rev_slider adventureforest]') ?>
</div> 
<div id="container">
<div id="adventure_forest_wrap">
<div id="landing">
<?php include (TEMPLATEPATH . '/slider_hub_bar.php'); ?>
</div>


<div id="afhomeSec1" class="fws"> 
<div class="inner max cf">
  
     <div id="af_home" class="span6 col">
     
	 <h1 style="text-align:left"><? echo get_the_title(); ?></h1> 
	<? echo apply_filters('the_content',  get_the_content()); ?>
     </div>
     
     
        <div id="attraction_pricing" class="span3 col">
     <h4>Prices &amp; Passes</h4>
     
      <div id="payperplay">Day <span>Or</span> Stay <br/></div>
     <strong>Unlimited Adventures</strong> - Over 50% Savings.
     <div class="af"><a href="/adventureforest/prices-passes" class="view_more">View Pricing</a></div>
		<br  />
        <div class="full_width_image" style="max-width:260px; height:auto;!important"><a href="/adventureforest/prices-passes"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/passesLink.png" /></a></div>       
        </div>
      
     <div class="span3 col" id="last_home_col">
     
     <h4>Parties &amp; Groups</h4>
     <div id="payperplay">Party<span> Time </span><br/></div>
     <strong>Birthdays</strong> and more!
     <div class="af"><a href="/adventureforest/parties-groups" class="view_more">More Info</a></div>
     
     </div>
 </div>
 </div>     
 
 <div id="homeSec2" class="fws grey cf">
 
	<div class="inner max cf"> 
	 <h3 id="af_carouselTitle"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/theAdventure.png" /></h3>
	<?php echo attractionsCarousel(); ?></div>
  	</div>
  
  <div id="homeSec3" class="fws white">
	<div  id="social_feeds" class="inner max cf af">
    
    </div>
  </div>
 </div> </div> </div>
<?php get_footer(); ?>

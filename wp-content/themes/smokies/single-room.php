<?php get_header(); ?>
<?php $meta_value = $_GET['r']; 
$reservationurl = "https://sh-secure.wildernessatthesmokies.com/wildernessatthesmokies-sh";
if ($_GET['rt']=='rl') {
	$reservationurl = "https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl";
} 
if ($_GET['rt']=='vl') {
	$reservationurl = "https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl";
} 


?>
<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb();  ?> </div></div></div>
<div id="resWidgetTopSub"><div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div><br class="clear" />
</div>

<div class="span12 cf" id="page_wrap">
<div id="container" class="inner max cf">
<h1>Room Details</h1>
<div class="back_btn"> <a  href="/lodging" > &lsaquo; &nbsp; View all Lodging </a></div>
</div>
<div class="span12">
<div class="inner max cf">

<div class="span9 col" id="content">
<div id="room-content">

<?php 
roomSingle($meta_value); 
?>



</div>
<br class="clear" />
</div>
<div class="span3 col">
<div class="span12 col dark " style=" text-align:center"><h5>Call to Reserve Now</h5>Reservations & Resort Info<span class="phone" >(877) 325-9453</span></div>
<div class="span12 col">

<div class="span12 dark" >
<h5>Resort Policies</h5>
<?php include (TEMPLATEPATH . '/resort_policies.php'); ?>
</div>
<div class="span12">


</div>
</div>
</div>
</div>


</div></div></div>
<?php get_footer(); ?>
<?php 
/*
Template Name: Attractions
*/
?>
<?php get_header(); ?>
<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap <? if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'af';} ?>"><div class="inner max"><div id="bread_crumbs" <? if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'class="af"';} ?>><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub" <? if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'class="af"';} ?>>
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
<br class="clear" />
</div>
<div id="pageSlider">
<?php echo do_shortcode('[rev_slider adventureforest]') ?>
</div>
<?php include (TEMPLATEPATH . '/slider_hub_bar.php'); ?>
<div class="span12" id="page_wrap">

<div id="container" class=" inner max cf attraction-prices" >
<h1><? echo get_the_title(); ?></h1> 
<? echo get_the_content(); ?>
<?  attractions(); ?>
</div>
</div></div>
<?php get_footer(); ?>
<?php

/**
 * Super-simple, minimum abstraction MailChimp API v2 wrapper
 * 
 * Requires curl (I know, right?)
 * This probably has more comments than code.
 * 
 * @author Drew McLellan <drew.mclellan@gmail.com>
 * @version 1.0
 */
class MailChimp
{
	private $api_key;
	private $api_endpoint = 'https://<dc>.api.mailchimp.com/2.0/';



	/**
	 * Create a new instance
	 * @param string $api_key Your MailChimp API key
	 */
	function __construct($api_key)
	{
		$this->api_key = $api_key;
		list(, $datacentre) = explode('-', $api_key);
		$this->api_endpoint = str_replace('<dc>', $datacentre, $this->api_endpoint);
	}




	/**
	 * Call an API method. Every request needs the API key, so that is added automatically -- you don't need to pass it in.
	 * @param  string $method The API method to call, e.g. 'lists/list'
	 * @param  array  $args   An array of arguments to pass to the method. Will be json-encoded for you.
	 * @return array          Associative array of json decoded API response.
	 */
	public function call($method, $args=array())
	{
		return $this->_raw_request($method, $args);
	}




	/**
	 * Performs the underlying HTTP request. Not very exciting
	 * @param  string $method The API method to be called
	 * @param  array  $args   Assoc array of parameters to be passed
	 * @return array          Assoc array of decoded result
	 */
	private function _raw_request($method, $args=array())
	{      
		$args['apikey'] = $this->api_key;

		$url = $this->api_endpoint.'/'.$method.'.json';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
		$result = curl_exec($ch);
		curl_close($ch);

		//echo "url was ".$url."<br />";
		//echo "post vars are ".json_encode($args)."<br />";

		return $result ? json_decode($result, true) : false;
	}

}

function checkifokemail($email) {
	if (!filter_var( $email, FILTER_VALIDATE_EMAIL )) {
		return;	
	}
	
	if (!valid_domain($email)) {
		return;
	}
	
	return 1;
}

function valid_domain($str)
{
 list($mailbox, $domain) = split('@', $str);
 if (!(checkdnsrr($domain, 'MX') || checkdnsrr($domain, 'A'))) {
 	return FALSE;
 }
 return TRUE;
}


mysql_connect("localhost","wildsmok_wpsmoki","^@]%ptn$+-V2");
mysql_select_db("wildsmok_wpsmokies");

$email = get_most_recent_email();

if (!$email) {die();}

//echo "email = ".$email."<br />";

	 $chimpy = new MailChimp("1f59262b4bff589c6582107d814c5d44-us7");



$args =array(
		"id"=>"da3bb28806",
		"double_optin"=>false,
		"email"=>array(
			"email"=>$email
		)
	);
	
/*echo "args = ";
print_r($args);*/

	$result = $chimpy->call("lists/subscribe",$args,1);
	//echo "<pre>"."result = ".print_r($result)."</pre>";
	
	//echo "success count = ".$result['success_count'];

function checkval($val) {
	
	foreach($val as $v) {
		
		if ($v['slug']=='email') {$email = $v['value'];}
		if ($v['slug']=='wilderness-at-the-smokies-newsletter' && $v['value']) {$unlock = true;}
		
				
		
		/*foreach ($v as $vv) {
			if ($vv==	
		}*/
	}
	
	//echo "unlock = $unlock";
	//echo "<br />";
	//echo "email = $email";
	
	if ($unlock && $email) {return $email;}
	
}

function get_most_recent_email() {
	$sql = "SELECT data
FROM `wp_visual_form_builder_entries`
ORDER BY `wp_visual_form_builder_entries`.`entries_id` DESC
LIMIT 0 , 1";
	$result = mysql_query($sql);
	
	
	
	if (!mysql_num_rows($result)) {return;}
	
	
	$row = mysql_fetch_array($result);
	
	$val = unserialize ($row[0]);
	
	
	$email = checkval($val);
		
	
	return $email;
	//print_r($val);
	
	
}

?>
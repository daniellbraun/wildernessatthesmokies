<?php get_header(); ?>

<br class="clear"/>
<div id="ch"><!--clear header--></div>
<div id="container">
    <div class="bread_crumbs_wrap>">
        <div class="inner max">
            <div id="bread_crumbs"><?php the_breadcrumb(); ?></div>
        </div>
    </div>

    <div id="resWidgetTopSub">
        <div class="inner max"><?php include TEMPLATEPATH . '/res_top.php'; ?></div>
    </div>
    <div id="pageSlider">
		<?php echo do_shortcode('[rev_slider waterparkslider]') ?>
    </div>
	<?php include TEMPLATEPATH . '/slider_hub_bar.php'; ?>

    <div class="span12" id="page_wrap">


        <div id="container" class="light shadow inner max cf employment-page">
            <h1><?php echo get_the_title(); ?></h1>

			<?php if ( have_posts() ): while ( have_posts() ): the_post();
				the_content();
			endwhile;

			else: ?>
                <p>Sorry, no posts matched your criteria.</p>
			<?php endif; ?>


            <input type="text" id="datepicker" class="ticket-datepicker"> <span class="loading"></span>

            <div class="woocommerce ticket-product-result">

                <div class="ticket-title"></div>

                <div class="ticket-price"></div>

                <form class="cart" method="post" enctype="multipart/form-data">

                    <div class="quantity">
                        <div class="quantity-warning"></div>
                        <label for="add-to-cart">Quantity
                        <input type="number" step="1" min="1" max="" name="quantity" value="1" title="Quantity"
                               class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">
                        </label>
                    </div>

                    <div>
                        <button type="submit" class="single_add_to_cart_button button alt">
                            Add To Cart
                        </button>
                    </div>

                    <input id="product_id" type="hidden" name="add-to-cart" value="">

                </form>
            </div>

            <?php $terms = get_field('terms_of_purchase');
                echo '<span style="font-size: 0.8em;">' . wpautop($terms) . '</span>';
            ?>

        </div>

    </div>


</div>
<?php get_footer(); ?>

<script>
    const picker = new Pikaday({
        field: document.getElementById('datepicker'),
        defaultDate: new Date("2018-04-29"),
        minDate: new Date("2018-04-29"),
        maxDate: new Date("2018-09-04"),
        format: 'MMMM D, YYYY',
        onSelect: function () {
            const dateToSearch = this.getMoment().format('YYYYMMDD');
            getTicketProduct(dateToSearch).done((result) => {
                populateDOM(result);
            });

        }
    });

    const ajax_url = "/wp-admin/admin-ajax.php";

    function getTicketProduct(dateToSearch) {
        const loading = document.querySelector('.loading');
        const ticketProductResult = document.querySelector('.ticket-product-result');
        return jQuery.ajax({
            url: ajax_url,
            type: "POST",
            data: {action: "retrieve_ticket", ticket_date: dateToSearch},
            beforeSend: () => {
                loading.textContent = "Searching...";
                ticketProductResult.classList.remove("show-result");
            }
        }).always(function () {
            loading.textContent = "";
        });
    }

    let ticketsRemaining;
    const quantity_warning = document.querySelector('.quantity-warning');
    function populateDOM(obj) {

        ticketsRemaining = obj.remaining;

        const loading = document.querySelector('.loading');
        if (ticketsRemaining < 1) {
            loading.textContent = "Sorry, there are no more tickets for this date.";
            return;
        }

        const ticketProductResult = document.querySelector('.ticket-product-result');
        const title = document.querySelector('.ticket-title');
        const price = document.querySelector('.ticket-price');
        const productId = document.querySelector('#product_id');

        const qty_input = document.querySelector('.qty');

        title.textContent = obj.title;
        price.textContent = `$${obj.price}`;
        productId.value = obj.id;

        ticketProductResult.classList.add("show-result");

        // reset button and qty
        qty_input.value = 1;
        jQuery(".single_add_to_cart_button").prop("disabled",false);

    }
    jQuery(".qty").on('input', function() {

        selectedQty = jQuery(this).val();

        if (selectedQty > ticketsRemaining) {

            jQuery(".single_add_to_cart_button").prop("disabled",true);
            jQuery(this).val(ticketsRemaining);
            quantity_warning.textContent = `Sorry, only ${ticketsRemaining} ${(ticketsRemaining > 1) ? 'tickets' : 'ticket'} left for this date!`;
            quantity_warning.classList.add("show-warning");
            setTimeout( () => {
                quantity_warning.classList.remove("show-warning");
            }, 3000)

        } else {
            jQuery(".single_add_to_cart_button").prop("disabled",false);
        }
    })


</script>
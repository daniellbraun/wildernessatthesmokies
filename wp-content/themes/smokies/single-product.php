<?php get_header();?>

<br class="clear" />
<div id="ch"><!--clear header--></div>

<div id="container">
  <div class="bread_crumbs_wrap"><div class="inner max">
    <div id="bread_crumbs">
      <?php the_breadcrumb();?>
    </div>
  </div>
</div>

<div id="resWidgetTopSub">
  <div class="inner max">
    <?php include TEMPLATEPATH . '/res_top.php';?>
  </div>
  <br class="clear" />
</div>

<div class="span12 cf" id="page_wrap">

  <div class="span12">
    <div class="inner max cf">
      <div class="span9 col" id="content">
        <br class="clear" />

        <?php

        if (have_posts()):
            while (have_posts()):
                the_post();?>
                <h1><?php the_title();?></h1>
                <?php
                the_content();
            endwhile;
        else:
        endif;

        ?>


      </div>
    </div>
  </div>

<?php get_footer();?>

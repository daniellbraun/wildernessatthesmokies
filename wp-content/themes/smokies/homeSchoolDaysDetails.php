<?php 
/*
Template Name: Home School Days Detail
*/
?>
<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<!--<div class="bread_crumbs_wrap <? if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'af';} ?>"><div class="inner max"><div id="bread_crumbs" <? if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'class="af"';} ?>><?php // the_breadcrumb(); ?></div></div></div>-->
<div class="clear"></div>
<div id="resWidgetTopSub">
<div class="inner max" style="margin-top:10px;"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>


<div class="span12" id="page_wrap">
<? if ($post->post_parent == 4799 && get_the_ID() != 223) {include (TEMPLATEPATH . '/meetings_nav.php'); } ?>


<div id="container" class="light shadow inner max cf homeSchool">
<h1><? echo get_the_title(); ?></h1>
<div class="back_btn"> <a  href="/home-school-days" > &lsaquo; &nbsp;  Back to Home School Days </a></div>

<div class="inner max cf full_width_image">
<?
		$post_id = get_the_ID(); 
		$url = get_permalink();
		$image = get_the_post_thumbnail( $post_id,'specials'); 
		if(!$image):
		$image = '<img src="'.get_bloginfo('template_directory').'/images/special_placeholder.jpg" />';
		endif; 
		echo $image;
?>
</div>






<? if  (is_page('223')){

$my_id = 6161;
$post_id_6161 = get_post($my_id);
$content = $post_id_6161->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo $content;

$rows = get_field('listing_format',6161);
if($rows):
foreach($rows as $row):	?>
<div class="span12 cf">
<div class="span3 col cf full_width_image"><img src="<? echo $row['image']["url"]; ?>" /></div>
<div class="span8 col cf">
<h2><? echo $row['title']; ?></h2>
<p><? echo $row['content']; ?></p>
</div>
</div>
<div id="container" class="cf"><hr /></div>
<?
endforeach;
endif;


	}else{
echo apply_filters('the_content',  get_the_content()); 
}
?>

<?
$rows = get_field('listing_format');
if($rows):
foreach($rows as $row):	?>
<div class="span12 cf">
<div class="span3 col cf full_width_image"><img src="<? echo $row['image']["url"]; ?>" /></div>
<div class="span8 col cf">
<h2><? echo $row['title']; ?></h2>
<p><? echo $row['content']; ?></p>
</div>
</div>
<div id="container" class="cf"><hr /></div>
<?
endforeach;
endif;
?>
</div>
</div>

<div id="container">
<br class="cf"  />
<?php if (is_page('4799') || $post->post_parent == 4799 ){  }else{ ?>
<div id="homeSec3" class="fws cf white">
<div  id="social_feeds" class="inner max cf"><!-- Socials Loaded with ajax--></div>
</div>
<?php } ?>
</div></div>
<?php get_footer(); ?>
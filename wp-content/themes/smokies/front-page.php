<?php get_header(); ?>

<div id="container">
	<div id="ch"><!--clear header--></div>
	<div id="homeSlider">
		<?php echo do_shortcode( '[rev_slider main]' ) ?>
	</div>


	<!-- Remove after Dec 4th. Can also remove .menu-footer-menu-container.closed-alert style in style.css. -->
	<div id="footercap">
		<div class="inner max cf">

			<div class="span12" id="footer-menu">
				<div class="menu-footer-menu-container closed-alert">
				</div>
			</div>

		</div>
	</div>


	<div id="landing">
		<?php include( TEMPLATEPATH . '/slider_hub_bar.php' ); ?>
	</div>
	<div id="homeSec1" class="fws white">        
		<div class="inner max cf">
			<div class="span6 col" id="home_content"><?php echo homeContent(); ?></div>
			<div class="span6 col">

				<div class="span6 col pixelated" id="home_last_col">
					<h4>Waterparks &amp; Attractions</h4>
					<br/>

					<div id="waterpark_logo_home"><a href="/waterparks">
							<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/images/wp_home.png"/></a>
					</div>
					<div id="af_logo_home"><a href="/adventureforest">
							<img src="<?php echo get_bloginfo( 'template_directory' ); ?>/images/ad_home.png"/> </a>
					</div>
					<div id="gift_card" class="home"><span>Gift Cards</span><a href="/resort-info/order-gift-cards">Buy
							Now &rsaquo;</a></div>
					<?php dynamic_sidebar( 'last_home_col' ) ?>

				</div>
				<div class="span6 col" id="home_featured_special"><h3 class="featured_special_header">Featured <b>Special
							Offer</b></h3><?php echo featuredSpecical(); ?>
					<div class='view_more'><a href="/specials">View more special offers &rsaquo; </a></div>
				</div>
				

			</div>
		</div>
	</div>
	<br/>

	<div id="homeSec2" class="fws grey cf">
		<div class="inner max cf">
			<?php echo espCarousel(); ?></div>
	</div>

	<div id="homeSec3" class="fws white">
		<div id="social_feeds" class="inner max cf">

		</div>
	</div>
</div>
<?php get_footer(); ?>

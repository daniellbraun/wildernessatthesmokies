<?php 
/*
Template Name: Video Gallery
*/
?>
<?php get_header(); ?>
<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
<br class="clear" />
</div>


<div class="span12 cf" id="page_wrap">
<div id="container" class="inner max">
<h1>Wilderness at the Smokies Videos</h1> 
<?  echo get_the_content(); ?>
</div>
<div class="inner max cf">
    <div id="container" class="inner max cf">
        <div class="span6 book_now_btn"><a href="/resort-info/photo-gallery">Photo Gallery</a></div>
        <div class="col span6 book_now_btn"><a href="/resort-info/videos">Video Gallery</a></div>
    </div>
 <div id="container">   
<? videos(); ?>
</div>
</div></div></div>
<?php get_footer(); ?>
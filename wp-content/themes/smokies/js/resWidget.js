jQuery.noConflict();
(function( $ ) {
$(function() {
// More code using $ as alias to jQuery
$( window ).resize(function() {resolutionchange()})


function resolutionchange(){
      width = $( window ).width(); 
      height = $( window ).height();
  if(width < 1110){ 
  $('#book_btn_container span').text('Rooms');
  }else{
	  $('#book_btn_container span').text('Room Search');
	}
	
	  if(width < 935){ 
	   $('.mcForm input[type=submit]').val('Go');
	 
	  }else{
		   $('.mcForm input[type=submit]').val('Sign Up');
	  }
	
}

$( document ).ready(function() {
	resolutionchange();
	
	
	
        $(".custom-select").each(function(){
           
		    $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
			
        });
        
		
		$(".custom-select, select#adults").change(function(){
			
			
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
			
			
			
        }).trigger('change');
		
		
		
 
    $('#dpd2').data('datepicker').hide();
	$('#dpd1').data('datepicker').hide();
	});
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
     
    var checkin = $('#dpd1').datepicker({
    onRender: function(date) {
    return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    if (ev.date.valueOf() > checkout.date.valueOf()) {
    var newDate = new Date(ev.date)
    newDate.setDate(newDate.getDate() + 1);
    checkout.setValue(newDate);
    }
    checkin.hide();
    $('#dpd2')[0].focus();
    }).data('datepicker');
    var checkout = $('#dpd2').datepicker({
    onRender: function(date) {
    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
    }
    }).on('changeDate', function(ev) {
    checkout.hide();
    }).data('datepicker');
	
	//end no conflict
	
	});
})(jQuery);
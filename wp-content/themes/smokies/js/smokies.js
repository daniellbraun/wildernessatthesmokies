jQuery.noConflict();
(function ($) {

    $(window).load(function () {

        $('.menu-trigger').click(function (event) {
            event.preventDefault();
        });

        var windw = this;
        var jPM = $.jPanelMenu();
        var jPM = $.jPanelMenu({
            menu: '#mobile-menu',
            trigger: '.menu-trigger',
            duration: 200,
            keyboardShortcuts: false
        }


        );
        jPM.on();
        ////  Unfix Sidebar	////			
        if ($('#sidebar').length) {
            var el = $('#sidebar');
            var origPos = el.position().top - 130;
            // var pos = $(document).height() + $('#rooms_containter').height();


            $.fn.followTo = function (pos) {
                var $this = this,
                    $window = $(windw),
                    $width = $('#sidebar').width();

                $window.scroll(function (e) {

                    if ($window.scrollTop() > pos) {
                        //$('#sidebar').removeClass('fixed');
                        $this.css({
                            position: 'absolute',
                            top: pos + 130,
                            maxWidth: $width,
                        });

                    } else if ($window.scrollTop() <= origPos) {
                        $this.css({
                            position: 'static',
                            top: origPos,
                            maxWidth: $width,
                        });
                    } else {

                        //$('#sidebar').addClass('fixed');
                        $this.css({
                            position: 'fixed',
                            top: 130,
                            maxWidth: $width,
                        });
                    }
                });
            };

        }


        /////////////// REV SLIDER API
        if ($('#resWidgetTop').length) {

            revapi1.bind("revolution.slide.onchange", function (e, data) {

                var slide = data.slideIndex;
                //alert(data.slideIndex);
                if (slide == 1 || slide == 4) {
                    $('#book').animate({
                        'color': '#272F34'
                    });
                    $('#availabilityform label').animate({
                        'color': '#272F34'
                    });
                } else {
                    $('#book').animate({
                        'color': '#ffffff'
                    });
                    $('#availabilityform label').animate({
                        'color': '#ffffff'
                    });
                }

            });
        }
        if ($('#resWidgetTopAf').length) {
            revapi4.bind("revolution.slide.onchange", function (e, data) {

                var slide = data.slideIndex;
                if (slide == 3 || slide == 4) {
                    $('#book').animate({
                        'color': '#272F34'
                    });
                    $('#availabilityform label').animate({
                        'color': '#272F34'
                    });
                } else {
                    $('#book').animate({
                        'color': '#ffffff'
                    });
                    $('#availabilityform label').animate({
                        'color': '#ffffff'
                    });
                }

            });
        }


        /////////////// END REV SLIDER API	

        navH = $('#nav').outerHeight();
        resWidgetH = $('#resWidgetTopSub').outerHeight();
        //fixit('#sidebar',navH+150);
        fixit('#resWidgetTopSub', navH);
        fixit('#logo', -65);
        fixit('#nav', 0);






        ///// FIXIT FUNCTION ////
        function fixit(elem, lead) {
            elem = $(elem);
            if (elem.length) {
                var offset = elem.offset();
                var leftValue = offset.left;
                var topValue = offset.top - lead;

                if (elem.width() == $(document).width()) {
                    var width = '100%'
                } else {
                    var width = elem.width();
                }

                $(window).scroll(function (event) {

                    var y = $(this).scrollTop();
                    if (y >= topValue) {
                        $(elem).addClass('fixed')
                        $(elem).css({
                            top: '0',
                            width: '100%',
                            maxWidth: width,
                        });

                    } else {
                        $(elem).removeClass('fixed')
                        $(elem).css({

                        });

                    }
                });
            }
        };
        ///// END FIXIT ////


    });

    $(document).ready(function () {

		
		
			
        if ($('#month').length) {

            var d = new Date(),

                n = d.getMonth() + 1,

                y = d.getFullYear();


            if (n.toString().length == 1) {
                n = "0" + n;
            }
            $('#month option[value=' + n + ']').prop('selected', true);
        }

        $('#logo').click(function () {
            window.location.href = "/";
        });

        $('#logo.af').click(function () {
            window.location.href = "/adventureforest";
        });


        if ($('#social_feeds').length == 0) { /*no social feed found*/
        } else {
            var socialsLoaded = false;

            $(window).scroll(function () {

                var bottom_of_object = $('#social_feeds').position().top + $('#social_feeds').outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                /* If the object is completely visible in the window, fade it in */
                if (bottom_of_window > bottom_of_object) {

                    if (!socialsLoaded) {
                        get_socials()
                    }
                    socialsLoaded = true;
                }
            });
        }



        $('.toggle a').click(function () {

            var collapse_content_selector = $(this).attr('href');
            var toggle_switch = $(this);

            $(collapse_content_selector).slideToggle(function () {
                var toggle_text = $(toggle_switch).data('text');
                $(this).is(':visible') ? toggle_switch.text('Hide ' + toggle_text) : toggle_switch.text('Show ' + toggle_text);
                if (toggle_switch.hasClass('expanded')) {
                    toggle_switch.removeClass('expanded');
                } else {
                    toggle_switch.addClass('expanded')
                }
            });
        });




        var allPanels = $('.accordion > dd').hide();

        $('.accordion > dt > a').click(function () {
            $this = $(this);
            $target = $this.parent().next();

            if (!$target.hasClass('active')) {
                allPanels.removeClass('active').slideUp('fast');
                $target.addClass('active').slideDown('fast');
            }

            return false;
        });

        $(".fancybox").fancybox();

        $(".various").fancybox({
            maxWidth: 980,
            maxHeight: 600,
            fitToView: false,
            width: '70%',
            height: '70%',
            autoSize: false,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });


        $('#loadingDiv')
            .hide()
            .ajaxStart(function () {
            $(this).show();
        })
            .ajaxStop(function () {
            $(this).hide();
        });
        var getvar = window.location.search.replace("?", "");
        if (getvar) {
            get_rooms(getvar);
            $('#roomTypes input[type=radio][name="radios"][value="' + getvar + '"]').attr('checked', 'checked');
        } else {
            var roomID = $("#roomTypes input:radio:checked").val();
            get_rooms(roomID);
        }
        $('#roomTypes input:radio').change(function () {

            roomID = $(this).val();
            get_rooms(roomID);
        });




        function get_rooms(roomID) {

            $.ajax({
                url: '/wp-content/plugins/request.php',
                type: 'POST',
                data: {
                    action: 'room_submit',
                    lodging: roomID,
                },
                success: function (data) {
                    /*
                    $('html,body').animate({
                        scrollTop: 0
                    }, 'fast');
					*/
                    $('#rooms_containter').html(data);
                    //pos = $(document).height() + $('#rooms_containter').height();
                    pos = $('#rooms_containter').height() - ($('#footer').height());
                    if ($('#sidebar').length) {
                        $('#sidebar').followTo(pos);
                    }
                },
                cache: true,
                async: true,
                error: function () {
                    $('#rooms_containter').html('<h3>Sorry, something went wrong.</h3>');

                }
            });

            return false;
        }

        $('.varied_title').each(function () {
            var html = $(this).html();
            var word = html.substr(0, html.indexOf(" "));
            var rest = html.substr(html.indexOf(" "));
            $(this).html(rest).prepend($("<span/>").html(word));
        });


        $('#month').change(function () {
            month = $(this).val();
            load_specials(month)
        });

        function load_specials(month) {

            $.ajax({
                url: '/wp-content/plugins/request.php',
                type: 'POST',
                data: {
                    action: 'specials',
                    month: month,
                },
                success: function (data) {
                    $('#load_specials').html(data);
                },
                cache: true,
                async: true,
                error: function () {
                    $('#load_specials').html('<h3>Sorry, an error occured available.</h3>');
                }
            });

            return false;
        }

        $('.varied_title').each(function () {
            var html = $(this).html();
            var word = html.substr(0, html.indexOf(" "));
            var rest = html.substr(html.indexOf(" "));
            $(this).html(rest).prepend($("<span/>").html(word));
        });




        function get_socials() {

            $.ajax({
                url: '/wp-content/plugins/request.php',
                type: 'POST',
                data: {
                    action: 'socials',
                },
                success: function (data) {

                    $('#social_feeds').hide().html(data).fadeIn('slow');

                },
                cache: true,
                async: true,
                error: function () {
					$('#social_feeds').html('<h3 class"primary">Sorry, social feeds not available.</h3>');
                }
            });

            return false;
        }

        $('.varied_title').each(function () {
            var html = $(this).html();
            var word = html.substr(0, html.indexOf(" "));
            var rest = html.substr(html.indexOf(" "));
            $(this).html(rest).prepend($("<span/>").html(word));
        });





    });

})(jQuery); // JavaScript Document
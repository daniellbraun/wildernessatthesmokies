<?php get_header(); ?>

<br class="clear"/>
<div id="ch"><!--clear header--></div>
<div id="container">
    <div class="bread_crumbs_wrap <?php if ( is_page( 'adventureforest' ) || $post->post_parent == 5902 ) {
		echo 'af';
	} ?>">
        <div class="inner max">
            <div id="bread_crumbs" <?php if ( is_page( 'adventureforest' ) || $post->post_parent == 5902 ) {
				echo 'class="af"';
			} ?>><?php the_breadcrumb(); ?>
            </div>
        </div>
    </div>

    <div class="span12" id="page_wrap">

        <div id="container" class="light shadow inner max cf">
            <h1><?php echo get_the_title(); ?></h1>

			<?php
			echo apply_filters( 'the_content', get_the_content() );
			?>

        </div>
    </div>
</div>
<?php get_footer(); ?>

<script>
	<?php echo 'var admin_ajax = "' . admin_url( 'admin-ajax.php' ) . '";'; ?>

    jQuery("#barcode-scan-result").hide();
    jQuery("#barcode-scan-result").on('DOMSubtreeModified', ajax_check_ticket_date);

    function ajax_check_ticket_date() {
        jQuery("#barcode-scan-result").hide();
        jQuery("#barcode-scan-result").off('DOMSubtreeModified');
        var input = jQuery('#barcode-scan-form input#scan-code').val();
        var input_action = jQuery('#barcode-scan-form #scan-action').val();
        jQuery.post(
            admin_ajax,
            {
                action: 'check_ticket_date',
                barcode_input: input,
                scan_action: input_action,
            }
        ).done(function (response) {

            console.log('response:',response);

            // Hide loading text
            jQuery('#barcode-scan-loader').hide();

            // Focus on barcode input field
            jQuery('#barcode-scan-form input#scan-code').focus();

            if (response.valid == false) {
                jQuery('#barcode-scan-result').html("<h2 style='color:red;font-size:2.5em;'>" + response.name + " is not valid for today's date</h2>");
                jQuery("#barcode-scan-result").append("<p>" +response.formatted_info + "</p>");
                jQuery("#barcode-scan-result").show();
                jQuery("#barcode-scan-result").on('DOMSubtreeModified', ajax_check_ticket_date);
                //jQuery(".order-again").hide();
                //jQuery(".woocommerce-message").hide();
                //jQuery(".woocommerce-message + p").hide();
            } else {
                jQuery("#barcode-scan-result").show();
                jQuery("#barcode-scan-result").on('DOMSubtreeModified', ajax_check_ticket_date);
            }

        });
    }
</script>

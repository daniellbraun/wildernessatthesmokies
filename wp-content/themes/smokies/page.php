<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap <?php if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'af';} ?>"><div class="inner max"><div id="bread_crumbs" <?php if (is_page('adventureforest') || $post->post_parent == 5902) {echo 'class="af"';} ?>><?php the_breadcrumb(); ?></div></div></div>

<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>
<div id="pageSlider">
<?php //sliders
if (is_page('215')){ echo do_shortcode('[rev_slider sports]'); }
if (is_page('223')){ echo do_shortcode('[rev_slider BirthdayParty]'); }
if (is_page('217')){ echo do_shortcode('[rev_slider weddings]'); }
if (is_page('6161')){ echo do_shortcode('[rev_slider afparties]'); } 
if (is_page('4799')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('5613')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('1487')){ echo do_shortcode('[rev_slider AreaInfo]'); }
if (is_page('201')){ echo do_shortcode('[rev_slider waterparkslider]'); }

if (is_page('211')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('213')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('230')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('642')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('225')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('121')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('219')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('221')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }
if (is_page('4734')){ echo do_shortcode('[rev_slider MeetingsEvents]'); }

if (is_page('5414')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('145')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('437')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('149')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('1485')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('151')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('135')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('137')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('441')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('234')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('7206')){ echo do_shortcode('[rev_slider ResortInfo]'); }
if (is_page('7360')){ echo do_shortcode('[rev_slider ResortInfo]'); }

if (is_page('7293')){ echo do_shortcode('[rev_slider school]'); }

if (is_page('16185')){ echo do_shortcode('[rev_slider giveaway]'); }
?>
</div>
<?php include (TEMPLATEPATH . '/slider_hub_bar.php'); ?>

<div class="span12" id="page_wrap">
<?php if ($post->post_parent == 4799 && get_the_ID() != 223) {include (TEMPLATEPATH . '/meetings_nav.php'); } ?>


<div id="container" class="light shadow inner max cf">
<h1><?php echo get_the_title(); ?></h1> 

    <?php if  (is_page('223')){

$my_id = 6161;
$post_id_6161 = get_post($my_id);
$content = $post_id_6161->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo $content;

$rows = get_field('listing_format',6161);
if($rows):
foreach($rows as $row):	?>
<div class="span12 cf">
<div class="span3 col cf full_width_image"><img src="<?php echo $row['image']["url"]; ?>" /></div>
<div class="span8 col cf">
<h2><?php echo $row['title']; ?></h2>
<p><?php echo $row['content']; ?></p>
</div>
</div>
<div id="container" class="cf"><hr /></div>
<?php

endforeach;
endif;


	}else{
echo apply_filters('the_content',  get_the_content()); 
}
?>

<?php
$rows = get_field('listing_format');
if($rows):
foreach($rows as $row):	?>
<div class="span12 cf">
<div class="span3 col cf full_width_image"><img src="<?php echo $row['image']["url"]; ?>" /></div>
<div class="span8 col cf">
<h2><?php echo $row['title']; ?></h2>
<p><?php echo $row['content']; ?></p>
</div>
</div>
<div id="container" class="cf"><hr /></div>
<?php
endforeach;
endif;
?>
</div>
</div>

<?php /* if (!is_page('7293')){ ?>
<div id="container">
	<br class="cf"  />
	<?php if (is_page('4799') || $post->post_parent == 4799 ){  }else{ ?>
	<div id="homeSec3" class="fws cf white">
		<div  id="social_feeds" class="inner max cf"><!-- Socials Loaded with ajax--></div>
	</div>
	<?php } ?>
</div>
<?php } */?>

</div>
<?php get_footer(); ?>

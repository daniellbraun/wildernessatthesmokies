<?php get_header(); ?>

	<br class="clear"/>
	<div id="ch"><!--clear header--></div>
	<div id="container">
		<div class="bread_crumbs_wrap>">
			<div class="inner max">
				<div id="bread_crumbs"><?php the_breadcrumb(); ?></div>
			</div>
		</div>

		<div id="resWidgetTopSub">
			<div class="inner max"><?php include( TEMPLATEPATH . '/res_top.php' ); ?></div>
		</div>
		<div id="pageSlider">
			<?php //sliders

			?>
		</div>
		<?php include( TEMPLATEPATH . '/slider_hub_bar.php' ); ?>

		<div class="span12" id="page_wrap">


			<div id="container" class="light shadow inner max cf employment-page">
				<h1><?php echo get_the_title(); ?></h1>

				<?php the_content(); ?>

				<div class="jobs-wrap">
					<?php if ( have_rows( 'jobs_repeater' ) ): while ( have_rows( 'jobs_repeater' ) ) : the_row(); ?>
						<div class="job-position">
							<div class="title">
								<div class="span2 padding">
									<div class="header">
										<strong>Job Title</strong>
									</div>
								</div>

								<div class="span10 padding">
									<strong><?php the_sub_field( 'job_title' ); ?></strong>
								</div>
							</div>

							<div class="description">
								<div class="span2 padding">
									<div class="header">
										<strong>Job Description</strong>
									</div>
								</div>

								<div class="span10 padding">
									<?php the_sub_field( 'job_description' ); ?>

									<p><strong><a href="/employment/employment-application">Online Application</a></strong></p>
								</div>
							</div>
						</div>
						<div class="border"></div>

					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>


	</div>
<?php get_footer(); ?>
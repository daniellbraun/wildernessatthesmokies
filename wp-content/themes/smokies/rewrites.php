<?php

function get_specials_rewritten() {
	$curmonth = date("m");
	$curyear = date("Y");	
	
	//use cache value if possible
	$cache_query = "get_specials_rewritten".$curyear.$curmonth; 
	$cache_value = function_cache::check_if_in_cache($cache_query);
	if ($cache_value) {return $cache_value;}
	
	$o = "";
	for ($i=1;$i<=12;$i++) {
		$o .= specials_by_month_rewritten15($i,$curmonth,$curyear);
	}
	
	//store into cache
	$expiration_triggers = array("post_types" => array("specials"));	
	function_cache::load_into_cache($cache_query,$o,$expiration_triggers);
		
	return $o;	
}

function specials_by_month_rewritten15($month,$curmonth,$curyear){
	$o = "";
	if ($month < 10) {$month = "0".$month;}
	$args = array (
		'post_type' => 'specials',
		'meta_key' => 'offer_start_date', // name of custom field
		'orderby'=> 'menu_order','order'=> "ASC",
		'posts_per_page' => -1 
	);
	$loop = new WP_Query( $args );	
	while ( $loop->have_posts() ) : $loop->the_post();	
		$lcv++;
		$post_id = get_the_ID(); 
		$url = get_permalink();
		$startDate = get_field('offer_start_date');
		$endDate = get_field('offer_end_date');
		$userDate = date('Y-'.$month);
		if (date('m') > date($month)){$userDate = date(date('Y-', strtotime('+1 year')).$month);		}
		$startDate = DateTime::createFromFormat('Ymd', get_field('offer_start_date'));
		$startDate = $startDate->format('Y-m');
		$endDate = DateTime::createFromFormat('Ymd', get_field('offer_end_date'));
		$endDate = $endDate->format('Y-m');
		if ($userDate >= $startDate && $userDate  <= $endDate) {
			$lcv=0;
			$image = get_the_post_thumbnail($post_id,'specials');
			if(!$image) {$image = '<img src="'.get_bloginfo('template_directory').'/images/special_placeholder.jpg" />';} 
			$savings = get_field('savings_text');
			$title = '<a href="'.$url.'">'.get_the_title().'</a>';
			$content = apply_filters('the_content',get_the_content());
			$new_content = strip_tags($old_content);
			$string = $new_content;
			$string = (strlen($string) > 120) ? substr($string,0,110).'...' : $string;
			
			$o .= '<div class="span4 special cf">'; //1
				$o .= '<div class="block_thumb full_width_image"><a href="'.$url.'">'.$image.'</a></div>';
				$o .= '<div class="special_wrap">'; //2
					$o .= '<h4>'.$title.'</h4>';
					$o .= '<div class="span12 col special_text">'.$string.'</div>';
					$o .= '<div id="container">'; //3
						$o .= '<div class="span12 promo">'; //4
						$promo = get_field('promo_code');
						if ($promo) {$o .= '<span class="primary">Use Online Promo Code :<b> '.$promo.' </b></span>';}
						$o .= '</div>'; //3
						$o .= '<br class="cf" />';
						$o .= '<div class="span6 col">'; //4
							$o .= '<a href="'.$url.'" class="view_more">Reserve Now</a>';
						$o .= '</div>'; //3
						$o .= '<div class="span6 col">'; //4
							$o .= '<a href="'.$url.'" class="view_more">More Info</a>';
						$o .= '</div> '; //3
					$o .= '</div>'; //2
				$o .= '</div>'; //1
			$o .= '</div>'; //0				
		}
	endwhile;
 	wp_reset_query();
	
	if ($month == $curmonth) {$o = "<div id='month_block_".$month."' class='month_block'>".$o."</div>";} 
	else {$o = "<div id='month_block_".$month."' class='month_block' style='display:none'>".$o."</div>";}
	
	return $o;
}

function packages_rewritten(){
	$curdate = date("Ymd");
	
	//use cache value if possible
	$cache_query = "packages_rewritten".$curdate; 
	$cache_value = function_cache::check_if_in_cache($cache_query);
	if ($cache_value) {return $cache_value;}
		
	$o = "";
	$args = array( 'post_type' => 'Packages', 'posts_per_page' => -1);
	$loop = new WP_Query( $args );	
	while ($loop->have_posts()) {
		$loop->the_post();	
		$post_id = get_the_ID(); 
		$url = get_permalink();
		$image =  get_the_post_thumbnail( $post_id,'specials');  
		if(!$image) {$image = '<img src="'.get_bloginfo('template_directory').'/images/special_placeholder.jpg" />';}
		$savings = get_field('packages_text');
		$title = '<a href="'.$url.'">'.get_the_title().'</a>';
		
		$content = apply_filters('the_content',get_the_content());
		$new_content = strip_tags($content);
		$string = $new_content;
		$string = (strlen($string) > 90) ? substr($string,0,80).'...' : $string;					

		$o .= '<div class="span3 package cf">';
			$o .= '<div class="package_image full_width_image"><a href="'.$url.'">'.$image.'</a></div>';
			$o .= '<div class="special_wrap">';
				$o .= '<h4>'.$title.'</h4>';
				$o .= '<div class="package_text">'.$string.'</div>';
				$o .= '<div class="package_value">'.$savings.'</div>';
				$o .= '<br class="cf" />';
				$o .= '<a href="'.$url.'" class="view_more">More Info</a>';
			$o .= '</div>';
		$o .= '</div>';
	} 
	wp_reset_query();
	
	//store into cache
	$expiration_triggers = array("post_types" => array("packages"));	
	function_cache::load_into_cache($cache_query,$o,$expiration_triggers);
		
	return $o;
}

function get_mobile_menu_rewritten() {
	$o = "";
	
	//use cache value if possible
	$cache_query = "get_mobile_menu_rewritten"; 
	$cache_value = function_cache::check_if_in_cache($cache_query);
	if ($cache_value) {return $cache_value;}
		
	$o .= wp_nav_menu(array('theme_location' => 'mobile','echo'=>0));
	
	//store into cache
	$expiration_triggers = array("widgets" => "1","menus"=>"1");	
	function_cache::load_into_cache($cache_query,$o,$expiration_triggers);
		
	return $o;
}

function get_main_menu_rewritten() {
	$o = "";
	
	//use cache value if possible
	$cache_query = "get_main_menu_rewritten"; 
	$cache_value = function_cache::check_if_in_cache($cache_query);
	if ($cache_value) {return $cache_value;}
		
	$o .= wp_nav_menu(array('theme_location' => 'header-menu','echo'=>0));
	
	//store into cache
	$expiration_triggers = array("widgets" => "1","menus"=>"1");	
	function_cache::load_into_cache($cache_query,$o,$expiration_triggers);
		
	return $o;
}



?>
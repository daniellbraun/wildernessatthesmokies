<?php
include 'BarcodeGenerator/BarcodeGenerator.php';
include 'BarcodeGenerator/BarcodeGeneratorJPG.php';

require_once 'vendor/autoload.php';
use \Defuse\Crypto;

function getErrors() {
	error_reporting( E_ALL );
	ini_set( 'display_errors', '1' );
}

add_theme_support( 'post-thumbnails' );
add_image_size( 'carousel-item', 275, 230, true );
add_image_size( 'jc-slider-item', 475, 325, true );
add_image_size( 'specials', 980, 500, true );
add_image_size( 'feat_special', 450, 200, true );
add_image_size( 'room_gallery_thumb', 100, 100, true );
add_image_size( 'room_gallery', 600, 600, true );

register_sidebar( array(
	'name'          => 'Home Last Column',
	'id'            => 'last_home_col',
	'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '<h4>',
	'after_title'   => '</h4>',
) );


function register_my_menus() {
	register_nav_menus( array(
		'header-menu' => __( 'Header Menu' ),
		'footer-menu' => __( 'Footer Menu' ),
		'mobile'      => __( 'Mobile' )
	) );
}

add_action( 'init', 'register_my_menus' );

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

add_action( 'custom_ajax_room_submit', 'rooms_callback' );
add_action( 'custom_ajax_nopriv_room_submit', 'rooms_callback' );

add_action( 'custom_ajax_socials', 'socials_callback' );
add_action( 'custom_ajax_nopriv_socials', 'socials_callback' );

add_action( 'custom_ajax_specials', 'specials_callback' );
add_action( 'custom_ajax_nopriv_specials', 'specials_callback' );

function specials_callback() {
	$month = $_POST['month'];
	specials( $month );
}

function socials_callback() {
	?>

    <div class="span4 col social_home">
        <div class="social_header" id="facebook"><a href="https://www.facebook.com/Wildernessatthesmokies?ref=share">Follow
                Us On <b>Facebook</b></a></div>
        <!--		<div class="social_content">--><?php //echo do_shortcode( '[facebook]' ) ?><!--</div>-->
        <div class="social_content facebook-frame">
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FWildernessatthesmokies&tabs=timeline&width=340&height=340&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId"
                    width="340" height="340"
                    style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                    allowTransparency="true"></iframe>
        </div>
    </div>

    <div class="span4 col social_home">
        <div class="social_header" id="twitter"><a href="https://twitter.com/WildSmokies"><b>Tweets</b> @wildsmokies</a>
        </div>
        <div class="social_content"><?php echo twitter_feed(); ?></div>
    </div>

    <div class="span4 col social_home">
        <div class="social_header" id="youtube"><a href="https://www.youtube.com/channel/UCzR19hUJ35Aa6poSvntWwXA">Smokies
                <b>YouTube</b> Videos</a></div>
        <div class="social_content youtube_feed"> <?php echo youTubeHome(); ?> </div>
    </div>

	<?php
	die();
}


function rooms_callback() {
	$id = $_POST['lodging'];
	if ( $id == 'handicap' ) {
		lodgingHandicap();
	} elseif ( $id == 'all' ) {
		lodgingAll();
	} else {
		lodging( $id );
	}
}


/*|||||||||||||||||||||||||||| Lodging ||||||||||||||||||||||||||||*/
function roomSingle( $meta_value ) {

	$tomorrow = new DateTime( 'tomorrow' );

	$reservationurl = 'https://sh-secure.wildernessatthesmokies.com/wildernessatthesmokies-sh?ArrivalDate=' . date( "m/d/Y" ) . '&DepartureDate=' . $tomorrow->format( 'm/d/Y' );
	if ( $_GET['rt'] == 'rl' ) {
		$reservationurl = 'https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl?ArrivalDate=' . date( "m/d/Y" ) . '&DepartureDate=' . $tomorrow->format( 'm/d/Y' );
	}
	if ( $_GET['rt'] == 'vl' ) {
		$reservationurl = 'https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl?ArrivalDate=' . date( "m/d/Y" ) . '&DepartureDate=' . $tomorrow->format( 'm/d/Y' );
	}


	$args = array( 'post_type' => 'room_listings' );
	$loop = new WP_Query( $args );


	while ( $loop->have_posts() ) : $loop->the_post();
		$postId = get_the_ID();

		$rows                        = get_field( 'room_listing' );
		if ( $rows ):
			foreach ( $rows as $row ):
				$room = $row['room_title'];
				if ( $room == $meta_value ):
					$options = $row['room_options'];
					$gallery         = $row['room_gallery'];
					$galId           = $gallery->ID;
					$room            = $row['room_title'];
					$r               = $row['room_title'];
					$r               = preg_replace( '/\s+/', '', $r );
					$room_layout     = $row['room_layout']['url'];
					$layoutThumbnail = $layout['sizes']['medium'];

					$unique_id = strtolower( $room );
					$unique_id = str_replace( ' ', '_', $unique_id );
					$unique_id = str_replace( '/', '', $unique_id );
					?>
                    <div class="room_listing cf">
                        <div class="span4 col room_gallery">
							<?php
							if ( $galId ):
								galleryImages( $galId );
							else:
								$thumbnail = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
								$fullImage = get_bloginfo( 'template_directory' ) . '/images/placeholder.jpg';
								echo ' <div class="room_listing_image"><a href="' . $fullImage . '" class="fancybox"><img src="' . $thumbnail . '" /></a> </div>';
							endif;
							?>
                        </div>

                        <div class="span8 room_info col">
                            <div class="room_title"><?php echo $room; ?></div>
                            <div class="room_options">
								<?php echo $options; ?>
                            </div>
                            <br class="clear"/>

                            <div class="span3 col room_listing_button get_rates cf">
                                <a id="room_book_<?php echo $unique_id; ?>" href="<?php echo $reservationurl ?>">Get
                                    Rates</a>
                            </div>

                            <div class="span3 col room_layout_button ">
                                <a href="<?php echo $room_layout; ?>" class="fancybox">Room layout</a></div>
                        </div>
                        <br class="clear"/>
                    </div>

					<?php
				endif;
			endforeach;
		endif;
	endwhile;
	wp_reset_query();
}


function waterparksMenu() {
	$args = array( 'post_type' => 'waterparks', 'orderby' => 'menu_order', 'order' => "ASC" );
	$loop = new WP_Query( $args );
	echo '<div id="custom_nav">';
	while ( $loop->have_posts() ) : $loop->the_post();
		$title = get_the_title();
		$url   = get_permalink();

		echo '<div class="span4 col">';
		echo '<div class="menu_header"><a href="' . $url . '">' . $title . '</a></div>';
		$rows = get_field( 'waterpark_feature' );
		if ( $rows ):
			echo '<ul class="sub_menu_item">';
			foreach ( $rows as $row ):
				$title = $row['waterpark_title'];


				echo '<li><a href="' . $url . '">' . $title . '</a></li>';

			endforeach;
			echo '</ul>';
		endif;
		echo '<br class="clear" /></div>';
	endwhile;
	wp_reset_query();
	echo '</div>';

}


function amenitiesMenu() {
	$args = array( 'post_type' => 'amenities', 'posts_per_page' => - 1, 'orderby' => 'menu_order', 'order' => "ASC" );
	$loop = new WP_Query( $args );
	echo '<div id="custom_nav">';
	echo '<ul class="sub_menu_item">';
	while ( $loop->have_posts() ) : $loop->the_post();
		$pageLink = get_field( 'page_link' );
		$title    = get_the_title();
		//$url = get_permalink();
		if ( $pageLink ) {
			$url = $pageLink;
		} else {
			$url = get_permalink();
		}

		echo '<div class="span2 col">';
		echo '<li><a href="' . $url . '">' . $title . '</a></li>';
	endwhile;
	wp_reset_query();
	echo '</ul>';
	echo '</div>';

}


function attactionsMenu() {
	$args = array( 'post_type' => 'attractions', 'orderby' => 'menu_order', 'order' => "ASC" );
	$loop = new WP_Query( $args );
	echo '<div id="custom_nav">';
	while ( $loop->have_posts() ) : $loop->the_post();
		$title = get_the_title();
		$url   = get_permalink();

		echo '<div class="span4 col">';
		echo '<div class="menu_header"><a href="/adventureforest/attractions">' . $title . '</a></div>';
		$rows = get_field( 'attraction_listing' );
		if ( $rows ):
			echo '<ul class="sub_menu_item">';
			foreach ( $rows as $row ):
				$title = $row['attraction_title'];


				echo '<li><a href="/adventureforest/attractions">' . $title . '</a></li>';

			endforeach;
			echo '</ul>';
		endif;
		echo '<br class="clear" /></div>';
	endwhile;
	wp_reset_query();
	echo '</div>';

}


function lodgingMenu() {
	$args = array( 'post_type' => 'room_listings', 'orderby' => 'menu_order', 'order' => "ASC" );
	$loop = new WP_Query( $args );
	echo '<div id="lodging_nav">';
	while ( $loop->have_posts() ) : $loop->the_post();
		$title   = get_the_title();
		$content = get_the_content();
		$postid  = get_the_ID();
		if ( $postid != "4916" ) {
			$rt = "rl";
		} else {
			$rt = "sht";
		}


		echo '<div class="span4 col">';
		echo '<div class="menu_header"><a href="/lodging?' . $postid . '">' . $title . '</a></div>';
		$rows = get_field( 'room_listing' );
		if ( $rows ):
			echo '<ul class="sub_menu_item">';
			foreach ( $rows as $row ):
				$handicap = $row['handicap_accessible'];

				if ( $handicap != '' ) {
				} else {
					$room = $row['room_title'];
					echo '<li><a href="/lodging/room?r=' . urlencode( $room ) . '&rt=' . $rt . '&postid=' . $postid . '">' . $room . '</a></li>';
				}
			endforeach;
			echo '</ul>';
		endif;
		echo '<br class="clear cf" /></div>';
	endwhile;
	wp_reset_query();
	echo '<br class="clear cf" />';
	echo '</div>';

}


function lodging( $id ) {
	$args = array( 'post_type' => 'room_listings', 'p' => $id );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		get_lodging_data();
	endwhile;
	wp_reset_query();
	die();
}

function lodgingAll() {
	$args = array(
		'post_type'      => 'room_listings',
		'posts_per_page' => - 1,
		'orderby'        => 'menu_order',
		'order'          => "ASC"
	);
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		get_lodging_data();
	endwhile;
	wp_reset_query();
	die();
}


function lodgingHandicap() {
	$args = array(
		'post_type'      => 'room_listings',
		'posts_per_page' => - 1,
		'orderby'        => 'menu_order',
		'order'          => "ASC"
	);
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$rows                = get_field( 'room_listing' );
		foreach ( $rows as $row ):
			$value = $row['handicap_accessible'];
			if ( in_array( 'Handicap Accessible', $value, true ) ):
				$options = $row['room_options'];
				$gallery     = $row['room_gallery'];
				$galId       = $gallery->ID;
				$room        = $row['room_title'];
				$room_layout = $row['room_layout']['url'];
				$unique_id   = strtolower( $room );
				$unique_id   = str_replace( ' ', '_', $unique_id );
				$unique_id   = str_replace( '/', '', $unique_id );
				?>
                <div class="room_listing cf">
                    <div class="span4 col room_gallery">
						<?php
						if ( $galId ):
							galleryImages( $galId );
						else:
							$thumbnail = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
							$fullImage = get_bloginfo( 'template_directory' ) . '/images/placeholder.jpg';
							echo ' <div class="room_listing_image"><a href="' . $fullImage . '" class="fancybox"><img src="' . $thumbnail . '" /></a> </div>';
						endif;
						?>
                    </div>
                    <div class="span8 room_info col">
                        <div class="room_title"><?php echo '<a href="/lodging/room?r=' . urlencode( $room ) . '">' . $room . '</a>'; ?></div>
                        <div class="room_options">
							<?php echo $options; ?>
                        </div>
                        <br class="clear"/>
						<?php
						$tomorrow       = new DateTime( 'tomorrow' );
						$reservationurl = 'https://sh-secure.wildernessatthesmokies.com/wildernessatthesmokies-sh?ArrivalDate=' . date( "m/d/Y" ) . '&DepartureDate=' . $tomorrow->format( 'm/d/Y' );
						if ( get_the_ID() == "4922" || get_the_ID() == "4924" ) {
							$reservationurl = 'https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl?ArrivalDate=' . date( "m/d/Y" ) . '&DepartureDate=' . $tomorrow->format( 'm/d/Y' );
						}
						?>

                        <div class="span3 col room_listing_button get_rates cf">
                            <a id="room_book_<?php echo $unique_id; ?>" href="<?php echo $reservationurl ?>">Get
                                Rates</a>
                        </div>

                        <div class="span3 col room_layout_button ">
                            <a href="<?php echo $room_layout; ?>" class="fancybox">Room layout</a></div>

                    </div>
                    <br class="clear"/>
                </div>
				<?php
			endif;
		endforeach;
	endwhile;
	wp_reset_query();
	die();
}


function videos() {

	$args = array( 'post_type' => 'youtube' );
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();

		$rows = get_field( 'youtube_video' );

		if ( $rows ):
			foreach ( $rows as $row ):

				$title     = $row['video_title'];
				$content   = $row['video_description'];
				$string    = $content;
				$string    = ( strlen( $string ) > 150 ) ? substr( $string, 0, 140 ) . '...' : $string;
				$video_url = $row['video_url'];
				$video_url = str_replace( "http://youtu.be/", "", $video_url );
				$thumbnail = 'http://img.youtube.com/vi/' . $video_url . '/mqdefault.jpg';
				$playBtn   = get_bloginfo( 'template_directory' ) . '/images/play_btn.png';
				echo '<div class="span4 col colDarkBox video_col">';
				echo '<div class="vid_thumb"><div class="play_btn"><img src="' . $playBtn . '" /></div><a href="http://www.youtube.com/embed/' . $video_url . '?autoplay=1" class="various fancybox.iframe full_width_image" data-fancybox-type="iframe"><img src="' . $thumbnail . '" /></a></div>';
				echo '<div class="waterpark_wrap">';
				echo '<h2>' . $title . '</h2>';
				echo $string;
				echo '<br class="cf" /> <a href="http://www.youtube.com/embed/' . $video_url . '?autoplay=1" class="various fancybox.iframe view_more" data-fancybox-type="iframe">Watch Now</a>';
				echo '</div>';
				echo '</div>';


			endforeach;
		endif;


	endwhile;

}


function galleryHidden() {
	echo '<div class="test1" id="container">';
	$args = array( 'post_type' => 'galleries', 'posts_per_page' => - 1, 'orderby' => 'menu_order', 'order' => "DESC" );
	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();
		$pid   = get_the_ID();
		$value = get_field( 'resort_gallery' );
		if ( $value && in_array( 'Add to Resort Gallery', $value, true ) ):
			$images = get_field( 'image_gallery' );
			$i      = 0;
			foreach ( $images as $image ) :
				$i ++;
				$thumbnail = $image['sizes']['feat_special'];
				$fullImage = $image['sizes']['room_gallery'];
				if ( $i != 1 ):
					echo '<a href="' . $fullImage . '" class="fancybox hidden" rel="gallery' . $pid . '"></a>';
				endif;
			endforeach;
		endif;
	endwhile;
	echo '</div>';
}

function gallery() {

	$args = array( 'post_type' => 'galleries', 'posts_per_page' => - 1, 'orderby' => 'menu_order', 'order' => "DESC" );
	$loop = new WP_Query( $args );
	echo '<div id="container">';
	while ( $loop->have_posts() ) : $loop->the_post();
		$value = get_field( 'resort_gallery' );
		if ( $value && in_array( 'Add to Resort Gallery', $value, true ) ):
			$images = get_field( 'image_gallery' );
			$pid    = get_the_ID();
			$title  = get_the_title();
			$i      = 0;
			foreach ( $images as $image ) :
				$thumbnail = $image['sizes']['feat_special'];

				$fullImage = $image['sizes']['room_gallery'];
			endforeach;

			echo '<div class="span4 col colDarkBox galleryWrap cf">';
			echo '<div class="full_width_image pixilated"><img src="' . $thumbnail . '" /></div>';
			echo '<div class="waterpark_wrap">';
			echo '<h2>' . $title . ' Gallery</h2>';
			echo '<div class="galleryCount">' . count( $images ) . '<span>&nbsp;Photos</span> </div>';
			echo '<br class="cf" /> <a href="' . $fullImage . '" rel="gallery' . $pid . '" class="view_more fancybox">Launch Gallery</a>';
			echo '</div>';
			echo '</div>';
		endif;
	endwhile;
	echo '</div>';
}


function galleryImages( $pid ) {
	$con = getmysqli();

	$sql = "select meta_value from wp_postmeta where post_id='" . mysqli_real_escape_string( $con, $pid ) . "' and meta_key='image_gallery' ";

	$result = mysqli_query( $con, $sql );
	if ( ! mysqli_num_rows( $result ) ) {
		return;
	}
	$row      = mysqli_fetch_array( $result );
	$val      = $row[0];
	$valarray = unserialize( $val );
	$i        = 0;
	foreach ( $valarray as $v ) {
		$i ++;
		$med          = wp_get_attachment_image_src( $v, 'medium' );
		$thumbnail    = $med[0];
		$room_gallery = wp_get_attachment_image_src( $v, 'room_gallery' );
		$fullImage    = $room_gallery[0];
		if ( $i != 1 ) {
			echo '<a href="' . $fullImage . '" class="fancybox hidden" rel="gallery' . $pid . '">' . $i . '</a>';
		} else {
			echo '<div class="room_listing_image">';
			echo '<a href="' . $fullImage . '" class="fancybox" rel="gallery' . $pid . '"><img src="' . $thumbnail . '" /></a>';
			echo '<span  class="gallery_link" >' . count( $valarray ) . '&nbsp;Photos</span> ';
			echo '</div>';
		}
	}
}


function galThumb( $pid ) {


	$con = getmysqli();


	$sql = "select meta_value from wp_postmeta where post_id='" . mysqli_real_escape_string( $con, $pid ) . "' and meta_key='image_gallery' ";
	//$result = mysql_query($sql);
	//$result = new $wpdb->get_results($sql);

	//$wpdb->query($sql);

	$result = mysqli_query( $con, $sql );

	if ( ! mysqli_num_rows( $result ) ) {
		return;
	}
	$row      = mysqli_fetch_array( $result );
	$val      = $row[0];
	$valarray = unserialize( $val );
	$i        = 0;
	foreach ( $valarray as $v ) {
		$i ++;
		$thumb      = wp_get_attachment_image_src( $v, 'feat_special' );
		$thumbImage = $thumb[0];
		if ( $i == 1 ) {
			return $thumbImage;
		}
	}
}


function get_lodging_data() {
	$title = get_the_title();

	$tomorrow = new DateTime( 'tomorrow' );

	$reservationurl = 'https://sh-secure.wildernessatthesmokies.com/wildernessatthesmokies-sh?ArrivalDate=' . date( "m/d/Y" ) . '&DepartureDate=' . $tomorrow->format( 'm/d/Y' );

	if ( get_the_ID() == "4922" || get_the_ID() == "4924" ) {
		$reservationurl = 'https://rl-secure.wildernessatthesmokies.com/wildernessatthesmokies-rl?ArrivalDate=' . date( "m/d/Y" ) . '&DepartureDate=' . $tomorrow->format( 'm/d/Y' );
	}

	$content = get_the_content();
	echo '<h3 class="primary">' . $title . '</h3>' . '<p>' . $content . '</p>';
	$rows                    = get_field( 'room_listing' );
	if ( $rows ):
		foreach ( $rows as $row ):
			$value = $row['handicap_accessible'];
			if ( ! $value || ! in_array( 'Handicap Accessible', $value, true ) ):
				$options = $row['room_options'];
				$gallery     = $row['room_gallery'];
				$room_layout = $row['room_layout']['url'];


				$galId     = $gallery->ID;
				$room      = $row['room_title'];
				$unique_id = strtolower( $room );
				$unique_id = str_replace( ' ', '_', $unique_id );
				$unique_id = str_replace( '/', '', $unique_id );
				?>
                <div class="room_listing cf">
                    <div class="span4 col room_gallery">
						<?php
						if ( $galId ):
							galleryImages( $galId );
						else:
							$thumbnail = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
							$fullImage = get_bloginfo( 'template_directory' ) . '/images/placeholder.jpg';
							echo ' <div class="room_listing_image"><a href="' . $fullImage . '" class="fancybox"><img src="' . $thumbnail . '" /></a> </div>';
						endif;
						?>
                    </div>
                    <div class="span8 room_info col">
                        <div class="room_title"><?php echo '<a href="/lodging/room?r=' . urlencode( $room ) . '">' . $room . '</a>'; ?></div>
                        <div class="room_options">
							<?php echo $options; ?>
                        </div>
                        <br class="clear"/>

                        <div class="span3 col room_listing_button get_rates cf">
                            <a id="room_book_<?php echo $unique_id; ?>" href="<?php echo $reservationurl ?>">Get
                                Rates</a>
                        </div>

                        <div class="span3 col room_layout_button ">
                            <a href="<?php echo $room_layout; ?>" class="fancybox">Room layout</a></div>

                    </div>
                    <br class="clear"/>
                </div>

			<?php endif;
		endforeach;
	endif;

}


/////////////////////////// WATERPARKS /////////////////////////////

function waterparks() {
	$args = array( 'post_type' => 'waterparks', 'posts_per_page' => - 1, 'orderby' => 'menu_order', 'order' => "ASC" );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$title = get_the_title();
		$rows  = get_field( 'waterpark_feature' );
		if ( $rows ):
			foreach ( $rows as $row ):
				//$gallery = $row['waterpark_gallery'];
				//$galId = $gallery->ID;
				$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID, 'feat_special' ) );
				if ( $feat_image ):
					//$image = galThumb($galId);
					$image = $feat_image;
				else:
					$image = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
				endif;

			endforeach;
		endif;
		ob_start();
		$content     = the_content();
		$old_content = ob_get_clean();
		$new_content = strip_tags( $old_content );
		$stringCut   = substr( $new_content, 0, 150 );
		$string      = substr( $stringCut, 0, strrpos( $stringCut, ' ' ) ) . '...';
		$post_id     = get_the_ID();
		$url         = get_permalink();
		?>
        <div class="waterpark span4 col colDarkBox cf">
            <div class="waterpark_image full_width_image"><?php echo '<a href="' . $url . '"><img src="' . $image . '" /></a>'; ?></div>
            <div class="waterpark_wrap">
                <h4><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h4>

                <div class="waterpark_text"><?php echo $string; ?></div>
                <br class="cf"/>
                <br class="cf"/>
                <a href="<?php echo $url; ?>" class="view_more">More Info</a>
            </div>
        </div>
		<?php
	endwhile;
	wp_reset_query();

}

////////////////////   AMENITIES   ///////////////////////

function amenities() {
	$args = array( 'post_type' => 'amenities', 'posts_per_page' => - 1, 'orderby' => 'menu_order', 'order' => "ASC" );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$title = get_the_title();

		$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );


		if ( $feat_image ):
			$image = $feat_image;
		else:
			$image = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
		endif;

		ob_start();
		$content     = the_content();
		$old_content = ob_get_clean();
		$new_content = strip_tags( $old_content );
		$stringCut   = substr( $new_content, 0, 150 );
		$string      = substr( $stringCut, 0, strrpos( $stringCut, ' ' ) ) . '...';
		$post_id     = get_the_ID();
		$pageLink    = get_field( 'page_link' );

		if ( $pageLink ) {
			$url = $pageLink;
		} else {
			$url = get_permalink();
		}
		?>
        <div class="span4 col colDarkBox cf">
            <div class="block_thumb full_width_image"><?php echo '<a href="' . $url . '"><img src="' . $image . '" /></a>'; ?></div>
            <div class="waterpark_wrap">
                <h4><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h4>

                <div class="waterpark_text"><?php echo $string; ?></div>
                <br class="cf"/>
                <a href="<?php echo $url; ?>" class="view_more">More Info</a>
            </div>
        </div>
		<?php
	endwhile;
	wp_reset_query();

}

function meetings_events() {

	$args = array(
		'post_parent'    => 4799,
		'post_type'      => 'page',
		'posts_per_page' => - 1,
		'orderby'        => 'menu_order',
		'order'          => "ASC"
	);

	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$postid = get_the_ID();
		if ( $postid == 5430 || $postid == 225 || $postid == 4734 || $postid == 230 ) {

		} else {

			$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );

			if ( $feat_image ):
				$image = $feat_image;
			else:
				$image = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
			endif;


			ob_start();
			$content     = the_content();
			$old_content = ob_get_clean();
			$new_content = strip_tags( $old_content );
			$stringCut   = substr( $new_content, 0, 150 );
			$string      = substr( $stringCut, 0, strrpos( $stringCut, ' ' ) ) . '...';
			$post_id     = get_the_ID();
			$url         = get_permalink();
			$title       = get_the_title();
			?>
            <div class="span4 col colDarkBox cf">
                <div class="block_thumb full_width_image"><?php echo '<a href="' . $url . '"><img src="' . $image . '" /></a>'; ?></div>
                <div class="waterpark_wrap">
                    <h4><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h4>

                    <div class="waterpark_text"><?php echo $string; ?></div>
                    <br class="cf"/>
                    <a href="<?php echo $url; ?>" class="view_more">More Info</a>
                </div>
            </div>
			<?php
		}

	endwhile;
	wp_reset_query();

}

function home_school_days() {

	$args = array(
		'post_parent'    => 7025,
		'post_type'      => 'page',
		'posts_per_page' => - 1,
		'orderby'        => 'menu_order',
		'order'          => "ASC"
	);

	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$postid = get_the_ID();

		$feat_image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );

		if ( $feat_image ):
			$image = $feat_image;
		else:
			$image = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
		endif;


		ob_start();
		$content     = the_content();
		$old_content = ob_get_clean();
		$new_content = strip_tags( $old_content );
		$stringCut   = substr( $new_content, 0, 150 );
		$string      = substr( $stringCut, 0, strrpos( $stringCut, ' ' ) ) . '...';
		$post_id     = get_the_ID();
		$url         = get_permalink();
		$title       = get_the_title();
		?>
        <div class="span4 col colDarkBox cf">
            <div class="block_thumb full_width_image"><?php echo '<a href="' . $url . '"><img src="' . $image . '" /></a>'; ?></div>
            <div class="waterpark_wrap">
                <h4><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h4>

                <div class="waterpark_text"><?php echo $string; ?></div>
                <br class="cf"/>
                <a href="<?php echo $url; ?>" class="view_more">More Info</a>
            </div>
        </div>
		<?php
	endwhile;
	wp_reset_query();

}


///////////////////// ATTRACTIONS ///////////////////////////////

function attractions() {
	$args = array( 'post_type' => 'attractions', 'posts_per_page' => - 1 );
	$loop = new WP_Query( $args );
	?>
    <div id="container" class="inner cf max">    <?php
	while ( $loop->have_posts() ) : $loop->the_post();
		$rows                = get_field( 'attraction_listing' );
		if ( $rows ):
			foreach ( $rows as $row ):
				$title = $row['attraction_title'];
				$description = $row['attraction_content'];
				$gallery     = $row['attraction_gallery'];
				$galId       = $gallery->ID;
				?>
                <div class="inner cf max attraction_listing">
                    <div class="span4 col">
						<?php
						if ( $galId ):
							galleryImages( $galId );
						else:
							$thumbnail = get_bloginfo( 'template_directory' ) . '/images/placeholder_300x300.jpg';
							$fullImage = get_bloginfo( 'template_directory' ) . '/images/placeholder.jpg';
							echo ' <div class="room_listing_image"><a href="' . $fullImage . '" class="fancybox"><img src="' . $thumbnail . '" /></a> </div>';
						endif;
						?>
                    </div>
                    <div class="span8 col">
                        <h2 class="primary"><?php echo $title; ?></h2>
						<?php echo $description; ?>
                        <div class="passesBtn">
                            <a href="http://www.wildernessatthesmokies.com/adventureforest/prices-passes">Prices &amp;
                                Passes</a></div>
                        <br/>
                    </div>
                </div>
				<?php
			endforeach;
		endif;
		?></div><?php
	endwhile;
	wp_reset_query();

}


/*|||||||||||||||||||||||||||| Home Content||||||||||||||||||||||||||||*/

function homeContent() {

	$args = array( 'post_type' => 'home_content', 'posts_per_page' => 1, 'orderby' => 'modified' );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$content = the_content();
		echo $content;
	endwhile;
	wp_reset_query();
}


/*|||||||||||||||||||||||||||| Specials ||||||||||||||||||||||||||||*/

function specials( $month ) {
	/*
		echo "Special temporarily down...check back soon!";
		return;
		*/
	$args = array(
		'post_type'      => 'specials',
		'meta_key'       => 'offer_start_date', // name of custom field
		//'orderby' => 'meta_value_num',
		'orderby'        => 'menu_order',
		'order'          => "ASC",
		'posts_per_page' => - 1

	);

	$loop = new WP_Query( $args );


	while ( $loop->have_posts() ) : $loop->the_post();

		//echo "title = ".get_the_title()."<br />";

		$lcv ++;

		$post_id = get_the_ID();
		$url     = get_permalink();

		/* check dates of offers */
		$startDate = get_field( 'offer_start_date' );
		$endDate   = get_field( 'offer_end_date' );

		/* selected user date */
		$userDate = date( 'Y-' . $month );

		/* Check if the month has past if it has add a year Booyakasha!!*/
		if ( date( 'm' ) > date( $month ) ) {
			$userDate = date( date( 'Y-', strtotime( '+1 year' ) ) . $month );
		}
		/* Converr date format */
		$startDate = DateTime::createFromFormat( 'Ymd', get_field( 'offer_start_date' ) );
		$startDate = $startDate->format( 'Y-m' );

		$endDate = DateTime::createFromFormat( 'Ymd', get_field( 'offer_end_date' ) );
		$endDate = $endDate->format( 'Y-m' );

		/* Check Date Range */
		if ( $userDate >= $startDate && $userDate <= $endDate ) {

			$lcv = 0;
			/* If all is good display the offers...YAY! */
			$image = get_the_post_thumbnail( $post_id, 'specials' );
			if ( ! $image ):
				$image = '<img src="' . get_bloginfo( 'template_directory' ) . '/images/special_placeholder.jpg" />';
			endif;
			$savings = get_field( 'savings_text' );
			$title   = '<a href="' . $url . '">' . get_the_title() . '</a>';
			ob_start();
			$content     = the_content();
			$old_content = ob_get_clean();
			$new_content = strip_tags( $old_content );
			$string      = $new_content;
			$string      = ( strlen( $string ) > 120 ) ? substr( $string, 0, 110 ) . '...' : $string;

			?>
            <div class="span4 special cf">
                <div class="block_thumb full_width_image"><?php echo '<a href="' . $url . '">' . $image . '</a>'; ?></div>
                <div class="special_wrap">

                    <h4><?php echo $title; ?></h4>

                    <div class="span12 col special_text"><?php echo $string; ?></div>
                    <div id="container">
                        <!-- <div class="span12 special_value"><?php //echo $savings;
						?></div>	-->
                        <div class="span12 promo">
							<?php $promo = get_field( 'promo_code' ); ?>
							<?php if ( $promo ): ?>
                                <span class="primary">Use Online Promo Code :<b> <?php echo $promo; ?> </b></span>
							<?php endif; ?>
                        </div>
                        <br class="cf"/>

                        <div class="span6 col"><a href="<?php echo $url; ?>" class="view_more">Reserve Now</a></div>
                        <div class="span6 col"><a href="<?php echo $url; ?>" class="view_more">More Info</a></div>
                    </div>
                </div>
            </div>
			<?php


		} else {
			//echo "special not found ".get_the_title()."<br />";
		}
	endwhile;


	wp_reset_query();

}

/*|||||||||||||||||||||||||||| s Specials ||||||||||||||||||||||||||||*/

function featuredSpecical() {
	$args = array( 'post_type' => 'specials', 'posts_per_page' => - 1 );
	$loop = new WP_Query( $args );
	$i    = 0;
	while ( $loop->have_posts() ) : $loop->the_post();
		$post_id = get_the_ID();
		$value   = get_field( 'options' );
		$url     = get_permalink();
		$i ++;
		if ( $value && in_array( 'Feature on homepage', $value, true ) ):

			$image = get_the_post_thumbnail( $post_id, 'feat_special' );
			if ( ! $image ):
				$image = '<img src="' . get_bloginfo( 'template_directory' ) . '/images/special_placeholder.jpg" />';
			endif;
			$savings = get_field( 'savings_text' );
			$title   = get_the_title();
			ob_start();
			$content     = the_content();
			$old_content = ob_get_clean();
			$new_content = strip_tags( $old_content );
			$string      = $new_content;
			$string      = ( strlen( $string ) > 150 ) ? substr( $string, 0, 140 ) . '...' : $string;

			?>
            <a href="<?php echo $url ?>"> <?php echo $image; ?></a>
            <h4><a href="<?php echo $url ?>"><?php echo $title; ?></a></h4>
            <p><?php echo $string; ?></p>
			<?php
		endif;

	endwhile;
	wp_reset_query();

}


/*|||||||||||||||||||||||||||| Packages ||||||||||||||||||||||||||||*/

function packages() {

	$args = array( 'post_type' => 'Packages', 'posts_per_page' => - 1 );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$post_id = get_the_ID();
		$url     = get_permalink();
		$image   = get_the_post_thumbnail( $post_id, 'specials' );
		if ( ! $image ):
			$image = '<img src="' . get_bloginfo( 'template_directory' ) . '/images/special_placeholder.jpg" />';
		endif;
		$savings = get_field( 'packages_text' );
		$title   = '<a href="' . $url . '">' . get_the_title() . '</a>';
		ob_start();
		$content     = the_content();
		$old_content = ob_get_clean();
		$new_content = strip_tags( $old_content );
		$string      = $new_content;
		$string      = ( strlen( $string ) > 90 ) ? substr( $string, 0, 80 ) . '...' : $string;
		?>
        <div class="span3 package cf">
            <div class="package_image full_width_image"><?php echo '<a href="' . $url . '">' . $image . '</a>'; ?></div>
            <div class="special_wrap">
                <h4><?php echo $title; ?></h4>

                <div class="package_text"><?php echo $string; ?></div>
                <div class="package_value"><?php echo $savings; ?></div>
                <br class="cf"/>
                <a href="<?php echo $url; ?>" class="view_more">More Info</a>

            </div>
        </div>
		<?php
	endwhile;
	wp_reset_query();
}

/*|||||||||||||||||||||||||||| Carousels ||||||||||||||||||||||||||||
Carousels are dependent on /js/jcarousel.responsive.js
*/


/* Home Carousel */
function homeCarousel() {

	$args = array( 'post_type' => 'carousel', 'posts_per_page' => - 1 );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$id = get_the_ID();

		if ( $id == 4862 ):
			$rows = get_field( 'carousel_items' );
			if ( $rows ):
				?>
                <div class="jcarousel-wrapper">
                    <div class="jc-slider">
                        <ul>
							<?php

							foreach ( $rows as $row ):
								$image = $row['carousel_image'];
								$title = $row['carousel_title'];
								$content = $row['carousel_content'];
								$link = $row['carousel_link'];
								?>
                                <li>
                                    <a href="<?php echo $link; ?>"><img
                                                src="<?php echo $image['sizes']['jc-slider-item']; ?>"/></a>

                                    <div class="jc-slider-content"><h2> <?php echo $title; ?></h2>

                                        <p><?php echo $content; ?></p>
                                    </div>
                                </li>
							<?php endforeach; ?>
                        </ul>
                    </div>
                    <p class="jc-slider-pagination"></p>
                </div>
				<?php
			endif;
		endif;
	endwhile;
	wp_reset_query();
}

/* Eat Stay Play Carousel */

function espCarousel() {
	$args = array( 'post_type' => 'carousel', 'posts_per_page' => - 1 );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$id = get_the_ID();
		if ( $id == 4861 ):

			$rows = get_field( 'carousel_items' );
			if ( $rows ):
				?>
                <div class="jcarousel-wrapper">
                    <div class="jcarousel">
                        <ul>
							<?php

							foreach ( $rows as $row ):
								$image = $row['carousel_image'];
								$content = $row['carousel_content'];
								$link = $row['carousel_link'];

								?>
                                <li>

                                    <a href="<?php echo $link; ?>"><img
                                                src="<?php echo $image['sizes']['carousel-item']; ?>"/></a>

                                    <p><?php echo $content; ?></p>
                                </li>

							<?php endforeach; ?>

                        </ul>
                    </div>
                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>

                </div>
				<?php
			endif;
		endif;
	endwhile;
	wp_reset_query();

}

/////////////// Attractions Carousel //////////////////////

function attractionsCarousel() {

	$args = array( 'post_type' => 'carousel', 'posts_per_page' => - 1 );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
		$id = get_the_ID();
		if ( $id == 6132 ):

			$rows = get_field( 'carousel_items' );
			if ( $rows ):
				?>
                <div class="jcarousel-wrapper">
                    <div class="jcarousel">
                        <ul>
							<?php

							foreach ( $rows as $row ):
								$image = $row['carousel_image'];
								$title = $row['carousel_title'];
								//$link =  $row['carousel_link'];

								?>
                                <li>

                                    <a href="/adventureforest/attractions"><img
                                                src="<?php echo $image['sizes']['carousel-item']; ?>"/></a>

                                    <p><?php echo $title; ?></p>
                                </li>

							<?php endforeach; ?>

                        </ul>
                    </div>
                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>

                </div>
				<?php
			endif;
		endif;
	endwhile;
	wp_reset_query();

}


///////////////   YouTube   //////////////////////


function youTubeHome() {
	$args = array( 'post_type' => 'youtube', 'posts_per_page' => 1, );
	$loop = new WP_Query( $args );
	$i    = 0;
	while ( $loop->have_posts() ) : $loop->the_post();

		$rows = get_field( 'youtube_video' );
		if ( $rows ):
			foreach ( $rows as $row ):
				$i ++;

				$content   = $row['video_description'];
				$string    = $content;
				$string    = ( strlen( $string ) > 150 ) ? substr( $string, 0, 140 ) . '...' : $string;
				$video_url = $row['video_url'];
				$video_url = str_replace( "http://youtu.be/", "", $video_url );

				?>
                <div class="video-container">
                    <iframe src="<?php echo 'http://www.youtube.com/embed/' . $video_url . '?rel=0'; ?>" frameborder="0"
                            width="560" height="315"></iframe>
                </div>
                <p style="padding-top:15px;"><?php echo $string; ?><span class='view_more'><a href="/videos"> Watch
							Now &rsaquo;</a></span></p>
				<?php
				if ( $i = 1 ): exit(); endif;
			endforeach;
		endif;

	endwhile;
	wp_reset_query();
}


function twitter_feed() {
	?>
    <a class="twitter-timeline" href="https://twitter.com/WildSmokies"
       data-chrome="nofooter transparent noheader noborders" data-tweet-limit="3" data-link-color="#006FB8"
       data-widget-id="427478790625718272">Tweets
        by @WildSmokies</a>
    <script>!function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + "://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, "script", "twitter-wjs");</script>
	<?php
}


function customTitle() {
	if ( is_page( 'room' ) ) {
		echo 'Wilderness at the Smokies | ' . $_GET["r"];
	} else {
		if ( is_home() ) {
			echo 'Wilderness at the Smokies | Tennessee\'s Largest Waterpark Resort';
		} else {
			wp_title( 'Wilderness at the Smokies | ', true, 'left' );
		}
	}
}

register_sidebar( array(
	'name'          => __( 'Lodging Sidebar' ),
	'id'            => 'sidebar-lodging',
	'before_widget' => '<li id="%1$s" class="sidebarBox">',
	'after_widget'  => '</li>',
	'before_title'  => '<h5 class="varied_title">',
	'after_title'   => '</h5>',
) );


function the_breadcrumb() {
	echo '<ul id="breadcrumbs">';
	if ( ! is_home() ) {
		echo '<li><a href="';
		echo get_option( 'home' );
		echo '">';
		echo 'Home';
		echo '</a></li><li class="separator"> / </li>';
		if ( is_page( 'lodging' ) ) {
			echo '<li><a href="/lodging">Lodging</a></li>';
		} else if ( is_page( 'room' ) ) {
			echo '<li><a href="/lodging">Lodging</a></li><li class="separator"> / </li><li><strong>' . $_GET['r'] . ' </strong></li> ';
		} else {
			if ( is_page( 'faqs' ) ) {
				echo '<li><a href="/resort-info">Resort Info</a></li><li class="separator"> / </li><li><strong>' . get_the_title() . ' </strong></li> ';
			} else if ( is_singular( 'specials' ) || is_singular( 'packages' ) ) {
				echo '<li><a href="/specials">Specials</a></li><li class="separator"> / </li><li><strong>' . get_the_title() . '</strong></li> ';
			} else if ( is_singular( 'amenities' ) ) {
				echo '<li><a href="/amenities">Amenities</a></li><li class="separator"> / </li><li><strong>' . get_the_title() . '</strong></li> ';
			} else if ( is_singular( 'waterparks' ) ) {
				echo '<li><a href="/waterparks">Waterparks</a></li><li class="separator"> / </li><li><strong>' . get_the_title() . '</strong></li> ';
			} else {

				if ( is_category() || is_single() ) {
					echo '<li>';
					the_category( ' </li><li class="separator"> / </li><li> ' );
					if ( is_single() ) {
						echo '</li><li class="separator"> / </li><li>';
						the_title();
						echo '</li>';
					}

				} elseif ( is_page() ) {
					if ( $post->post_parent ) {
						$anc = get_post_ancestors( $post->ID );
						foreach ( $anc as $ancestor ) {
							$output = '<li><a href="' . get_permalink( $ancestor ) . '" title="' . get_the_title( $ancestor ) . '">' . get_the_title( $ancestor ) . '</a></li> <li class="separator">/</li>';
						}
						echo $output;
						echo '<strong title="' . $title . '"> ' . $title . '</strong>';
					} else {
						echo '<strong> ';
						echo the_title();
						echo '</strong>';
					}
				}
			}
		}
	} elseif ( is_tag() ) {
		single_tag_title();
	} elseif ( is_day() ) {
		echo "<li>Archive for ";
		the_time( 'F jS, Y' );
		echo '</li>';
	} elseif ( is_month() ) {
		echo "<li>Archive for ";
		the_time( 'F, Y' );
		echo '</li>';
	} elseif ( is_year() ) {
		echo "<li>Archive for ";
		the_time( 'Y' );
		echo '</li>';
	} elseif ( is_author() ) {
		echo "<li>Author Archive";
		echo '</li>';
	} elseif ( isset( $_GET['paged'] ) && ! empty( $_GET['paged'] ) ) {
		echo "<li>Blog Archives";
		echo '</li>';
	} elseif ( is_search() ) {
		echo "<li>Search Results";
		echo '</li>';
	}
	echo '</ul>';


}


/*|||||||||||||||||||||||||||| POST TYPES ||||||||||||||||||||||||||||*/

add_action( 'init', 'create_post_types' );

function create_post_types() {

	register_post_type( 'specials', array(
		'labels'      => array(
			'name'          => __( 'Specials' ),
			'singular_name' => __( 'Special' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/packagesicon.png'

	) );

	register_post_type( 'packages', array(
		'labels'      => array(
			'name'          => __( 'Packages' ),
			'singular_name' => __( 'Package' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/packageicon.png'

	) );


	register_post_type( 'carousel', array(
		'labels'      => array(
			'name'          => __( 'Carousels' ),
			'singular_name' => __( 'Carousel' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/carouselicon.png'
	) );

	register_post_type( 'home_content', array(
		'labels'      => array(
			'name'          => __( 'Home Page' ),
			'singular_name' => __( 'Home Content' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/homeicon.png'
	) );


	register_post_type( 'youtube', array(
		'labels'      => array(
			'name'          => __( 'YouTube' ),
			'singular_name' => __( 'YouTube Gallery' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/youtube_admin_icon.png'
	) );


	register_post_type( 'room_listings', array(
		'labels'      => array(
			'name'          => __( 'Room Listings' ),
			'singular_name' => __( 'Rooms' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/rooms_icon.png'
	) );


	register_post_type( 'galleries', array(
		'labels'      => array(
			'name'          => __( 'Galleries' ),
			'singular_name' => __( 'Gallery' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/gallery_icon.png'
	) );


	register_post_type( 'faq', array(
		'labels'      => array(
			'name'          => __( 'FAQ\'s' ),
			'singular_name' => __( 'FAQ' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', ),
		'menu_icon'   => '/wp-content/themes/smokies/images/faq_icon.png',
		'supports'    => array( 'title' ),
	) );


	register_post_type( 'waterparks', array(
		'labels'      => array(
			'name'          => __( 'Waterparks' ),
			'singular_name' => __( 'Waterpark' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/waterparks_icon.png'

	) );


	register_post_type( 'amenities', array(
		'labels'      => array(
			'name'          => __( 'Amenities' ),
			'singular_name' => __( 'Amenity' ),

		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/amenities_icon.png'

	) );

	register_post_type( 'attractions', array(
		'labels'      => array(
			'name'          => __( 'Attractions' ),
			'singular_name' => __( 'Attracton' ),

		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/attractions_icon.png'

	) );

	register_post_type( 'specials', array(
		'labels'      => array(
			'name'          => __( 'Specials' ),
			'singular_name' => __( 'Special' )
		),
		'public'      => true,
		'has_archive' => false,
		'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'menu_icon'   => '/wp-content/themes/smokies/images/packagesicon.png'

	) );

}


/*|||||||||||||||||||||||||||| END POST TYPES ||||||||||||||||||||||||||||*/


function map_shortcode( $content = null ) {

	return '
	
	
	 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<form id="directions" action="http://maps.google.com/maps" method="get" target="_blank">
   <label for="saddr"><h2> Enter your location:</h2></label>
   <input type="text" name="saddr" />
   <input type="hidden" name="daddr" value="1424 Old Knoxville Hwy, Sevierville, TN 37876" />
   <input type="submit" value="Get directions" />
</form>
<br />
	<script> var map;
	function initialize() {
		  var smokies =  new google.maps.LatLng(35.894477, -83.584944);

	  var mapOptions = {
		zoom: 16,
		center: smokies,
	  };
	  
	  map = new google.maps.Map(document.getElementById("map-canvas"),
		  mapOptions);
		  
		  var marker = new google.maps.Marker({
      position: smokies,
      map: map,
      title: "Wilderness at the Smokies"
  });

		  
	}
	
	
	google.maps.event.addDomListener(window, "load", initialize);
	</script>
	<div id="map-canvas" style="width:100%; height:450px;"></div>
	';

}

function testimonial_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'by' => ''
	), $atts ) );

	return '<div class="testimonial span6 col"><blockquote><span class="openQuote"></span>' . $content . ' <span class="byline"> â€“ ' . $by . '</span></blockquote></div>';
}


add_shortcode( 'testimonial', 'testimonial_shortcode' );
add_shortcode( 'map', 'map_shortcode' );

function getmysqli() {
	$link = mysqli_connect( DB_HOST, DB_USER, DB_PASSWORD, DB_NAME );

	return $link;
}

require( "rewrites.php" );

function enqueue_tickets_calendar_scripts() {
	wp_enqueue_script( 'wp-util' );
	wp_enqueue_script( 'tickets-calendar/js' );
}

/* Retrieve single waterpark ticket */

add_action( 'wp_ajax_retrieve_ticket', 'retrieve_ticket_callback' );
add_action( 'wp_ajax_nopriv_retrieve_ticket', 'retrieve_ticket_callback' );

function retrieve_ticket_callback() {

	$ticket_date = $_POST['ticket_date'];

	$product_query_result = wc_get_products( array(
		'meta_key'   => 'ticket_date',
		'meta_value' => $ticket_date
	) );

	$product = $product_query_result[0];

	if ( ! $product ) {
	    $result = [
		    "title"     => "No Tickets",
		    "price"     => "0",
		    "remaining" => 0,
		    "id"        => 0,
        ];
		wp_send_json( $result );

    }
	$result = [
		"title"     => $product->get_title(),
		"price"     => $product->get_price(),
		"remaining" => $product->get_stock_quantity(),
		"id"        => $product->get_id(),
	];

	wp_send_json( $result );
}

/**
 * Create AJAX response to deal with non matched dates
 */
add_action( 'wp_ajax_check_ticket_date', 'check_ticket_date_callback' );
add_action( 'wp_ajax_nopriv_check_ticket_date', 'check_ticket_date_callback' );
function check_ticket_date_callback() {

	$barcode_string = $_POST['barcode_input'];
	$scan_action = $_POST['scan_action'];

	$order_id = WC_Order_Barcodes()->get_barcode_order( $barcode_string );

	if ( ! $order_id || $scan_action != 'complete') return;

	$order_instance = new WC_Order( $order_id );
	$line_items     = $order_instance->get_items();

	foreach ( $line_items as $item ) {

	    $product_id  = $item->get_product()->get_id();
	    $product_name = $item->get_product()->get_name();
		$date_string = get_field( 'ticket_date', $product_id );

		$ticket_date = new DateTime( $date_string );

		$ticket_date_fmt = $ticket_date->format( 'F j, Y' );

		$todays_date = new DateTime();
		$ticket_date = $ticket_date->format( 'Y-m-d' );
		$todays_date = $todays_date->format( 'Y-m-d' );



		if ( $ticket_date != $todays_date ) {
			$order_instance->update_status( 'processing' );
			$response = [ 'valid' => false, 'name' => $product_name ];

			$response['formatted_info'] = sprintf(
				__( 'Order #%1$s was placed on %2$s and is only valid for %3$s.', 'woocommerce' ),
				'<mark class="order-number">' . $order_instance->get_order_number() . '</mark>',
				'<mark class="order-date">' . wc_format_datetime( $order_instance->get_date_created() ) . '</mark>',
				'<mark class="order-status">' . $ticket_date_fmt . '</mark>'
			);

			wp_send_json( $response );

		} else {
			$response = [ 'valid' => true ];

			wp_send_json( $response );

		}
	}
}


/**
 * Hook into order creation with woocommerce_order_status_changed -
 * destroy original order create individual order for each line item
 * if there is more than one line item. Also build email for customer.
 *
 * @param int $this_get_id
 * @param string $this_status_transition_from
 * @param string $this_status_transition_to
 * @param WC_Order $instance
 */
function vi_woocommerce_possibly_split_new_order( $this_get_id, $this_status_transition_from, $this_status_transition_to, $instance ) {

    // prevent this function from happening when triggered by the update_status call below
    // since this function is triggered by woocommerce_order_status_changed action
    //
    // If this order has a orig_order_key meta key, it is an order that was created below
    // and should not be run though this function (this happens when the update_status mentioned above)

    if ( metadata_exists( 'post', $this_get_id, 'orig_order_key') ) return;


	// this function runs during a order status change action so it must be prevented
	// from running if user is in admin
	if ( ! ( $GLOBALS['pagenow'] == 'post.php' ) ) {

		if ( $this_status_transition_from == 'pending' && $this_status_transition_to == 'processing' ) {

			// this is a new order being created
			$line_items = $instance->get_items();

			// so we can find the newly created orders associated eith the orig order key in the thankyou url
			$orig_order_key = $instance->get_order_key();

			$orig_total         = $instance->get_formatted_order_total();
			$billing_email      = $instance->get_billing_email();
			$billing_first_name = $instance->get_billing_first_name();
			$billing_last_name  = $instance->get_billing_last_name();
			$billing_company    = $instance->get_billing_company();
			$billing_address_1  = $instance->get_billing_address_1();
			$billing_address_2  = $instance->get_billing_address_2();
			$billing_city       = $instance->get_billing_city();
			$billing_state      = $instance->get_billing_state();
			$billing_zip        = $instance->get_billing_postcode();
			$billing_country    = $instance->get_billing_country();
			$billing_phone      = $instance->get_billing_phone();

			$customer_id = $instance->get_customer_id();

			$message_html = '<h2>Thank you ' . $billing_first_name . ' ' . $billing_last_name . '!</h2>';
			$message_html .= '<p>Below are the details and ticket barcodes for your waterpark tickets.</p>';
			$message_html .= '<p>Print out this email and present the attached barcodes the day of your visit.' .
			                 ' Each barcode is only valid for the day that the barcode was created for.</p>';

			$barcode_table = '<table>';
			$barcode_table .= '<tr>';
			$barcode_table .= '<th>Order ID</th><th>Ticket Date</th><th>Barcode For Ticket Date</th>';
			$barcode_table .= '</tr>';

			if ( count( $line_items ) > 1 ) {

				// there are more than one days being. Create email with multiple barcodes/orders
				// delete original order with multiple line items

				foreach ( $line_items as $item ) {

					$product  = $item->get_product();

					// Generate a new barcode string from scratch

					$barcode_string = WC_Order_Barcodes()->get_barcode_string();

					$generator    = new Picqer\Barcode\BarcodeGeneratorJPG();
					$barcode      = $generator->getBarcode( $barcode_string, $generator::TYPE_CODE_128, 2, 60 );
					$base64string = 'data:image/bmp;base64,' . base64_encode( $barcode );

					$order = wc_create_order();

					// JUST ADD ITEM?!?!?!?!?!?!?!?!?!?!?!?!?!?!?

//					$item->save();
					$order->add_item($item);

					$address = array(
						'first_name' => $billing_first_name,
						'last_name'  => $billing_last_name,
						'company'    => $billing_company,
						'email'      => $billing_email,
						'phone'      => $billing_phone,
						'address_1'  => $billing_address_1,
						'address_2'  => $billing_address_2,
						'city'       => $billing_city,
						'state'      => $billing_state,
						'postcode'   => $billing_zip,
						'country'    => $billing_country,
					);

					// Reduce stock quantity for this line item's Product
					// Instead of using wc_update_product_stock() directly
					// the following function does more checks, error handling
					// adds notes to order
					wc_reduce_stock_levels( $order );

					$order->set_address( $address, 'billing' );
					$order->set_customer_id( $customer_id );
					$order->calculate_totals();

					$order_id = $order->get_id();

					update_post_meta( $order_id, '_barcode_image', $base64string );
					update_post_meta( $order_id, '_barcode_text', $barcode_string );

					update_post_meta( $order_id, 'orig_order_key', $orig_order_key );
					update_post_meta( $order_id, 'orig_order_formatted_total', $orig_total );

					$order->update_status( 'processing' );

					$barcode_image_tag = WC_Order_Barcodes()->display_barcode( $order_id, '', '', false );

					$barcode_table .= '<tr>';
					$barcode_table .= '<td>' . $order_id . '</td><td>' . $product->get_name() . '</td><td>' . $barcode_image_tag . '</td>';
                    $barcode_table .= '</tr>';
				}

				$barcode_table .= '</table>';

				// delete original order, completely
				wp_delete_post( $this_get_id, true );

				$message_html .= $barcode_table;

				// Send Order Email to customer
				// send_email_woocommerce_style($email, $subject, $heading, $message)
				send_email_woocommerce_style( $billing_email, 'Your Waterpark Tickets - Wilderness at the Smokies', 'Here Are Your Waterpark Tickets', $message_html );

			} else {

				// this is not a multi-day order

				$single_item = null;
				foreach ( $line_items as $item ) {
					$single_item = $item;
					break;
				}

				$barcode_image_tag = WC_Order_Barcodes()->display_barcode( $this_get_id, '', '', false );
				$barcode_table .= '<tr>';
				$barcode_table .= '<td>' . $this_get_id . '</td><td>' . $single_item->get_product()->get_name() . '</td><td>' . $barcode_image_tag . '</td>';
				$barcode_table .= '</tr>';
				$barcode_table .= '</table>';
				$message_html .= $barcode_table;

				send_email_woocommerce_style( $billing_email, 'Your Waterpark Tickets - Wilderness at the Smokies', 'Here Are Your Waterpark Tickets', $message_html );

			}
		}
	}
}

function vi_thankyou_noorder_content() {

	$order_query_result = wc_get_orders( array(
		'posts_per_page' => - 1,
		'meta_key'       => 'orig_order_key',
		'meta_value'     => $_GET['key'],
		'order'          => 'ASC',
	) );

	?>
    <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">Thank you. Your
        order
        has been received.
        <strong style="color:red;font-size: 1.2em;"> You must print out this page <i>or</i> the email you received
            including all barcodes to be scanned upon arrival to gain entry into the waterparks. Ticket barcodes are
            only good for the day indicated in the product name(s) below.</strong> (ie. "Waterpark Ticket
        5/31/2018")
    </p>

	<?php

	$original_formatted_total = get_post_meta( $order_query_result[0]->get_id(), 'orig_order_formatted_total', true );
	$number_of_days           = count( $order_query_result );

	foreach ( $order_query_result as $order ) {

		// Using custom template in woocommerce/checkout/thankyou.php

		wc_get_template( 'checkout/thankyou.php', array(
			'order'              => $order,
			'is_multiple_orders' => true,
		) );
	}
	?>

    <!--  DISPLAY THE GRAND TOTAL FOR ALL DAYS OF TICKETS  -->

    <table class="woocommerce-table woocommerce-table--order-details shop_table order_details"
           style="font-size:1.4em;">
        <thead>
        <tr>
            <th class="woocommerce-table__product-name product-name">Total for <?php echo $number_of_days; ?> Days
                of
                Tickets.
            </th>
            <th class="woocommerce-table__product-table product-total"><?php echo $original_formatted_total; ?></th>
        </tr>
        </thead>
        <tbody></tbody>
        <tfoot></tfoot>
    </table>

	<?php
}

// Define a constant to use with html emails
define( "HTML_EMAIL_HEADERS", array( 'Content-Type: text/html; charset=UTF-8' ) );
// @email - Email address of the reciever
// @subject - Subject of the email
// @heading - Heading to place inside of the woocommerce template
// @message - Body content (can be HTML)
function send_email_woocommerce_style( $email, $subject, $heading, $message ) {
	// Get woocommerce mailer from instance
	$mailer = WC()->mailer();
	// Wrap message using woocommerce html email template
	$wrapped_message = $mailer->wrap_message( $heading, $message );
	// Create new WC_Email instance
	$wc_email = new WC_Email;
	// Style the wrapped message with woocommerce inline styles
	$html_message = $wc_email->style_inline( $wrapped_message );
	// Send the email using wordpress mail function
	$sent_mail = wp_mail( $email, $subject, $html_message, HTML_EMAIL_HEADERS );
}

// add the actions
add_action( 'woocommerce_order_status_changed', 'vi_woocommerce_possibly_split_new_order', 10, 4 );
//add_action( 'woocommerce_order_status_changed', 'vi_woocommerce_order_status_changed', 10, 4 );

add_action( 'woocommerce_thankyou_noorder', 'vi_thankyou_noorder_content', 10 );

// Function to change email address

function wpb_sender_email( $original_email_address ) {
	return 'noreply@wildernessatthesmokies.com';
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
	return 'Wilderness at the Smokies';
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

add_action( 'admin_head', 'admin_style_note_fix' );
function admin_style_note_fix() {
    echo '<style>.delete_note { display: none; }</style>';
}


class Daniel_Encyrpted_Option {


	public static function update_option($option, $value, $autoload = null) {

		return update_option($option, Crypto\Crypto::encrypt($value, Crypto\Key::loadFromAsciiSafeString( CRYPTOKEY ), $autoload ) );
	}

	public static function add_option($option, $value, $autoload = 'yes') {
		add_option($option, Crypto\Crypto::encrypt($value, Crypto\Key::loadFromAsciiSafeString( CRYPTOKEY ) ), '', $autoload);
	}

	public static function get_option($option, $default = false) {
		$encrypted = get_option($option, $default);
		if ($encrypted === $default) {
			return $default;
		} else {
			try {
				$value = Crypto\Crypto::decrypt($encrypted, Crypto\Key::loadFromAsciiSafeString( CRYPTOKEY ) );
				return $value;
			} catch (Crypto\Exception\WrongKeyOrModifiedCiphertextException $e) {
				return new WP_Error($e->getCode(), $e->getMessage());
			}
		}
	}

}

//add_filter( 'update_post_metadata', 'vi_filter_meta_update', 10, 5 );

function vi_filter_meta_update( $check, $object_id, $meta_key, $meta_value, $prev_value )
{
	// Avoid infinite recursion:
	remove_filter( current_filter(), __FUNCTION__ );

	// Modify the meta value;
	$meta_value = strtolower( $meta_value );

	// Do something else ... encrypt

	// Update the modified value.
	update_post_meta( $object_id, $meta_key, $meta_value, $prev_value );

	// Return something else than null
	return true;
}
<?
/*
Template Name: Hours Calendar
*/
?>
<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<!--<div id="resWidgetTopSub">
<div class="inner max"><?php // !!! CONFLICTS !! include (TEMPLATEPATH . '/res_top.php'); ?></div> 
</div>-->
<div id="pageSlider">
<? //sliders

echo do_shortcode('[rev_slider waterparkslider]');

?>
</div>
<div class="span12" id="page_wrap">
<div id="container" class="light shadow inner max cf">
<h1><? echo get_the_title(); ?></h1> 
<? echo apply_filters('the_content',  get_the_content()); ?>

<?
$rows = get_field('listing_format');
if($rows):
foreach($rows as $row):	?>
<div class="span12 cf">
<div class="span3 col cf full_width_image"><img src="<? echo $row['image']["url"]; ?>" /></div>
<div class="span8 col cf">
<h2><? echo $row['title']; ?></h2>
<p><? echo $row['content']; ?></p>
</div>
</div>
<div id="container" class="cf"><hr /></div>
<?
endforeach;
endif;
?>
</div>
</div>

<div id="container">
<br class="cf"  />
<div id="homeSec3" class="fws cf white">
<div  id="social_feeds" class="inner max cf">
</div>
</div>
</div>
</div>
<?php get_footer(); ?>
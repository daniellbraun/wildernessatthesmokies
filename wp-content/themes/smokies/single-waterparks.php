<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>
<div id="pageSlider">
<? //sliders
if (is_single('5595')){ echo do_shortcode('[rev_slider wildwaterdome]'); }
if (is_single('5627')){ echo do_shortcode('[rev_slider lakewilderness]'); }
if (is_single('5629')){ echo do_shortcode('[rev_slider salamandersprings]'); }
?>
</div>
<?php include (TEMPLATEPATH . '/slider_hub_bar.php'); ?>


<div class="span12 cf" id="page_wrap">
<div class="inner cf max">	

<? $rows = get_field('waterpark_feature'); ?>
<h1><? echo get_the_title(); ?></h1>
<div class="back_btn"> <a  href="/waterparks" >&lsaquo; &nbsp; View all Waterparks </a></div>

<? echo get_the_content(); ?>
</div>
<div id="container" class="inner cf max">	
<? 
$rows = get_field('waterpark_feature');
if($rows):
foreach($rows as $row):	
$title = $row['waterpark_title'];
$description = $row['waterpark_description'];
$gallery = $row['waterpark_gallery'];
		$galId = $gallery->ID;	
$requirements = $row['requirements'];
?>
<div class="inner cf max waterpark_listing">
<div class="span4 col">
<?
if($galId):
		galleryImages($galId); 
		else:
		$thumbnail = get_bloginfo('template_directory').'/images/placeholder_300x300.jpg';
		$fullImage = get_bloginfo('template_directory').'/images/placeholder.jpg';
		echo  ' <div class="room_listing_image"><a href="'.$fullImage.'" class="fancybox"><img src="'.$thumbnail.'" /></a> </div>';
        endif;
        ?>
        </div>
  <div class="span8 col">      
<h2 class="primary"><? echo $title; ?></h2>
<? echo $description; ?>
<br />
<? echo '<b class="primary">'.$requirements .'</b>'; ?>
</div>
</div>
<?	
endforeach;
    endif;
?>

</div>
</div>
</div>

<?php get_footer(); ?>
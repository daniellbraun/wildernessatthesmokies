<?php
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'carousel-item', 275, 230, true);
	add_image_size( 'jc-slider-item', 475, 325, true);
    add_image_size( 'specials', 700, 500, true);
    add_image_size( 'feat_special', 450, 200, true);
    add_image_size( 'room_gallery_thumb', 100, 100, true);
  	add_image_size( 'room_gallery', 600, 600, true);
	
 	register_sidebar( array(
		'name' => 'Main Sidebar',
		'id' => 'main_sidebar',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );

function register_my_menus() {
  register_nav_menus(
    array( 'header-menu' => __( 'Header Menu' ), 'footer-menu' => __( 'Footer Menu' ) )
  );
}
add_action( 'init', 'register_my_menus' );

remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version



add_action( 'wp_ajax_room_submit', 'rooms_callback' );
add_action( 'wp_ajax_nopriv_room_submit', 'rooms_callback' );

function rooms_callback() {
$id = $_POST['lodging'];	
if($id == 'all'){
lodgingAll();
}else{
lodging($id);
}
}





/*|||||||||||||||||||||||||||| Lodging ||||||||||||||||||||||||||||*/


function getfieldprefix($postid) {
	$sql = "select meta_key from wp_postmeta where post_id='".mysql_real_escape_string($postid)."' and meta_key like 'room%room_title' and meta_value='".$_GET['r']."'";
	$result = mysql_query($sql);
	
	if (mysql_num_rows($result)) {
		$row = mysql_fetch_array($result);
		return $row[0];
	}
}

function getroomval($num,$title,$roomid) {
	
	/*
	room_listing_0_room_title
room_listing_1_handicap_accessible
room_listing_1_room_gallery
room_listing_1_room_layout
room_listing_1_room_options
room_listing_1_room_title
	*/
	
	
	$q = "room_listing_".$num.$title;
	$sql = "select meta_value from wp_postmeta where meta_key = '".mysql_real_escape_string($q)."' and post_id='".$roomid."' ";
	
	//echo "sql = $sql";
	
	$result = mysql_query($sql);
	
	if (mysql_num_rows($result)) {
		$row = mysql_fetch_array($result);
		return $row[0];	
	}
}

function getattributenum($att) {
	return 1;
}

function roomSingle($meta_value){	
$args = array( 'post_type' => 'room_listings');
$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post();	
		$rid = get_the_ID();

		/*
		echo "id = ".$rid;
		echo "<br />";
		*/

		$meta_key = getfieldprefix($rid);
		if (!$meta_key) {continue;}
		echo "meta key = ".$meta_key;
		$an = getattributenum($meta_key);
		
		echo "<br />";

		$roomoptions = "";
		$roomoptions = getroomval($an,"_room_options",$rid);
		echo "room options ".$roomoptions;	
		echo "<br />";
		
		echo "<br />";


	endwhile;
}

function roomSinglebackup($meta_value){	
$args = array( 'post_type' => 'room_listings');
$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post();	
?>

<?

	$rows = get_field('room_listing');
	if($rows):
	foreach($rows as $row):
	$room =  $row['room_title'];
	
	if($room == $meta_value){
		
		$options = $row['room_options'];	
		$room =  $row['room_title'];
		//$images = $row['room_gallery'];
 		//$image = $images[0];
		//$thumbnail = $image['sizes']['medium'];
		$thumbnail = get_bloginfo('template_directory').'/images/placeholder_300x300.jpg';
		$layout = $row['room_layout'];
		$layoutThumbnail = $layout['sizes']['medium'];	
		
		?>
        <h1>Lodging Details</h1>
		<div class="room_listing cf" >
        <div class="span4"><img class="room_listing_image" src="<? echo $thumbnail; ?>" /></div>
		<div class="span8">
        <div class="room_title"><? echo $room; ?></div>
    	<div class="room_options"> 
		<? echo $options; ?>
        </div>
        </div>
        <br class="clear" />
		</div>
		<?
		}

	endforeach;
	endif;
	endwhile;
}

//////// LODGING MENU /////////

function makeroomtitles($id) {
	$sql = "SELECT *
FROM wp_postmeta
WHERE post_id ='".mysql_real_escape_string($id)."'
AND meta_key LIKE 'r%room_title'
ORDER BY `wp_postmeta`.`meta_value` ASC
";

$result = mysql_query($sql);

while ($row = mysql_fetch_array($result)) {
	echo "title: ".$row['meta_value']."<br />";
}

}

function lodgingMenu(){	
$args = array( 'post_type' => 'room_listings','orderby'=> 'menu_order','order'=> "ASC");
$loop = new WP_Query( $args );

echo '<div id="lodging_nav">';

while ( $loop->have_posts() ) : $loop->the_post();	

	makeroomtitles(get_the_ID());
	echo "<br /><br />";
endwhile;



/*
while ( $loop->have_posts() ) : $loop->the_post();	



	$title = get_the_title();
	$content = get_the_content();
	$postid = get_the_ID();
	echo '<div class="span4 col">';
echo '<div class="menu_header"><a href="/lodging?'.$postid.'">'.$title.'</a></div>';



	$rows = get_field('room_listing');





	

	if($rows):
	echo '<ul class="sub_menu_item">';
	foreach($rows as $row):
	$handicap = $row['handicap_accessible'];
	
	if ($handicap != ''){		
	}else{
	$room =  $row['room_title'];
	echo '<li><a href="/lodging/room?r='.urlencode($room).'">'.$room.'</a></li>';
	}
	endforeach;
	echo '</ul>';
	endif;
	echo '<br class="clear" /></div>';
	
	
	endwhile;
		echo '</div>';

*/

	wp_reset_query();
	
}

//////// Lodging Options ///////

function lodging($id){	
$args = array( 'post_type' => 'room_listings', 'p'=>$id);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();	
	get_lodging_data($post->ID);
	endwhile;
	exit();
}

function lodgingAll(){	
$args = array( 'post_type' => 'room_listings', 'posts_per_page' => -1,'orderby'=> 'menu_order','order'=> "ASC");
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();	
	get_lodging_data($post->ID);
	endwhile;
	exit();
}

function galleryCount($images,$r){
	if($images):
	$i = 0;
  foreach( $images as $image ):
  $i++;
  endforeach;	
     echo  '<a class="gallery_link">'.$i.'&nbsp;Photos</a>';
  endif;
}

function galleryImages($images, $r){
	if($images):
		$i = 0;
	?> 
      <div class="jcarousel-wrapper">
        <div class="jc">
        <ul class="galThumbs"> 	
     <?
  foreach( $images as $image ):
   $i++;
     if($i!= 1):
  ?><li class="galThumb">
     <a href="<?php echo $image['sizes']['room_gallery']; ?>" class="fancybox hidden" rel="gallery<? echo $r; ?>">$i</a> 
    </li>
  <?   
   endif;
  endforeach;	
  ?></ul></div></div><?
    
  endif;
}



function get_lodging_data($id){	
	$title = get_the_title($id);
	$content = get_the_content($id);
echo '<h3 class="primary">'.$title.'</h3>'.'<p>'.$content.'</p>'; 		
	$rows = get_field('room_listing');
	if($rows):
		foreach($rows as $row):
		$options = $row['room_options'];	
		$room =  $row['room_title'];
		$r =  $row['room_title'];
		$r  = preg_replace('/\s+/', '', $r);
		$images = $row['room_gallery'];
 		$image = $images[0];
		if ($image){
		$thumbnail = $image['sizes']['medium'];
		$fullImage = $image['sizes']['room_gallery'];
		}else{
		$thumbnail = get_bloginfo('template_directory').'/images/placeholder_300x300.jpg';
		$fullImage = get_bloginfo('template_directory').'/images/placeholder.jpg';
		}
		$layout = $row['room_layout'];
		$layoutThumbnail = $layout['sizes']['medium'];	
		?>
		<div class="room_listing cf" >
        <div class="span4 col room_gallery">
        <div class="room_listing_image"><a href="<?php echo $fullImage; ?>" class="fancybox" rel="gallery<? echo $r; ?>"><img src="<? echo $thumbnail; ?>" /></a>
		<? galleryCount($images,$r); ?>       
       	<? galleryImages($images,$r); ?>
        </div></div>
  	  
		<div class="span8 room_info col">
 		<div class="room_title"><? echo '<a href="/lodging/room?r='.$room.'">'. $room .'</a>'; ?></div>
     	<div class="room_options"> 
		<? echo $options; ?>
        </div>
        <br class="clear" />
        <div class="span3 room_listing_button get_rates cf"><a href="http://res.wildernessatthesmokies.com/Availability.aspx">Get Rates</a></div>
        </div>
        <br class="clear" />
		</div>
   
	<?
    endforeach;
	endif;
}


/*|||||||||||||||||||||||||||| Home Content||||||||||||||||||||||||||||*/

function homeContent(){
	
$args = array( 'post_type' => 'home_content', 'posts_per_page' => 1,'orderby'=> 'modified');
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();	
$content = the_content();		
echo $content; 	
endwhile;
}

/*|||||||||||||||||||||||||||| Featured Specials ||||||||||||||||||||||||||||*/

function featuredSpecical(){
	

$args = array( 'post_type' => 'specials', 'posts_per_page' => 1,'orderby'=> 'modified');
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();	
$feat = get_field('featured_special');
if($feat):
	foreach($feat as $feat): 
		
		$image =  the_post_thumbnail('feat_special');
		$savings = get_field('savings_text');
		$title = get_the_title();

		ob_start();
		$content = the_content();
		$old_content = ob_get_clean();
		$new_content = strip_tags($old_content);
		
		if ($feat == 'Feature on homepage'):?>
			
          <? echo $image; ?>
            <h4><? echo $title; ?></h4>
            <p><? echo $new_content; ?></p>
            <?
			
	endif;
	endforeach;
	endif;
endwhile;
 
}


/*|||||||||||||||||||||||||||| Carousels ||||||||||||||||||||||||||||
Carousels are dependent on /js/jcarousel.responsive.js 
*/


/* Home Carousel */
function homeCarousel(){
	
$args = array( 'post_type' => 'carousel', 'posts_per_page' => -1);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();	
$id = get_the_ID();

	if($id == 4862):
	$rows = get_field('carousel_items');
	if($rows):
	?>
        <div class="jcarousel-wrapper">
        <div class="jc-slider">
        <ul> 			 
	<?			 

	foreach($rows as $row):
		$image =   $row['carousel_image'];
		$title =   $row['carousel_title'];
		$content =  $row['carousel_content'];
		$link =  $row['carousel_link'];
		?>
         <li>
            <a href="<? echo $link; ?>"><img src="<?php echo $image['sizes']['jc-slider-item']; ?>" /></a>
            <div class="jc-slider-content"> <h2> <?php echo $title; ?></h2> <p><?php echo $content; ?></p> 
            </div>
        </li>
        <? endforeach; ?>
    </ul>
    </div>
    <p class="jc-slider-pagination"></p>
    </div> 	
	<?
	endif;
	endif;	
	endwhile;
}

/* Eat Stay Play Carousel */

function espCarousel(){
$args = array( 'post_type' => 'carousel', 'posts_per_page' => -1);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
$id = get_the_ID();
if($id == 4861):
	
	$rows = get_field('carousel_items');
	if($rows):
	?>
        <div class="jcarousel-wrapper">
        <div class="jcarousel">
        <ul> 			 
	<?			 

	foreach($rows as $row):
		$image =   $row['carousel_image'];
		$content =  $row['carousel_content'];
		$link =  $row['carousel_link'];
	
		?>
        		 <li>
                 
                    <a href="<? echo $link; ?>"><img src="<?php echo $image['sizes']['carousel-item']; ?>" /></a>
                    <p><?php echo $content; ?></p>
                </li>
      
	  <? endforeach; ?>
 
    </ul>
    </div>
    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
    <a href="#" class="jcarousel-control-next">&rsaquo;</a>

    </div> 	
	<?
	endif;
	endif;
	endwhile;
	
}


function youTubeHome(){
$args = array( 'post_type' => 'youtube', 'posts_per_page' => 1);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();
$id = get_the_ID();
if($id == 4881):
	$rows = get_field('youtube_video');
	if($rows):
		
	foreach($rows as $row):
		$image =   $row['video_thumb'];
		$content =  $row['video_description'];

$string = strip_tags($content);
if (strlen($string) > 180) {
    $stringCut = substr($string, 0, 180);
    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
}
		$video_url=  $row['video_url'];
		$video_url = str_replace("http://youtu.be/", "", $video_url);

 ?>
        <div class="video-container">
        <iframe src="<? echo 'http://www.youtube.com/embed/'.$video_url.'?rel=0'; ?>" frameborder="0" width="560" height="315"></iframe>
        </div>                    
        <p style="padding-top:15px;"><? echo  $string ; ?><span class='view_more'><a href="/videos"> Watch Now &#10097;</a></span></p>
		<?
	endforeach;
	endif;
	endif;
endwhile;	
}


function twitter_feed(){
?>
<a class="twitter-timeline"  href="https://twitter.com/WildSmokies" data-chrome="nofooter transparent noheader noborders" data-tweet-limit="3" data-link-color="#006FB8" data-widget-id="427478790625718272">Tweets by @WildSmokies</a>
 <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<?	
}





/*|||||||||||||||||||||||||||| POST TYPES ||||||||||||||||||||||||||||*/

add_action( 'init', 'create_post_types' );
function create_post_types() {
	
	register_post_type( 'specials',
		array(
			'labels' => array(
				'name' => __( 'Specials' ),
				'singular_name' => __( 'Special or Package' )
			),
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title', 'editor',  'thumbnail','page-attributes' ),
		'menu_icon' => '/wp-content/themes/smokies/images/packagesicon.png'

		)
	);
	
	register_post_type( 'carousel',
		array(
			'labels' => array(
				'name' => __( 'Carousels' ),
				'singular_name' => __( 'Carousel' )
			),
		'public' => true,
		'has_archive' => true,
		'supports' => array( 'title','page-attributes' ),
		'menu_icon' => '/wp-content/themes/smokies/images/carouselicon.png'
		)
	);	
	
	register_post_type( 'home_content',
		array(
			'labels' => array(
				'name' => __( 'Home Page' ),
				'singular_name' => __( 'Home Content' )
			),
		'public' => true,
		'has_archive' => true,
	    'supports' => array( 'title', 'editor'),
		'menu_icon' => '/wp-content/themes/smokies/images/homeicon.png'
		)
	);	
	
	
	register_post_type( 'youtube',
		array(
			'labels' => array(
			'name' => __( 'YouTube' ),
			'singular_name' => __( 'YouTube Gallery' )
			),
		'public' => true,
		'has_archive' => true,
	    'supports' => array( 'title'),
		'menu_icon' => '/wp-content/themes/smokies/images/youtube_admin_icon.png'
		)
	);	
	
	
	
	register_post_type( 'room_listings',
		array(
			'labels' => array(
			'name' => __( 'Room Listings' ),
			'singular_name' => __( 'Rooms' )
			),
		'public' => true,
		'has_archive' => true,
	    'supports' => array( 'title', 'editor','page-attributes' ),
		'menu_icon' => '/wp-content/themes/smokies/images/rooms_icon.png'
		)
	);	
	
	
	
}

/*|||||||||||||||||||||||||||| END POST TYPES ||||||||||||||||||||||||||||*/
function customTitle(){
	if (is_page('room')){
	echo 'Wilderness at the Smokies | '.$_GET["r"];
	}else{
		if (is_home()){
			echo 'Wilderness at the Smokies | Tennessee\'s Largest Waterpark Resort';
		}else{
	wp_title('Wilderness at the Smokies | ', true, 'left'); 	
		}
	}
}

register_sidebar(array(
  'name' => __( 'Lodging Sidebar' ),
  'id' => 'sidebar-lodging',
  'before_widget' => '<li id="%1$s" class="sidebarBox">',
  'after_widget'  => '</li>',
  'before_title' => '<h5 class="varied_title">',
  'after_title' => '</h5>',
));



function the_breadcrumb() {
        echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        echo '</a></li><li class="separator"> / </li>';
		if (is_page('lodging')) {echo '<li><a href="/lodging">Lodging</a></li>';  }else{
		if (is_page('room')) {echo '<li><a href="/lodging">Lodging</a></li><li class="separator"> / </li><li><strong>'.$_GET['r'] .' </strong></li> ';  }else{
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li class="separator"> / </li><li> ');
            if (is_single()) {
                echo '</li><li class="separator"> / </li><li>';
                the_title();
                echo '</li>';
            }
		
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );  
                foreach ( $anc as $ancestor ) {
                    $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator">/</li>';
                }
                echo $output;
                echo '<strong title="'.$title.'"> '.$title.'</strong>';
            } else {
                echo '<strong> ';
                echo the_title();
                echo '</strong>';
            }
        }
    }
  }	
}	
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
	
	
}

?>
<?php get_header(); ?>
<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>
<div id="pageSlider">
<? //sliders
if (is_single('5823')){ echo do_shortcode('[rev_slider pottery]'); }
if (is_single('5797')){ echo do_shortcode('[rev_slider golf]'); }
if (is_single('5801')){ echo do_shortcode('[rev_slider dining]'); }
if (is_single('5938')){ echo do_shortcode('[rev_slider freetickets]'); }
if (is_single('5826')){ echo do_shortcode('[rev_slider retail]'); }
if (is_single('5950')){ echo do_shortcode('[rev_slider fitness]'); }
if (is_single('5934')){ echo do_shortcode('[rev_slider rafting]'); }
if (is_single('7169')){ echo do_shortcode('[rev_slider jetboat]'); }

?>
</div>
<?php include (TEMPLATEPATH . '/slider_hub_bar.php'); ?>

<div class="span12 cf" id="page_wrap">

<div  id="amenities_wrap" class="inner max cf">
<div id="container" class="light shadow inner max cf">
<h1><? echo get_the_title(); ?></h1> <br />
<div class="back_btn"> <a  href="/amenities" > &lsaquo; View all Amenities </a></div>

<? echo get_the_content(); ?>

<?
$rows = get_field('listing_format');
if($rows):
foreach($rows as $row):	?>
<div class="span12 cf">
<div class="span3 col cf full_width_image"><img src="<? echo $row['image']["url"]; ?>" /></div>
<div class="span8 col cf">
<h2><? echo $row['title']; ?></h2>
<p><? echo $row['content']; ?></p>
</div>
</div>
<div id="container" class="cf"><hr /></div>
<?
endforeach;
endif;
?>

</div>
</div>
</div>
</div>

<?php get_footer(); ?>
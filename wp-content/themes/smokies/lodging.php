<?php 
/*
Template Name: Lodging
*/
?>
<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
<br class="clear" />
</div>
<div id="pageSlider">
<?php echo do_shortcode('[rev_slider lodgingslider]') ?>
</div> 
<?php include (TEMPLATEPATH . '/slider_hub_bar.php'); ?>

<div id="container" class="cf">
<div class="span12 cf" id="page_wrap">

<div class="span12">
<div class="inner max cf">

<div  class="span3 col">
<?php get_sidebar('lodging'); ?>
<br class="clear" />
</div>

<div class="span9 col" id="content">

<div id="room_filters" >

<form id="roomTypes">
	<div class="span1 col">
    <input type="radio" id="radio1" name="radios" value="all" checked>
    <label for="radio1">All</label>
    </div>
    <div class="span3 col">
    <input type="radio" id="radio2"name="radios" value="4916">
    <label for="radio2">Stone Hill Lodge</label>
    </div>
    <div class="span3 col">
    <input type="radio" id="radio3" name="radios" value="4922">
    <label for="radio3">River Lodge Suites</label>
    </div>
    <div class="span3 col">
    <input type="radio" id="radio4" name="radios"  value="4924">
    <label for="radio4">Sanctuary Villas</label>
 	</div>
      <div id="handicap" class="span1 col">
    <input type="radio" id="handi" name="radios"  value="handicap">
    <label for="handi">handicap rooms</label>
 	</div>
</form>
   <br class="clear" />
</div>
<br class="clear" />
<div id="container">
<?php include (TEMPLATEPATH . '/loader.php'); ?>
<div id="rooms_containter"><!--Rooms Loaded By Ajax--></div>
</div>
</div>
</div>
</div>
</div></div></div>
<?php get_footer(); ?>
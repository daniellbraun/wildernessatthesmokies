<?php 
/*
Template Name: Specials
*/
?>
<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">
<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
<br class="clear" />
</div>

<div class="span12 cf" id="page_wrap">

<div class="inner max cf">
<h1><?php echo get_the_title(); ?></h1>
<?php echo get_the_content(); ?>
<div id="specialsFilter">
<div id="containter">
<form id="monthFilter">
    <div class="span2 col">
    <label for="month">Monthly Special Offers</label>
    </div>
       <div class="span2 col">
    <select class="monthly custom-select"  name="month"  id="month">
	<option value='01'>January</option>
    <option value='02'>February</option>
    <option value='03'>March</option>
    <option value='04'>April</option>
    <option value='05'>May</option>
    <option value='06'>June</option>
    <option value='07'>July</option>
    <option value='08'>August</option>
    <option value='09'>September</option>
    <option value='10'>October</option>
    <option value='11'>November</option>
    <option value='12'>December</option>
    
    <? /*
    $months = array();
    for ($i = 0; $i < 12; $i++) {
    $timestamp = mktime(0, 0, 0, date('m') - $i, 1);
    $months[date('m', $timestamp)] = date('F', $timestamp);
    }	
    $reversed = array_reverse($months);

    foreach ( $reversed as $num => $name) {
	$cm =  date('m'); 
	if($num == $cm){	
	    echo '<option value="'.$num.'" selected>'.$name.'</option>';
	}else{
    echo '<option value="'.$num.'">'.$name.'</option>';
	}
	}
   */ ?>
    </select>
    </div>
</form>
</div>
<br class="cf" />
</div>
</div>
<div class="inner max cf">
<!--content area-->
<?php include (TEMPLATEPATH . '/loader.php'); ?>
<div id="load_specials"><!--Specials Loaded By Ajax--></div> 
</div>
<div id="container">
<div class="inner max cf">

<h3 class="cf primary"><span>+</span> Add-On Packages</h3>
<p>Packages are a great way to enhance your stay and to acquire savings during your Wilderness at the Smokies getaway. Add-on these packages while you reserve your room.</p>
</div>
<div class="inner max cf">
<?
packages();
?>
<!--end content area-->
</div>
</div>
</div></div>
<?php get_footer(); ?>
<?php
/*
Template Name: Home School Days
*/
?>
<?php get_header(); ?>
	<br class="clear"/>
	<div id="ch"><!--clear header--></div>
	<div id="container">
		<div class="bread_crumbs_wrap">
			<div class="inner max">
				<div id="bread_crumbs"><?php the_breadcrumb(); ?></div>
			</div>
		</div>
		<div id="resWidgetTopSub">
			<div class="inner max"><?php include( TEMPLATEPATH . '/res_top.php' ); ?></div>
			<br class="clear"/>
		</div>
		<div id="pageSlider">
			<?php echo do_shortcode( '[rev_slider homeSchool]' ) ?>
		</div>
		<?php include( TEMPLATEPATH . '/slider_hub_bar.php' ); ?>

		<div class="span12 cf" id="page_wrap">
			<?php //include (TEMPLATEPATH . '/meetings_nav.php'); ?>

			<div id="container" class="inner max homeSchool">
				<h1><? echo get_the_title(); ?></h1>
				<? echo get_the_content(); ?>
			</div>
			<div class="inner max cf">
				<? home_school_days(); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
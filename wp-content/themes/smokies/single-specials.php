<?php
/*
Template Name: specials-single
*/
?>
<?php get_header(); ?>

<br class="clear" />
<div id="ch"><!--clear header--></div>
<div id="container">

<div class="bread_crumbs_wrap"><div class="inner max"><div id="bread_crumbs"><?php the_breadcrumb(); ?></div></div></div>
<div id="resWidgetTopSub">
<div class="inner max"><?php include (TEMPLATEPATH . '/res_top.php'); ?></div>
</div>

<div class="span12 cf" id="page_wrap">
<div class="inner cf max">	

<h1><? echo get_the_title(); ?></h1>
<div class="back_btn"> <a  href="/specials" > &lsaquo; &nbsp;  View all Specials </a></div>

</div>
<div class="inner cf max">
<div class="offer_value_wrap">
<div class="span9 cf col dates_wrap">
Good Thru
<br />
<span class="savings_dates">
<?  
$startdate = DateTime::createFromFormat('Ymd', get_field('offer_start_date'));
echo $startdate->format('M d, Y');
echo '&nbsp;&mdash;&nbsp;';
$enddate = DateTime::createFromFormat('Ymd', get_field('offer_end_date'));
echo $enddate->format('M d, Y');
?>
</span></div>
<div class="span3 cf col value_wrap">	
Offer Value
<br />
<h2 class="savings_text"> <? echo get_field('savings_text'); ?></h2>
</div>
<br class="cf" />
</div>	
</div>	
<br class="cf">
<div class="inner max cf full_width_image">
<?
		$post_id = get_the_ID(); 
		$url = get_permalink();
		$image = get_the_post_thumbnail( $post_id,'specials'); 
		if(!$image):
		$image = '<img src="'.get_bloginfo('template_directory').'/images/special_placeholder.jpg" />';
		endif; 
		echo $image;
?>
</div>
<br class="cf">
<div class="inner cf max">
<div class="span7 col">	
<div class="span12 col specials_content">	
<h3 class="offer_details">What's Included?</h3>

<? echo the_content(); ?>

<?  $restrictions = get_field('special_restrictions'); ?>

<div id="date-restrictions" class="primary"><? echo $restrictions; ?></div>
<br class="cf" />
<? 
$value = get_field('options');
if ( in_array('Waterpark passes included',$value, TRUE) ):
$imgDir = get_bloginfo('template_directory').'/images/';
echo '<div class="span6 cf" style="text-align:center;"><img src="'.$imgDir.'wp_included_sm.png"></div>';
endif;
?>
<br /><br />
<div id="container">

<h4> Offer Details </h4>
<p><? echo get_field('special_details'); ?></p>
</div>
<br />
<? $promo = get_field('promo_code'); ?>
<? if($promo): ?>
Use Online Promo Code:<b> <? echo $promo; ?> </b>
<br class="cf" />
<? endif; ?>

</div>


</div>
<div class="span5 col">
<div class="span12 col dark " style=" text-align:center"><h5>Call to Reserve Now</h5>Reservations & Resort Info<span class="phone" >(877) 325-9453</span></div>
<div class="span12 col">

<div class="span12 dark" >
<h5>Resort Policies</h5>
<?php include (TEMPLATEPATH . '/resort_policies.php'); ?>
</div>
<div class="span12">

<?
$unique_id = get_the_ID();
if ( in_array('Show book now button',$value, TRUE) ):
	$tomorrow = new DateTime('tomorrow');
echo '<div class="book_now_btn"><a id ="special_book_' . $unique_id . '" href="https://sh-secure.wildernessatthesmokies.com/wildernessatthesmokies-sh?ArrivalDate='. date("m/d/Y") .'&DepartureDate='. $tomorrow->format('m/d/Y') .'">Reserve Now</a></div>';
endif;
?>
</div>
</div>
</div>
</div>
</div></div>
<?php get_footer(); ?>
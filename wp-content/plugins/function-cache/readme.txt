=== Plugin Name ===
Contributors: smithaa02
Tags: function cache, fragment caching, optimization, performance, database, speed, optimize
Requires at least: 3.0.1
Tested up to: 4.1
Stable tag: 20150122
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A fragment caching framework with customizable expiration triggers.  Control what gets cached and what triggers will cycle out specific cache values.

== Description ==

asdf

== Installation ==

Just activiate it.  Once you do this, you'll have access in your plugins/functions.php file/templates to use the core cache functions provided.  See description for how to make this work.


== Frequently Asked Questions ==

= There are a lot of Wordpress caching plugins.  Why this one? =

The big problem with most cache engines is they cache too much and the cache values don't dissapear when you make updates.  This solves both problems, which is frankly pretty unique for Wordpress cache engines.

= What about W3 Total Cache? =

This is probrably the most popular cache plugin out there.  It can be good, but in my experience it creates a lot of stale content that frustrates users when the website doesn't get properly updated.

= What about Wordpress Transients? =

Couple problems.  WP puts the transients in the wp_options table which causes and is inflected with serious bloat (bloat = performance issues). Wordpress function cache uses its own table to improve performance. Second issue is that transients just use a date to trigger cache deletion.  This is not practical and too limiting.  This is why this cache engine associates page ids and post types with the cache values, and deletes name/value pairs when posts are updated.

= Why is my site slow? =

It is either client side or server side.  To determine if it is client side, view the source of a rendered slow page. Save it as say test.html and upload it.  If that is faster, you have a server side problem.  If not, client side.  Another way to check is in firefox and chrome, the loading graphic spins counter clockwise for server load and clockwise for client load (rough abstraction).  If it spins say more then a half cycle "6 hours" counter clock-wise, this plugin may help.

= What slows down a wordpress page from the server side? = 

Much of what is on a wordpress template is innocent (including get_content()).  Usually the culprits are components that use the wp_options and wp_posts table and components that incorproate looping.  95% of slow WP sites is due to slow databases.  Wordpress stuffs so much in the wp_options and wp_posts table and then multiplies these together which takes a ton of computing power.  Key is to mimimize the lookups you need to do to the database.  This mostly means caching the functions that contain important query loops and custom field lookups.

= How does this relate to Custom Post Types & Fields? =

These tend to be the biggest resource hogs.  If you use Advanced Custom Fields, this is especially bad because they multiply the wp_options table on itself (which becomes a serious issue for big sites).  ACF also uses wildcard strings for their repeater fields which turns into a performance nightmare for queries.  Custom Content Manager is much faster (20X by my benchmark for their get custom field function) and benifits from a builtin cache unlike ACF.  But CCTM doesn't cache results accross sessions.  Wordpress Function Cache does.

= Can upgrading Apache/PHP/MySQL help with my slow website? = 

Yes, but usually not signficantly.  Wordpress performance tends to degrade in an exponetial fashion whereas these improvements just improve things in an incremental fashion.  Same with hardware.  The best updates you can do would be to switch tables to Innodb, make sure you are using the latest MySQL, optimize MySQL for large table queries, and install OPcache (for PHP 5.5).  OPcache is AWESOME!  But even with that you'll still need a plugin like this one to handle sites with large databases.


== Changelog ==

= 20150122 =
The very first version of the program!


<?php
/**
 * Plugin Name: Function Cache
 * Plugin URI: http:www.google.com
 * Description: A fragment caching framework with customizable expiration triggers
 * Version: 20150619
 * Author: Aaron Smith
 * Author URI: http://www.google.com
 * License: GPL2
 */


defined('ABSPATH') or die("No script kiddies please!");

//Wordpress actions that will clear specific cache triggers
add_action('save_post', array('function_cache','flush_db_post_cache'));
add_action('deleted_post', array('function_cache','flush_db_post_cache'));
add_action('widget_update_callback', array('function_cache','flush_db_widget_cache'));
//add_action('wp_update_nav_menu','flush_db_menu_cache'); //WP bug
if ($_SERVER['REQUEST_URI']=='/wp-admin/nav-menus.php') {function_cache::flush_db_menu_cache();}
add_action('admin_menu', array('function_cache','register_function_cache_admin_page'));


/**
add_category
create_category
delete_category
edit_categoy

add_attachment
edit_attachment
delete_attachment (timing is off)
save_post
updated_postmeta 

trashed_post
untrashed_post 
deleted_post
post_updated
transition_post_status


future posts, pages, attachments, comments, rss
**/

//Plugin Stuff
register_activation_hook(__FILE__, array('function_cache','create_db'));
add_action( 'plugins_loaded', array('function_cache','db_check') );
register_deactivation_hook( __FILE__, array('function_cache','db_destroy') );

class function_cache {
	
/************cache section of the class************/

	public static function testaction() {
		//mail("asmith0202@gmail.com","action Fired","Action Just Fired");
	}


	public static function load_into_cache($name,$value,$args) {
		//mail("asmith0202@gmail.com","Load into cache fired","Load into cache fired");
		global $wpdb;

		$post_types = '';
		$widgets = '';
		$menus = '';
		$post_ids = '';
		
		if (!empty($args['post_types'])) {$post_types = "'".implode("','",$args['post_types'])."'";}
		if (!empty($args['post_ids'])) {$post_ids = "'".implode("','",$args['post_ids'])."'";}
		if (!empty($args['widgets'])) {$widgets=1;}
		if (!empty($args['menus'])) {$menus=1;}

		$wpdb->insert($wpdb->prefix."function_cache",array("name"=>$name,"value"=>$value,"post_types"=>$post_types,"post_ids"=>$post_ids,"widgets"=>$widgets,"menus"=>$menus));
	}

	public static function check_if_in_cache($name) {
		if (!empty($_GET['stop_cache'])) {return;} //to disable the cache for a page for testing purposes, just add ?stop_cache=1 to the URL
		
		global $wpdb;
		$sql = $wpdb->prepare('select value from '.$wpdb->prefix."function_cache".' where name= %s limit 0,1',$name);	
		return $wpdb->get_var($sql);
	}

	public static function flush_db_post_cache($post_id) {
		if ( wp_is_post_revision($post_id))    return; //not smart to flush the cache on mere auto revision updates!

		global $wpdb;

		$sql = $wpdb->prepare("delete from {$wpdb->prefix}function_cache where post_ids like '%%%s%%'","'$post_id'");
		//$sql = $wpdb->prepare("delete from {$wpdb->prefix}function_cache where post_ids like '%%%s%%'","'$post_id'");

		$wpdb->query($sql); //so if post 33 was just create/deleted/updated, all cache values with the expiration trigger of post id 33 will be deleted

		if (get_post_type($post_id)) {
			$sql = $wpdb->prepare("delete from {$wpdb->prefix}function_cache where post_types like '%%%s%%'","'".get_post_type($post_id)."'");
			$wpdb->query($sql); //so if post 33 was of post type houseforsale, then all cache values with the trigger of posttype = houseforsale will be deleted
		}

	}
	
	public static function flush_db_widget_cache() {
		global $wpdb;
		$sql = "delete from {$wpdb->prefix}function_cache where widgets='1'";
		$wpdb->query($sql); 
	}
	
	public static function flush_db_menu_cache() {
		//global $current_screen;
		//if (!($current_screen == 'nav-menus' && isset($_POST['menu']))) {return;}
  
  		//mail("aaron@ad-lit.com","This is a test subject","This is a test body");
  
  		global $wpdb;
		$sql = "delete from {$wpdb->prefix}function_cache where menus='1'";
		$wpdb->query($sql); 
	}
		
	public static function empty_entire_cache() {}


/************utility components of the class************/
	public static $db_version = 20150622;

	public static function register_function_cache_admin_page() {
		add_menu_page( 'Function Cache Settings', 'Function Cache', 'manage_options', 'functioncachesettings', array('function_cache','function_cache_settings_page7')); 
	}

	public static function function_cache_settings_page7() {
		global $wpdb;
		
		echo "<div class='wrap'>";
		echo "<h2>Empty Function Cache</h2>";
		echo '<br />';
		if ($_GET['flushcache']) {
			$sql = "delete from {$wpdb->prefix}function_cache";
			$wpdb->query($sql); 
			echo "<b style='color:red'>CACHE DELETED</b>";
			echo '<br /><br />';
		}

		echo "<form method='GET' action='/wp-admin/admin.php'><input type='hidden' name='page' value='functioncachesettings' /><input type='submit' name='flushcache' value='FLUSH CACHE' /></form>";
		echo "</div>";
	}

	public static function create_db() {
		global $wpdb;
		$sql = "CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."function_cache (
		  id bigint(11) NOT NULL AUTO_INCREMENT,
		  name text NOT NULL,
		  value longtext NOT NULL,
		  post_types text NOT NULL,		  
		  post_ids text NOT NULL,
		  widgets TINYINT(1) NOT NULL,
		  menus TINYINT(1) NOT NULL,
		  PRIMARY KEY  (id)
		) ";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		add_option("function_cache_db",self::$db_version);
	}	

	public static function db_check() {
		if ( get_site_option( 'function_cache_db' ) != self::$db_version ) {
		    self::create_db();
		}
	}

	public static function db_destroy() {
		global $wpdb;		
		delete_option('function_cache_db');
		$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}function_cache");
	}

	private function __construct() {/*who needs constructors when static functions/variables do the trick*/}

/************end of class************/

}

/** example **/
/*
	$cache_query = __FUNCTION__.serialize(func_get_args()); 
	$cache_value = function_cache::check_if_in_cache($cache_query);
	if (0 && $cache_value) {return $cache_value;}

	$expiration_triggers = array(
		"post_types" => array("attachment"),
		"post_ids" => array($post_id)
	);	
	function_cache::load_into_cache($cache_query,$output,$expiration_triggers);
	
*/
/** end example **/

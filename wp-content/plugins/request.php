<?php
define('DOING_AJAX', true);
 
if (!isset( $_POST['action']))
    die('-1');
 
//relative to where your plugin is located
require_once($_SERVER["DOCUMENT_ROOT"].'/wp-load.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/wp-content/themes/smokies/functions.php'); 
//Typical headers
//header('Content-Type: text/html');
//send_nosniff_header();
 
//Disable caching
//header('Cache-Control: no-cache');
//header('Pragma: no-cache');
 
$action = esc_attr($_POST['action']);
 
//A bit of security
$allowed_actions = array(
	'socials',
    'room_submit',
	'specials',	
);
 
if(in_array($action, $allowed_actions)){
    if(is_user_logged_in())
        do_action('custom_ajax_'.$action);
    else   
        do_action('custom_ajax_nopriv_'.$action);
}
else{
    die('-1');
}

?>